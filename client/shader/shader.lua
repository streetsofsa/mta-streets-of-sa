screen = {guiGetScreenSize()}

shader = {}
shader.types = { --path
	["blur"] = {"blur.fx"}
}


function shader:new(...)
	local obj = setmetatable({}, {__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
	return obj
end

function shader:render()
	dxUpdateScreenSource(self.texture)
	dxDrawImage(self.coord[1], self.coord[2], self.coord[3], self.coord[4], self.shader)
end

function shader:destroy()
	removeEventHandler("onClientRender", root, self.renderfunc)
	self = nil
end

function shader:constructor(shadertyp, amount, x, y, width, height)
	if not shader.types[shadertyp] then
		return "Invalid shader-type"
	end
	
	if not x then x = 0 end
	if not y then y = 0 end
	if not width then width = screen[1] end
	if not height then height = screen[2] end
	
	self.shadertyp = shadertyp
	self.amount = amount --Float(0.01-1)
	self.coord = {x,y,width,height}
	
	self.shader = dxCreateShader("resources/shaders/"..shader.types[shadertyp][1])
	
	if not self.shader then
		return "Shader creation failed!"
	end
	
	self.texture = dxCreateScreenSource(screen[1], screen[2])
	self.txdsize = {dxGetMaterialSize(self.texture)}
	
	dxSetShaderValue(self.shader, "SFX_TEXTURE", self.texture)	
	dxSetShaderValue(self.shader, "SFX_TXDSIZE", self.txdsize[1], self.txdsize[2])
	dxSetShaderValue(self.shader, "SFX_AMOUNT", self.amount)
	--dxSetShaderTessellation(self.shader, 16, 16)
	
	self.renderfunc = function(...) self:render(...) end
	
	addEventHandler("onClientRender", root, self.renderfunc)
end






