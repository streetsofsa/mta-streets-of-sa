--[[addEventHandler("onClientHUDRender", getRootElement(), function()
for i, v in ipairs(getElementsByType("player", getRootElement(), true)) do
local r,g,b = unpack(Fraktionen.settings.colors[getPlayerData(v, "Faction")])
	if v ~= getLocalPlayer() then
		setPlayerNametagShowing(v, false)
		local lX, lY, lZ = getPedBonePosition(getLocalPlayer(), 8)
		local pX, pY, pZ = getPedBonePosition(v, 8)
		if getDistanceBetweenPoints3D(lX, lY, lZ, pX, pY, pZ) < 20 then
			if isLineOfSightClear(lX, lY, lZ, pX, pY, pZ) == true then
				local x1, y1 = getScreenFromWorldPosition(pX, pY, pZ + 0.4)
				local x2, y2 = getScreenFromWorldPosition(pX, pY, pZ + 0.3)
				dxDrawText(getPlayerName(v), x1, y1, x1, y1, tocolor(r, g, b), 1.7, "default-bold", "center")
				dxDrawRectangle(x2 - 100, y2, 200, 25, tocolor(2, 2, 2, 170))
				dxDrawRectangle(x2 - 99, y2 + 1, 198 / 100 * getElementHealth(v), 23, tocolor(120, 0, 0, 180))
			end
		end
	end
end	
end)
]]--