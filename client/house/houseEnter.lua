local screen = {guiGetScreenSize()}
cHouse = {}
cHouse.gui = {
	window = {},
	button = {},
	label = {}
}

function cHouse.func(player) --|| Source = House
	local house = source
	--outputChatBox("Du hast das Haus in "..tostring(getElementData(house, "Ort")).." mit der ID "..tostring(getElementData(house, "ID")).." betreten!")
	cHouse.gui.window[1] = sosaGUI.window:new((screen[1] - 250) / 2, (screen[2] - 350) / 2, 250, 350, "")
	cHouse.gui.label[1] = sosaGUI.label:new(cHouse.gui.window[1], 0, 20, 250, 60, "Du stehst vor dem Haus von "..getElementData(house, "Owner")..".")
	cHouse.gui.button[1] = sosaGUI.button:new(cHouse.gui.window[1], 10, 310, 90, 30, "Betreten", function() 
		triggerServerEvent("sHouse:enter", getLocalPlayer(), house) 
		sosaGUI.terminate(cHouse.gui.window[1]) 
	end)
end


addEvent("cHouse:call", true)
addEventHandler("cHouse:call", root, cHouse.func)

