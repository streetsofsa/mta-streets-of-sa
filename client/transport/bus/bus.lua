cBus = {}
cBus.events = {
	"vehicles.bus:new",
	"vehicles.bus:changeRoute"
}
table.foreach(cBus.events, function(_, v)
	addEvent(v, true)
end)


function cBus:new(...)
	local obj = setmetatable({}, {__index = self})
	if obj.constructor then
		obj:constructor()
	end
	return obj
end

function cBus:onChange()
	local route = getElementData(self.veh, "Route")
	
	if route[1] == 0 then
		engineRemoveShaderFromWorldTexture(self.shader, "dxn1-route", self.veh)
		return
	end
	
	if self.txd then
		engineRemoveShaderFromWorldTexture(self.shader, "@hite", self.veh)
	end	
	 
	
	self.txd = dxCreateTexture("resources/textures/trans/"..tostring(route[1]).."."..tostring(route[2])..".px")
	dxSetShaderValue(self.shader, "FX_TEXTURE", self.txd)
	
	engineApplyShaderToWorldTexture(self.shader, "@hite", self.veh)
end

function cBus:constructor()
	self.veh = source
	self.route = route
	self.shader = dxCreateShader("resources/shaders/text.fx")
	
	
	self.changeFunc = function()
		if source == self.veh then
			self:onChange()
		end
	end
	
	self.event = addEventHandler("vehicles.bus:changeRoute", root, self.changeFunc)
	self:onChange()
end

addEventHandler("vehicles.bus:new", root, function()
	cBus:new(source)
end)


--core.writePX("resources/textures/trans/1.1.px","resources/textures/trans/4.1.px")


function cBus.loadBus()
	local f = {}
	f[1] = engineLoadTXD("resources/models/bus.txd")
	engineImportTXD(f[1], 431)
	f[2] = engineLoadDFF("resources/models/bus.dff")
	engineReplaceModel(f[2], 431)

--[[	--local pl = dxGetTexturePixels(txd, 31, 427, 450, 50)
	local f2 = fileOpen("2_1.px")
	local read = fileRead(f2, fileGetSize(f2))
	local pxls = dxConvertPixels(read, "plain")
	--outputChatBox(tostring(read))
	
	dxSetTexturePixels(txd, pxls, 31, 427, 450, 50)
	fileClose(f2)]]--
	
--[[	local file = fileCreate("2_1.px")
	fileWrite(file, dxConvertPixels(pl, "png"))
	fileClose(file) ]]--

end

addEventHandler("onClientResourceStart", resourceRoot, cBus.loadBus)

