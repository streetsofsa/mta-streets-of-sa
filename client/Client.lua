ClientClass = {};

function ClientClass:init()
	local obj = setmetatable ( {}, {__index = self} )
	if obj.constructor then
		obj:constructor()
	end
	return obj
end


function ClientClass:trigger(...)
	triggerServerEvent(...)
end

function ClientClass:getData(e, i)
	if self.Datas[e][i] then
		return self.Datas[e][i]
	end
end

function ClientClass:playSound(sound)
	if string.lower(Client.sounds[sound]) then
		return playSound(string.lower(Client.sounds[sound]))
	end
	return "Sound not found!"
end
	
function ClientClass:refreshElementData(e, i, v)
	if not self.Datas[e] then
		self.Datas[e] = {}
	end
	--outputChatBox(string.format("[CLIENT]index: %s, value: %s, elementTable: %s, dataTable: %s", tostring(i), tostring(v), tostring(self.Datas[e]), tostring(self.Datas[e][i])), 255, 104, 4)
	self.Datas[e][i] = v		
end	
	
	
function ClientClass:constructor()
	
	self.Datas = {}
	
	self.events = {
		"client:receiveData"
	}
	table.foreach(self.events, function(_, v)
		addEvent(v, true)
	end)
	
	
	self.redfunc = function(i, v) self:refreshElementData(source, i, v) end
	
	
	addEvent("client:receiveData", true)
	addEventHandler("client:receiveData", root, self.redfunc)
	
	--return self
end


addEventHandler("onClientResourceStart", root, function()
	Client = ClientClass:init()
end)





