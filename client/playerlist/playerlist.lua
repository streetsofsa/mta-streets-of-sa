
playerlist = {}
playerlist.__index = playerlist

local sx, sy = guiGetScreenSize ()

function playerlist:new (...)
	local obj = setmetatable ( {},{__index = self} )
	if obj.constructor then
		obj:constructor (...)
	end
	return obj
end

function playerlist:render ()
	if not self.isShowing then
		return
	end
	local players = getElementsByType('player')
	self.players = {}
	for i = 1, #players do
		self.players[i] = {}
		self.players[i].name = getPlayerName(players[i])
		self.players[i].state = getPlayerData(players[i], "loggedIn")
		self.players[i].playtime = tonumber(getPlayerData(players[i], "Spielzeit")) or '-/-'
		self.players[i].unternehmen = tonumber(getPlayerData(players[i], "Unternehmen")) or '-/-'
		self.players[i].fraktion = tonumber(getPlayerData(players[i], "Faction")) or "-/-"
		self.players[i].ping = getPlayerPing ( players[i] )
	end
	
	
	dxDrawImage (self.x,self.y,self.width,self.height, "resources/images/scoreboard/scoreboard.png", 0, 0, 0, tocolor(255, 255, 255, 245))
	
	local offSet = 0
	
	for i = 1 + self.offSet, 10 + self.offSet do
		if self.players[i] and self.players[i].state == true then
			local r,g,b
			if getPlayerData(players[i], "loggedIn") then
				if self.players[i].fraktion > 0 then
					r,g,b = unpack(Fraktionen.settings.colors[self.players[i].fraktion])
				else
					r,g,b = 255, 255, 255
				end
			else
				r,g,b = 255, 255, 255
			end
			--|| Name ||--
			local name = self.players[i].name
			if getPlayerData(players[i], "Adminlevel") > 0 then
				name = "[S-Team]"..name
			end
			dxDrawText (name,self.x + 23,self.y + 140 + (offSet*21),self.x + 176,0,tocolor(r,g,b),1.2,'arial', "center")
			dxDrawText (self.players[i].ping,self.x + 659,self.y + 140 + (offSet*21),self.x + 773,0,tocolor(r,g,b),1.25,'arial', "center")
			
			if getPlayerData(players[i], "loggedIn") then
				--|| Spielzeit ||--
				local playTime = self.players[i].playtime
				local h = math.floor(playTime/60)
				local m = string.format("%02d", math.floor(playTime-(h*60)))
				local playTime = h..":"..m			
				dxDrawText (playTime,self.x + 183,self.y + 140 + (offSet*21),self.x + 328,0,tocolor(r,g,b),1.25,'arial', "center")
				
				--|| Status ||--
				dxDrawText(unternehmen.name[self.players[i].unternehmen],self.x + 335,self.y + 140 + (offSet*21),self.x + 494,0,tocolor(r,g,b),1.25,'arial', "center")
				
				--|| Fraktion ||--
				if self.players[i].fraktion > 0 then
					dxDrawText(Fraktionen.settings.name[self.players[i].fraktion],self.x + 501,self.y + 140 + (offSet*21),self.x + 652, 0,tocolor(r,g,b),1.25,'arial', "center")
				else
					dxDrawText("-/-",self.x + 501,self.y + 140 + (offSet*21),self.x + 652, 0,tocolor(r,g,b),1.25,'arial', "center")
				end
			end
			
			
			offSet = offSet + 1
		end
	end
end



function playerlist:getOnlineplayers ()
	local prev = 0
	
	for key, value in next, getElementsByType('player') do
		prev = prev + 1
	end
	return prev
end

function playerlist:constructor ()
	self.width = 800
	self.height = 480
	self.x = sx/2-(self.width/2)
	self.y = sy/2-(self.height/2)
	self.isShowing = false
	self.players = {}
	self.offSet = 0
	
	bindKey ('mouse_wheel_up','down', function ()
		if self.offSet - 1 >= 0 then
			self.offSet = self.offSet - 1
		end
	end)	
	
	bindKey ('mouse_wheel_down','down', function ()
		if self.offSet + 1 <= #getElementsByType('player')-16 then
			self.offSet = self.offSet + 1
		end
	end)
	
	bindKey ('tab','down', function () self.isShowing = true end )
	bindKey ('tab','up', function () self.isShowing = false end )
	
	addEventHandler ('onClientRender', root, function () self:render () end)
end



playerlist:new()
