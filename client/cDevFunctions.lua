
cDevFunctions = {
	ui = {}
}

function cDevFunctions.ui.drawfps(bool)
	if bool == "true" then
	local function _drawFPS()
		if not getCurrentFPS() then
			return
		end
		local sx = guiGetScreenSize()
		local roundedFPS = math.floor(getCurrentFPS())
		
		dxDrawText(roundedFPS, sx-10 - dxGetTextWidth(roundedFPS, 3)-1, 0, 0, 0, tocolor(0, 0, 0), 3)
		dxDrawText(roundedFPS, sx-10 - dxGetTextWidth(roundedFPS, 3)+1, 0, 0, 0, tocolor(0, 0, 0), 3)
		dxDrawText(roundedFPS, sx-10 - dxGetTextWidth(roundedFPS, 3), 1, 0, 0, tocolor(0, 0, 0), 3)
		dxDrawText(roundedFPS, sx-10 - dxGetTextWidth(roundedFPS, 3), 3, 0, 0, tocolor(0, 0, 0), 3)
		
		dxDrawText(roundedFPS, sx-10 - dxGetTextWidth(roundedFPS, 3), 2, 0, 0, tocolor(255, 255, 0), 3)
	end
	
	addEventHandler("onClientHUDRender", root, _drawFPS)
	
	else
	
	end
end


addCommandHandler("drawfps", function()
	cDevFunctions.ui.drawfps("true")
end)


function cDevFunctions.cmd(func, funcArgs)
	outputChatBox("DJAI")
	if sDevFunctions[string.lower(gettok(func, 1, "."))][string.lower(gettok(func, 2, "."))] then
		sDevFunctions[string.lower(gettok(func, 1, "."))][string.lower(gettok(func, 2, "."))](funcArgs)
	else
		outputChatBox("/dev Error: no such function")
	end
end


addEventHandler("dev:executeCommand", root,function() outputChatBox("JAI") cDevFunctions.cmd() end)
addEvent("dev:executeCommand", true)