locations = {}

function locations.render()
	--Eingangsmarker
	if locations.marker == 1 then
	local pos = {getElementPosition(localPlayer)}
	if isLineOfSightClear(pos[1], pos[2], pos[3], locations.datas.x1, locations.datas.y1, locations.datas.z1, true, false) then
		local screen = {getScreenFromWorldPosition(locations.datas.x1, locations.datas.y1, locations.datas.z1 + 0.45, 960)}
		if type(screen[1]) == "number" then
		dxDrawText(locations.datas.name, screen[1] - (dxGetTextWidth(locations.datas.name, 1, "default-bold") / 2), screen[2], screen[1], screen[2], white, 1.03, "default-bold")
		dxDrawText("Drücke 'Enter' zum Betreten", screen[1] - (dxGetTextWidth("Drücke 'Enter' zum Betreten", 1, "default-bold") / 2), screen[2] + 20, screen[1], screen[2] + 20, white, 1.03, "default-bold")
		end
	end
	--Ausgangsmarker
	elseif locations.marker == 2 then
	local pos = {getElementPosition(localPlayer)}
	if isLineOfSightClear(pos[1], pos[2], pos[3], locations.datas.x2, locations.datas.y2, locations.datas.z2) then
		local screen = {getScreenFromWorldPosition(locations.datas.x2, locations.datas.y2, locations.datas.z2 + 0.45, 9)}
		if type(screen[1]) == "number" then
		dxDrawText(locations.datas.name, screen[1] - (dxGetTextWidth(locations.datas.name, 1, "default-bold") / 2), screen[2], screen[1], screen[2], white, 1.03, "default-bold")
		dxDrawText("Drücke 'Enter' zum Verlassen", screen[1] - (dxGetTextWidth("Drücke 'Enter' zum Verlassen", 1, "default-bold") / 2), screen[2] + 20, screen[1], screen[2] + 20, white, 1.03, "default-bold")
		end
	end		
	end
end

addEvent("locations:startRender", true)
addEventHandler("locations:startRender", root, function(datas, marker)
	locations.datas = datas
	locations.marker = marker
	addEventHandler("onClientRender", root, locations.render)
end)

addEvent("locations:stopRender", true)
addEventHandler("locations:stopRender", root, function()
	removeEventHandler("onClientRender", root, locations.render)
end)