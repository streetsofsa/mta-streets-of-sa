local screen = {guiGetScreenSize()}
local native = {screen[1]/1920, screen[2]/1080}

local cVars = {open = false}


adminpanel = {}

function adminpanel:open(...)
	--if not open then
		open = true
		setmetatable({}, {__index = self})
		self:constructor(...)
	--end
end


function adminpanel:constructor(alvl)
	--Class Variables
	self.admin = source
	self.players = getElementsByType("player")
	self.x = (screen[1]/2)-(650/2)
	self.y = (screen[2]/2)-(550/2)
	self.w = 650
	self.h = 550
	
	--Creating GUI Elements

	self.gui = {tabs = {}, buttons = {}, labels = {}, editboxen = {}}

	self.gui = {tabs = {}, buttons = {}, labels = {}, editboxes = {}}

	
	self.gui.window = sosaGUI.window:new(self.x, self.y, self.w, self.h, "Admin Panel")
	
	self.gui.gridlist = sosaGUI.gridlist:new(self.gui.window, 1, 0, self.w/3.8, self.h, "Spieler")
	self.gui.gridlist:addColumn("Spieler : "..tostring(#self.players).." online")
	table.foreach(self.players, function(_, v)
		self.gui.gridlist:addRow(getPlayerName(v))
	end)
	
	self.tab = sosaGUI.tab:new(self.gui.window, (self.w/3.8), 0, self.w - (self.w/3.8), self.h)
	self.gui.tabs[1] = self.tab:addTab("Allgemein")

	self.gui.tabs[2] = self.tab:addTab("Ban/Kick")

	self.gui.tabs[2] = self.tab:addTab("Kick/Ban")

	self.gui.tabs[3] = self.tab:addTab("Fraktionen")
	
	self.gui.labels[1] = sosaGUI.label:new(self.gui.tabs[3], 10, 10, 60, 20, "Fraktion")
	self.gui.editboxen[1] = sosaGUI.editbox:new(self.gui.tabs[3], 70, 10, 30, 30, "", false) 
	self.gui.labels[2] = sosaGUI.label:new(self.gui.tabs[3], 10, 35, 60, 20, "Rang")
	self.gui.editboxen[2] = sosaGUI.editbox:new(self.gui.tabs[3], 70, 35, 20, 20, "", false) 
	
	self.gui.labels[3] = sosaGUI.label:new(self.gui.tabs[1], 10, 10, 60, 40, "Ja...")
	self.gui.labels[2] = sosaGUI.label:new(self.gui.tabs[2], 10, 10, 60, 40, "Kick")

	--self.gui.labels[3] = sosaGUI.label:new(self.gui.tabs[3], 10, 10, 60, 40, "Jobs")
	
	
	

	
	self.gui.editboxes[1] = sosaGUI.editbox:new(self.gui.tabs[3], 50, 20, 20, "", false)
	self.gui.editboxes[2] = sosaGUI.editbox:new(self.gui.tabs[3], 50, 20, 20, "", false)
	self.gui.labels[3] = sosaGUI.label:new(self.gui.tabs[3], 10, 10, 60, 40, "Jobs")

end

addEvent("admin:panel", true)
addEventHandler("admin:panel", root, function(...)
	adminpanel:open(...)
end)
