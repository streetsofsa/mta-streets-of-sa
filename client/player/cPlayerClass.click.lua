--||||||||||||||||||||||||||||||||||||
--|||Name: Clientside playersystem |||
--|||Project: MTA Streets of SA    |||
--|||Author:                       |||
--|||Version:                      |||
--||||||||||||||||||||||||||||||||||||



function cPlayerClass.click:initialize()
	local obj = setmetatable({}, {__index = self})
	obj:constructor()
	return obj
end

function cPlayerClass.click:handler(_, _, absX, absY, wX, wY, wZ)
	self.cPos = {absX, absY}
	local cM = {getCameraMatrix()}
	local _,_,_,_,ele = processLineOfSight(cM[1], cM[2], cM[3], wX, wY, wZ)
	self.cSelectedElement = ele
end


function cPlayerClass.click:renderHandler()
	--dxDrawImage(self.cPos[1], self.cPos[2], 17, 28, "resources/images/gui/mouse_left.png")
	
end

function cPlayerClass.click:toggleCursor()
	if #sosaGUI.getWindows() > 0 then
		return
	end
		showCursor(not isCursorShowing()) 
		if isCursorShowing() then
			
			table.foreach(getEventHandlers("onClientRender", root), function(i, v)
				if v == self.c_render then
					return
				end
			end)
			outputChatBox("ADDING EVENT")
			addEventHandler("onClientRender", root, self.c_render)
		else
			outputChatBox("REMOVING EVENT")
			table.foreach(getEventHandlers("onClientRender", root), function(i, v)
				if v == self.c_render then
					removeEventHandler("onClientRender", root, self.c_render)
				end
			end)
		end
end


function cPlayerClass.click:constructor()
	self.cPos = {}
	self.cSelectedElement = nil
	
	self.mHandler = function(...) 
		if isCursorShowing() then
			self:handler(...) 
		end
	end
	
	self.c_render = function()
		if isCursorShowing() and self.cSelectedElement then 
			self:renderHandler() 
		end 
	end
	
	self.toggle = function(...) 
		if #sosaGUI.getWindows() == 0 then --prevent rendering while open wnd
			self:toggleCursor(...)
		end
	end	
	
	
	addEventHandler("onClientCursorMove", root, self.mHandler)
	
	bindKey("ralt", "down", self.toggle)
end


setDevelopmentMode(true)