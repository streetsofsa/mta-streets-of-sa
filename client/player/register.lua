local sX, sY = guiGetScreenSize ()
local regRender = {
	logo = {
		x = ((sX - 1133) / 2) * (sX / 1920),
		y = 10,
		width = 1133 * (sX / 1920),
		height = 185 * (sY / 1080)},
	starten = {
		x = (sX - ((212* (sX / 1920)))) / 2,
		y = ((sY - (93* (sY / 1080)))),
		width = (212*(sX / 1920)),
		height = (93* (sY / 1080))},
	rules = {
		x = ((sX - ((212* (sX / 1920)))) / 2) - (212* (sX / 1920)),
		y = ((sY - (93* (sY / 1080)))),
		width =  (122* (sX / 1920)),
		height = (93* (sY / 1080))},
	info = {
		x = ((sX - ((212* (sX / 1920)))) / 2) + (212* (sX / 1920) * 1.5),
		y = ((sY - (93* (sY / 1080)))),
		width = (63* (sX / 1920)),
		height = (93* (sY / 1080))}		
}
local regB_x = 1
local regB_y = 1
local regelnB_x = 1
local regelnB_y = 1
local infoB_x = 1
local infoB_y = 1

local function introHandler()
	fadeCamera(false, 0.5)
	removeEventHandler("onClientClick", getRootElement(), regClicked)
	removeEventHandler("onClientRender", getRootElement(), createFJPanel)
end

local function regClicked(btn, state, absX, absY, wX, wY, wZ, cE)
	if (absX > regRender.starten.x) and (absX < (regRender.starten.x + regRender.starten.width)) and (absY > regRender.starten.y) and (absY < (regRender.starten.y + regRender.starten.width)) then
		--Registrieren Button gedrückt
		introHandler()
		
	elseif (absX > regRender.rules.x) and (absX < (regRender.rules.x + regRender.rules.width)) and (absY > regRender.rules.y) and (absY < (regRender.rules.y + regRender.rules.height)) then
		--Regeln Button gedrückt
		sosaCreateWindow((sX - 500) / 2, (sY - 400) / 2, 500, 400, "Regeln", true)
		
	elseif (absX > regRender.info.x) and (absX < (regRender.info.x + regRender.info.width)) and (absY > regRender.info.y) and (absY < (regRender.info.y + regRender.info.height)) then
		--Info Button gedrückt
		sosaCreateWindow((sX - 600) / 2, (sY - 350) / 2, 600, 350, "Infos", true)
	end
end	

function createFJPanel()
	--dxDrawImage(regRender.logo.x, regRender.logo.y, regRender.logo.width, regRender.logo.height, "resources/images/logo.png", 0, 0, 0, tocolor(255, 255, 255, 200))
	--dxDrawImage(0, 0, sX, sY, "resources/images/registerlogin/weichzeichner.png")
	dxDrawImage(0, sY - (93* (sY / 1080)), sX, (93* (sY / 1080)) + 5, "resources/images/registerlogin/board.png")
	dxDrawImage(regRender.starten.x, regRender.starten.y, regRender.starten.width, regRender.starten.height, "resources/images/registerlogin/start.png")
	dxDrawImage(regRender.rules.x, regRender.rules.y, regRender.rules.width, regRender.rules.height, "resources/images/registerlogin/regeln.png")
	dxDrawImage(regRender.info.x, regRender.info.y, regRender.info.width, regRender.info.height, "resources/images/registerlogin/info.png")
end
function tttest()
	outputChatBox("JJ")
	triggerEvent("onServerRequestRegister", getRootElement())
end
addCommandHandler("regrender", tttest)

addEvent("onServerRequestRegister", true)
addEventHandler("onServerRequestRegister", root, function()
	showCursor(true)
	outputChatBox("HH")
	fadeCamera(true, 1.5)
	setCameraMatrix(1684.8128662109, 2380.5991210938, 49.1, 0, 0, 0)
	addEventHandler("onClientRender", getRootElement(), createFJPanel)
	addEventHandler("onClientClick", getRootElement(), regClicked)
end)




--[[local settings = {}

settings.w,settings.h = 800, 600

settings.x, settings.y = x/2-(settings.w/2),y/2-(settings.h/2)

settings.visible = false

settings.currentSelectEdit = 1

settings.textContent_1 = ''
settings.textContent_2 = ''

-- // 1 = first Password
-- // 2 = repeat password

addEventHandler ('onClientRender', root, function ()
	if not settings.visible then
		return
	end
	
	showCursor ( true )
	
	dxDrawRectangle (0,0,x,y*100/1080,tocolor(0,0,0,200))
	dxDrawRectangle (0,y-y*100/1080,x,y*100/1080,tocolor(0,0,0,200))
	
	--drawOutline (settings.x,settings.y,settings.w,settings.h,tocolor(0,0,0))
	dxDrawImage (settings.x,settings.y,settings.w,settings.h,'resources/images/startenandlogin/starten.png')
	--drawOutline (settings.x,settings.y,settings.w,35,tocolor(0,0,0))
	--dxDrawRectangle (settings.x,settings.y,settings.w,35,tocolor(50,50,50))
	local text = 'starten'
	local forCalc = dxGetTextWidth (text,1.5,'arial')
	--drawTextOutline ('starten',settings.x+(settings.w/2-(forCalc/2)),settings.y+(dxGetFontHeight(1.5,'arial')/4),0,0,tocolor(0,0,0),1.5,'arial')
	--dxDrawText ('starten',settings.x+(settings.w/2-(forCalc/2)),settings.y+(dxGetFontHeight(1.5,'arial')/4),0,0,tocolor(255,255,255),1.5,'arial')
	
	-- // username
		drawOutline 	(settings.x+(settings.w/2)-(200/2),settings.y+285,200,25,tocolor(0,0,0,25))
		dxDrawRectangle (settings.x+(settings.w/2)-(200/2),settings.y+285,200,25,tocolor(255,255,255,125))
		dxDrawText 		(getPlayerName(localPlayer),settings.x+(settings.w/2)-(195/2),settings.y+285+(dxGetFontHeight()/2),0,0,tocolor(0,0,0))
	-- //	
	
	-- // first password
		drawOutline 	(settings.x+(settings.w/2)-(200/2),settings.y+370,200,25,tocolor(0,0,25))
		dxDrawRectangle (settings.x+(settings.w/2)-(200/2),settings.y+370,200,25,tocolor(255,255,255,125))
		local password = settings.textContent_1
		if not isCursorOverArea ( settings.x+(settings.w/2)-(200/2),settings.y+370,200,25 ) then
			password = password:gsub ('.', '*')
		end
		dxDrawText 		(password..'|',settings.x+(settings.w/2)-(195/2),settings.y+370+(dxGetFontHeight()/2),0,0,tocolor(0,0,0))
		if settings.currentSelectEdit == 1 then
			dxDrawRectangle (settings.x+(settings.w/2)-(200/2),settings.y+370,200,25,tocolor(255,255,0,50))
		end
	-- //	
	
	-- // repeat password
		drawOutline 	(settings.x+(settings.w/2)-(200/2),settings.y+175,200,25,tocolor(0,0,0))
		dxDrawRectangle (settings.x+(settings.w/2)-(200/2),settings.y+175,200,25,tocolor(255,255,255))
		local password = settings.textContent_2		
		if not isCursorOverArea ( settings.x+(settings.w/2)-(200/2),settings.y+175,200,25 ) then
			password = password:gsub ('.', '*')
		end		
		dxDrawText 		(password..'|',settings.x+(settings.w/2)-(195/2),settings.y+175+(dxGetFontHeight()/2),0,0,tocolor(0,0,0))
		if settings.currentSelectEdit == 2 then
			dxDrawRectangle (settings.x+(settings.w/2)-(200/2),settings.y+175,200,25,tocolor(255,255,0,50))
		end		
	-- //
	-- // confirm button
		drawOutline 	(settings.x+(settings.w/2)-(200/2)+200,settings.y+525,200,40,tocolor(0,0,0,25))
		dxDrawRectangle (settings.x+(settings.w/2)-(200/2)+200,settings.y+525,200,40,tocolor(200,200,200,50))
		local password = settings.textContent_2		
		if isCursorOverArea ( settings.x+(settings.w/2)-(200/2)+200,settings.y+525,200,40 ) then	
			drawTextOutline ('Registrieren',settings.x+(settings.w/2)-(200/2)+(200/2)-(dxGetTextWidth('Registrieren')/2)+200,settings.y+525+(40/3),0,0,tocolor(125,125,125,125),1,'default')
		end
		dxDrawText ('Registrieren',settings.x+(settings.w/2)-(200/2)+(200/2)-(dxGetTextWidth('Registrieren')/2)+200,settings.y+525+(40/3),0,0,tocolor(0,0,0))
	-- //
end)

addEventHandler ('onClientKey', root, function (key,std)
	if not settings.visible then
		return
	end

	if std then
		
		local text;
		
		if settings.currentSelectEdit == 1 then
			text = settings.textContent_1
		else
			text = settings.textContent_2
		end
		
		if key:len() == 1 then
			text = text..key
		elseif key == 'space' then
			text = text..' '
		elseif key == 'backspace' then
			text = text:sub(1,#text-1)
		end
		
		if settings.currentSelectEdit == 1 then
			settings.textContent_1 = text
		else
			settings.textContent_2 = text
		end	
	end	
	
end)
-- onPlayerWantTostarten
local function checkIfPasswordsEqual ()
	local pw1 = settings.textContent_1
	if pw1:len () > 3 and pw1:len () < 16 then
		triggerServerEvent ('requestServerForstarten',localPlayer,pw1)
	else
		outputChatBox ('Das Passwort darf zwischen 3 und 16 Zeichen besitzen !',125,0,0)
	end
end

addEvent ('onServerRequeststarten', true)
addEventHandler ('onServerRequeststarten', localPlayer, function ( boolean )
	fadeCamera ( true )
	settings.visible = boolean
	showCursor ( boolean )
end)

addEventHandler ('onClientClick', root, function (btn,std)
	if not settings.visible then
		return
	end

	if btn == 'left' and std == 'up' then
		if isCursorOverArea (settings.x+(settings.w/2)-(200/2),settings.y+370,200,25) then
			settings.currentSelectEdit = 1
		elseif isCursorOverArea (settings.x+(settings.w/2)-(200/2)+200,settings.y+525,200,40) then
			checkIfPasswordsEqual ()
		end
	end
end)]]--