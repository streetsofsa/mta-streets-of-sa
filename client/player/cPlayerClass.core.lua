--||||||||||||||||||||||||||||||||||||
--|||Name: Clientside playersystem |||
--|||Project: MTA Streets of SA    |||
--|||Author:                       |||
--|||Version:                      |||
--||||||||||||||||||||||||||||||||||||

cPlayerClass = {click = {}, hud = {}, skin = {}}

cPlayerClass.events = {
	"requestLogin",
	"acceptLogin",
	"requestRegister",
	"updateData",
	"wasted",
	"payday",
	"new",
	"skin.get",
	"openInventory"
}
table.foreach(cPlayerClass.events, function(_, v)
	addEvent("c.playerClass:"..v, true)
end)


function cPlayerClass:new(...)
	local obj = setmetatable({}, {__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
	return obj
end

function cPlayerClass:setDatas(data, value)
	self.cDatas[data] = value
end

function cPlayerClass:trigger(cmd, ...)
	triggerServerEvent("s.playerClass:"..cmd, self.c_player, ...)
end

function cPlayerClass:wasted(ammo, killer, killerWeapon, bpart)
	outputChatBox(tostring(source).." : "..tostring(ammo).." : "..tostring(killer).." : "..tostring(killerWeapon).." : "..tostring(bpart))
end

function cPlayerClass:payday()

end


function cPlayerClass:constructor()
	
	--outputChatBox("[SOSA]: Creating client-side player class for "..localPlayer:getName().."!", 200) 
	
	self.cDatas = {}
	self.c_player = localPlayer
	
	
	
	addEventHandler("c.playerClass:wasted", root, function(...) self:wasted(...) end)
	addEventHandler("c.playerClass:payday", root, function(...) self:payday(...) end)
	addEventHandler("c.playerClass:updateData", root, function(data)
		if (source == self.c_player) then
			self.cDatas = data
		end
	end)
	
	addEventHandler("c.playerClass:acceptLogin", root, function() 
		cLogin.destructor()
		self.c_skin = self.skin:initialize()
		self.c_click = self.click:initialize()
		self.c_hud = self.hud:create()
	end)
	addEventHandler("c.playerClass:requestLogin", root, cLogin.constructor)
end



addEventHandler("c.playerClass:new", root, function()
	cPlayer = cPlayerClass:new()
end)


addEventHandler("onClientResourceStart", resourceRoot, function()
	--triggerServerEvent("Server:playerJoin", localPlayer)
end)


addCommandHandler("sl", function()
	local t = triggerServerEvent("Server:playerJoin", localPlayer)
end)






