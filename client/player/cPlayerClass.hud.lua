local screen = {guiGetScreenSize()}
local native = {screen[1]/1920, screen[2]/1080}

local HUDCOMPONENTS = {
	["main"] = {visible=1, sx=0, sy=screen[2]-(30*native[2]), width=screen[1], height=30*native[2], alpha=255,name="Main", bx=screen[1]-(30*native[1]), by=screen[2]-(30*native[2]), bw=30*native[1], bh=30*native[2]},
	["money"] = {visible=1, sx=screen[1]-20-dxGetTextWidth("$99999999", 1, core.font[2]), sy=170*native[2], width=dxGetTextWidth("$99999999", 1, core.font[2]), height=48*native[2], alpha=255, name="Geld"},
	["radar"] = {visible=1, sx=25, sy=screen[2]-(240*native[2])-(30*native[2])-20, width=410*native[1], height=250*native[1], alpha=255, name="Minimap", msizex=3000, msizey=3000, mzoom=13},
	["clock"] = {visible=1, sx=0, sy=screen[2]-(800*native[2]), width=screen[1]/4.5, height=30*native[2], alpha=255, name="Uhr"},
	["weapons"] = {visible=1, sx=screen[1]-(320*native[1]), sy=20, width=320*native[1], height=170*native[2], alpha=255, name="Waffen"},
	["wanteds"] = {visible=1, sx=screen[1]-15, sy=10, width=6*(35*native[1]), height= 35, alpha=255, name="Wanteds"}
}




function cPlayerClass.hud:create()
	local obj = setmetatable ({},{__index = self} )
	if obj.constructor then
		obj:constructor()
	end
	return obj
end



function cPlayerClass.hud:loadSettings()
	self:log("HUD gestartet!")
	
	table.foreach(HUDCOMPONENTS, function(i, v)
	
		--self:log("Lade Einstellungen für Komponente '"..i.."'...");
		
		--|| Checking, if file exists
	--[[	if fileExists("saved/hud/"..i..".ini") then
			self:log("Datei '".."saved/hud/'"..i.."'.ini".."' gefunden! Lade Einstellungen...")
			
			--||Loading settings
			self.files[i] = fileOpen("saved/hud/"..i..".ini")
			fileSetPos(self.files[i], 11)
			
			self.components[i] = fromJSON("[ { "..fileRead(self.files[i],fileGetSize(self.files[i])).." } ]")
			fileClose(self.files[i])
			
			self:log("Einstellungen für Komponente '"..i.."' geladen!")
		else]]--
			--self:log("Datei '".."saved/hud/"..i..".ini".."' nicht gefunden! Erstelle...")
			--Create file
			self.files[i] = fileCreate("saved/hud/"..i..".ini")
			fileWrite(self.files[i], "[settings]:"..string.sub(toJSON(v), 5, #toJSON(v)-4))
			
			--cPlayerClass.hud:log("Datei '".."saved/hud/'"..i.."'.ini".."' erfolgreich erstellt! Lade Einstellungen...")

			--||Loading settings
			fileSetPos(self.files[i], 11)
			
			self.components[i] = fromJSON("[ { "..fileRead(self.files[i],fileGetSize(self.files[i])).." } ]")
			fileClose(self.files[i])	
			
			--self:log("Einstellungen für Komponente '"..i.."' geladen!")
	--	end

	end)	
	
	self:initialize()
end



function cPlayerClass.hud:initialize()

	--|| Map ||--
	self.components["radar"].map = dxCreateTexture("resources/images/hud/radar/map.jpg")
	self.components["radar"].mask = dxCreateTexture("resources/images/hud/radar/radar_mask.png")
	
	self.components["radar"].fx = dxCreateShader("resources/shaders/mask.fx")
		dxSetShaderValue(self.components["radar"].fx, "SFX_TXD", self.components["radar"].map)
		dxSetShaderValue(self.components["radar"].fx, "SFX_MASK", self.components["radar"].mask)
	--
	
	--||||||||||--
	
	self.renderfunc = function(...) 
		--|| HUD Main ||--
		if self.components["main"].visible == 1 then
			self:render_main()
		end
		--|| HUD Radar ||--
		if self.components["radar"].visible == 1 then
			self:render_radar()
		end
		--|| HUD Clock ||--
		if self.components["clock"].visible == 1 then
			self:render_clock()
		end
		--|| HUD Wanteds ||--
		if self.components["wanteds"].visible == 1 then
			self:render_wanteds()
		end		
		--|| HUD Money ||--
		if self.components["money"].visible == 1 then
			self:render_money()
		end		
		--|| HUD Weapons ||--
		if self.components["weapons"].visible == 1 then
			self:render_weapons()
		end
	end
	self.clickfunc = function(...) self:click(...) end
	
	addEventHandler("onClientClick", root, self.clickfunc)
	addEventHandler("onClientRender", root, self.renderfunc)
end

--||Render HUD Components in seperate functions
function cPlayerClass.hud:render_main()
	--|| HUD Main ||--
	dxDrawRectangle(self.components["main"].sx, self.components["main"].sy, self.components["main"].width, self.components["main"].height, tocolor(15, 15, 15, 220))
	dxDrawText("streets-of-sa.de", self.components["main"].sx, self.components["main"].sy, self.components["main"].width, self.components["main"].sy + self.components["main"].height, white, 1.2, "default-bold", "center", "center")
	dxDrawImage(self.components["main"].bx, self.components["main"].by, self.components["main"].bw, self.components["main"].bh, "resources/images/hud/settings.png")
end


function cPlayerClass.hud:render_radar()
	--|| HUD Radar ||--
	--get datas & apply to shader
	local ppos = {getElementPosition(localPlayer)}
	local data = {
		pos = {(ppos[1])/6000, (ppos[2])/-6000},
		crot = {getElementRotation(getCamera())},
		prot = {getElementRotation(localPlayer)},
		health = getElementHealth(localPlayer),
		armor = getPedArmor(localPlayer)
	}
		dxSetShaderValue(self.components["radar"].fx, "FX_POS", data.pos[1], data.pos[2])
		dxSetShaderValue(self.components["radar"].fx, "FX_SCALE", 1/13, 1/13)
		dxSetShaderValue(self.components["radar"].fx, "FX_ROTATION", math.rad(-data.crot[3]))

	--health&armor bars
	dxDrawRectangle(self.components["radar"].sx+(6*native[1]), self.components["radar"].sy+(6*native[2])+(self.components["radar"].height-(28*native[2])), (self.components["radar"].width-12)/2, 20*native[2], tocolor(100, 255, 100, self.components["radar"].alpha-80))
	dxDrawRectangle(self.components["radar"].sx+(6*native[1])+(self.components["radar"].width-12)/2, self.components["radar"].sy+(6*native[2])+(self.components["radar"].height-(28*native[2])), (self.components["radar"].width-12)/2, 20*native[2], tocolor(80, 155, 255, self.components["radar"].alpha-80))
		
	dxDrawRectangle(self.components["radar"].sx+(6*native[1]), self.components["radar"].sy+(6*native[2])+(self.components["radar"].height-(28*native[2])), ((((self.components["radar"].width-12)/2)/100)*data.health)*native[1], 20*native[2], tocolor(0, 255, 0, self.components["radar"].alpha))
	dxDrawRectangle(self.components["radar"].sx+(6*native[1])+(self.components["radar"].width-12)/2, self.components["radar"].sy+(6*native[2])+(self.components["radar"].height-(28*native[2])), (((((self.components["radar"].width-12)/2)/100)*data.armor))*native[1], 20*native[2], tocolor(0, 0, 255, self.components["radar"].alpha))	
	--draw map
	dxDrawImage(self.components["radar"].sx+(6*native[1]), self.components["radar"].sy+(6*native[2]), self.components["radar"].width-(12*native[1]), self.components["radar"].height-(28*native[2]), self.components["radar"].fx,0,0,0,tocolor(255,255,255,self.components["radar"].alpha))
	--dxDrawImage(self.components["radar"].sx, self.components["radar"].sy, self.components["radar"].width, self.components["radar"].height, "resources/images/hud/radar/radar.png",0,0,0,tocolor(255,255,255,self.components["radar"].alpha))
	--Drawing Cover
	dxDrawRectangle(self.components["radar"].sx, self.components["radar"].sy - 22, self.components["radar"].width, 22+6*native[2], tocolor(10,10,10, 250))
	dxDrawRectangle(self.components["radar"].sx, self.components["radar"].sy+self.components["radar"].height-(2*native[1]), self.components["radar"].width, 6*native[2], tocolor(10,10,10, 250))
		
	dxDrawRectangle(self.components["radar"].sx, self.components["radar"].sy+self.components["radar"].height-(24*native[2]), self.components["radar"].width, 4*native[2], tocolor(10,10,10, 250))
	dxDrawRectangle(self.components["radar"].sx+(self.components["radar"].width/2), self.components["radar"].sy+self.components["radar"].height-(24*native[2]), 2*native[1], 24*native[2], tocolor(10,10,10, 250))
	
	dxDrawRectangle(self.components["radar"].sx, self.components["radar"].sy, 6*native[1], self.components["radar"].height, tocolor(10,10,10, 250))
	dxDrawRectangle(self.components["radar"].sx + self.components["radar"].width-(6*native[1]), self.components["radar"].sy, 6*native[1], self.components["radar"].height, tocolor(10,10,10, 250))
	--
	dxDrawImage(self.components["radar"].sx+(6*native[1])+((self.components["radar"].width-(12*native[1]))/2)-10,
				self.components["radar"].sy+(6*native[2])+((self.components["radar"].height-(28*native[2]))/2)-10,
				20,20, "resources/images/hud/radar/me.png",-data.prot[3]+data.crot[3],0,0,tocolor(255,255,255,self.components["radar"].alpha))
end


function cPlayerClass.hud:render_clock()
	--|| HUD Clock ||--		
	self.time = getRealTime()
	--dxDrawText(string.format("%02d", self.time.hour)..":"..string.format("%02d", self.time.minute),self.components["clock"].sx, self.components["clock"].sy, self.components["clock"].sx + self.components["clock"].width, self.components["clock"].sy + self.components["clock"].height, white, 1, core.font[1], "center", "center")
	dxDrawText(string.format("%02d", self.time.hour)..":"..string.format("%02d", self.time.minute),self.components["radar"].sx+(6*native[1]), self.components["radar"].sy-15, self.components["radar"].sx + self.components["radar"].width, self.components["radar"].sy + 10, white, 1, core.font[1], "left", "bottom")
		
end


function cPlayerClass.hud:render_wanteds()
	--|| HUD Wanteds ||--
	local wanteds = getPlayerWantedLevel()
	
	if wanteds > 0 then
		local anzahl
		for i=1, wanteds do
			anzahl = i
			dxDrawImage((screen[1]-15)-(anzahl*35), self.components["wanteds"].sy, 30, 30, "resources/images/hud/wanteds/0.png")
		end
		
		for i=anzahl+1, 6 do
			anzahl = i
			dxDrawImage((screen[1]-15)-(anzahl*35), self.components["wanteds"].sy, 30, 30, "resources/images/hud/wanteds/1.png")
		end
	end	
end


function cPlayerClass.hud:render_money()
	--|| HUD Money ||--
	money = getPlayerData(localPlayer, "Handgeld") or getPlayerMoney(localPlayer)
	
	dxDrawTextOutline("$"..money, self.components["money"].sx, self.components["money"].sy, self.components["money"].sx + self.components["money"].width, self.components["money"].sy + self.components["money"].height, tocolor(2,2,2,self.components["money"].alpha), 1, core.font[2], "right", "top")
	dxDrawText("$"..money, self.components["money"].sx, self.components["money"].sy, self.components["money"].sx + self.components["money"].width, self.components["money"].sy + self.components["money"].height, tocolor(160,255,160,self.components["money"].alpha), 1, core.font[2], "right")
end


function cPlayerClass.hud:render_weapons()
	--|| HUD Weapons ||--
	local data = {localPlayer:getWeapon(), localPlayer:getTotalAmmo(), getPedAmmoInClip(localPlayer)}
	local iw,il = 128*native[1], 128*native[2]
	if data[1] ~= 0 then
	dxDrawImage(self.components["weapons"].sx, self.components["weapons"].sy, 128, 128, "resources/images/hud/weapon/"..tostring(data[1])..".png")
	--dxDrawTextOutline(tostring(getWeaponNameFromID(data[1])), self.components["weapons"].sx, self.components["weapons"].sy + il, self.components["weapons"].sx + iw, self.components["weapons"].sy + il, tocolor(2,2,2,self.components["weapons"].alpha), 1, core.font[2], "center", "top")
--	dxDrawText(tostring(getWeaponNameFromID(data[1])), self.components["weapons"].sx, self.components["weapons"].sy + il, self.components["weapons"].sx + iw, self.components["weapons"].sy + il, tocolor(240,240,240,self.components["weapons"].alpha), 1, core.font[2], "center")
	dxDrawTextOutline(tostring(getWeaponNameFromID(data[1])), self.components["weapons"].sx + iw, self.components["weapons"].sy + il - 30-40, self.components["weapons"].sx + self.components["weapons"].width, self.components["weapons"].sy + self.components["weapons"].height, tocolor(2,2,2,self.components["weapons"].alpha), 0.9, core.font[2], "left", "top")
	dxDrawText(tostring(getWeaponNameFromID(data[1])), self.components["weapons"].sx + iw, self.components["weapons"].sy + il - 30-40, self.components["weapons"].sx + self.components["weapons"].width, self.components["weapons"].sy + self.components["weapons"].height, tocolor(240,240,240,self.components["weapons"].alpha), 0.9, core.font[2], "left", "top")
	
	dxDrawTextOutline(tostring(data[2] - data[3]).."  |  "..tostring(data[3]), self.components["weapons"].sx + iw, self.components["weapons"].sy + il -30-10, self.components["weapons"].sx + self.components["weapons"].width, self.components["weapons"].sy + il, tocolor(2,2,2,self.components["weapons"].alpha), 0.9, core.font[2], "left", "top")
	dxDrawText(tostring(data[2] - data[3]).."  |  "..tostring(data[3]), self.components["weapons"].sx + iw, self.components["weapons"].sy + il -30-10, self.components["weapons"].sx + iw, self.components["weapons"].sy + self.components["weapons"].height, tocolor(240,240,240,self.components["weapons"].alpha), 0.9, core.font[2], "left", "top")
	end
end


function cPlayerClass.hud:click(btn, state, absX, absY)
if btn ~= "left" or state ~= "down" then
	return
end

if (absX > self.components["main"].bx) and (absX < self.components["main"].bx + self.components["main"].bw) and (absY > self.components["main"].by) and (absY < self.components["main"].by + self.components["main"].bh) then
	self.instance = {wnd = {}}
	self.instance = sosaGUI.window:new(screen[1] - 350, screen[2] - 450, 350, 420, "HUD Einstellungen")
end
	
end


function cPlayerClass.hud:log(text)
	outputDebugString(text)
--	core.log("hud", text)
end


function cPlayerClass.hud:constructor()
	self.files = {}
	self.components = {}
	self.visible = false
	
	self:loadSettings()
end



addCommandHandler("HUD", function()
	cPlayerClass.hud:create()
end)



addEventHandler("onClientResourceStart", root, function()
	setPlayerHudComponentVisible("all", false)
	
end)