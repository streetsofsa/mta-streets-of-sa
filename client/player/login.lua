local screen = {guiGetScreenSize()}
local native = {screen[1]/1920, screen[2]/1080}


local pos = { --{x,y,w,h}
	logo = {((screen[1] - 1133) / 2) * native[1], 10*native[2], 1133 * native[1], 185 * native[2]},
	login = {((screen[1] - dxGetTextWidth("Login", 1, core.font[3])) / 2), screen[2] - (93 * native[2]), dxGetTextWidth("Login", 1, core.font[3]), 93 * native[2]},
	regeln = {((screen[1] - dxGetTextWidth("Regeln", 1, core.font[3])) / 2) - (212*native[1])*1.1, screen[2] - (93 * native[2]), dxGetTextWidth("Regeln", 1, core.font[3]), 93 * native[2]},
	info = {((screen[1] - dxGetTextWidth("Info", 1, core.font[3])) / 2) + (212*native[1]), screen[2] - (93 * native[2]), dxGetTextWidth("Info", 1, core.font[3]), 93 * native[2]},
}


cLogin = {}

function cLogin.renderHandler()
	dxDrawRectangle(0, screen[2] - (93 * native[2]), screen[1], 93 * native[2], tocolor(15,15,15,230))
	
	dxDrawText("Login", pos.login[1], pos.login[2], pos.login[1] + pos.login[3], pos.login[2] + pos.login[4], tocolor(220, 140, 30), 0.8, core.font[3], "center", "center")
	dxDrawText("Regeln", pos.regeln[1], pos.regeln[2], pos.regeln[1] + pos.regeln[3], pos.regeln[2] + pos.regeln[4], tocolor(220, 140, 30), 0.8, core.font[3], "center", "center")
	dxDrawText("Info", pos.info[1], pos.info[2], pos.info[1] + pos.info[3], pos.info[2] + pos.info[4], tocolor(220, 140, 30), 0.8, core.font[3], "center", "center")
end

function cLogin.moveHandler()
	--[[if isCursorOnElement(pos.login[1], pos.login[2], pos.login[3], pos.login[4]) then
		local x, y = interpolateBetween(pos.login[1], pos.login[2], 0, 3, 3, 0, 0, "Linear")
		outputChatBox(tostring(x).."   "..tostring(y))
		pos.login[1] = x
		pos.login[2] = y
	end
	]]--
end


function cLogin.clickHandler(btn, state)
if btn ~= "left" or state ~= "down" then
	return
end
	
	if isCursorOnElement(pos.login[1], pos.login[2], pos.login[3], pos.login[4]) then
		cLogin.open("login")
	--	Client:playSound("click")
		
	elseif isCursorOnElement(pos.regeln[1], pos.regeln[2], pos.regeln[3], pos.regeln[4]) then
		cLogin.open("regeln")
		Client:playSound("click")
		
	elseif isCursorOnElement(pos.info[1], pos.info[2], pos.info[3], pos.info[4]) then
		cLogin.open("info")
		Client:playSound("click")
		
	end
end


function cLogin.open(window)
	cLogin.wnd = {}
	
	if window == "login" then
		
		cLogin.wnd.window = sosaGUI.window:new((screen[1] - 450) / 2, (screen[2] - 250) / 2, 450, 250, "Willkommen zurück!")
		--cLogin.wnd.image = sosaGUI.image:new(cLogin.wnd.window, 10, 10, 430, 80, "resources/images/logo2.png")
		cLogin.wnd.label1 = sosaGUI.label:new(cLogin.wnd.window, 0, 100, 430, 70, "Willkommen zurück!, logge dich hier mit deinem Passwort ein.")
		cLogin.wnd.label2 = sosaGUI.label:new(cLogin.wnd.window, 10, 160, 150, 30, "Passwort:")
		cLogin.wnd.edit = sosaGUI.editbox:new(cLogin.wnd.window, 150, 160, 290, "", true)
		cLogin.wnd.button = sosaGUI.button:new(cLogin.wnd.window, 345, 220, 100, 25, "Login", function() cLogin.login(sosaGetText(cLogin.wnd.edit)) end)
		
	elseif window == "regeln" then
	
	
	elseif window == "info" then
	
	
	end
end


function cLogin.login(pw)
	if string.len(pw) > 3 and string.len(pw) < 16 then
		cPlayer:trigger("requestLogin", pw)

	else
		infobox("Das Passwort muss zwischen 3 und 16 Zeichen besitzen!", "error")
	end
end


function cLogin.constructor()
	--||Creating login interface
	cLogin.shader = shader:new("blur", 0.3)
	
	unbindKey ('tab','down', function () playerlist.isShowing = true end )
	unbindKey ('tab','up', function () playerlist.isShowing = false end )
	
	fadeCamera(true, 1)
	showChat(false)
	showCursor(true)
	
	cLogin.startCameraDrive()
	
	cLogin.clickFunc = function(...) cLogin.clickHandler(...) end
	cLogin.moveFunc = function(...) cLogin.moveHandler(...) end
	cLogin.renderFunc = function(...) cLogin.renderHandler(...) end
	
	
	addEventHandler("onClientClick", root, cLogin.clickFunc)
	addEventHandler("onClientCursorMove", root, cLogin.moveFunc)
	addEventHandler("onClientRender", root, cLogin.renderFunc)
end


function cLogin.destructor()
	fadeCamera(false, 1)
	cLogin.wnd.window:terminate()
	--||Destroy login interface & create UI
	setTimer(function()
	cLogin.shader:destroy()
	
	bindKey ('tab','down', function () playerlist.isShowing = true end )
	bindKey ('tab','up', function () playerlist.isShowing = false end )
	
	fadeCamera(true, 1)
	showChat(true)
	
	removeEventHandler("onClientClick", root, cLogin.clickFunc)
	removeEventHandler("onClientRender", root, cLogin.renderFunc)
	removeEventHandler("onClientCursorMove", root, cLogin.moveFunc)
	--hud
	
	cLogin = nil
	end, 1000, 1)
end


function cLogin.startCameraDrive()


end


addCommandHandler("iu", function() fadeCamera(false, 1) fadeCamera(true, 1) end)




addEventHandler("cLogin:close", root, cLogin.destructor)