local files = {
	[1] = "face.png",
	[2] = "torso.png",
	[3] = "legs.png",
	[4] = "foot.png"
}

function cPlayerClass.skin:initialize(...)
	local obj = setmetatable({}, {__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
	return obj
end

function cPlayerClass.skin:applySkins(data)
	
	
end

function cPlayerClass.skin:constructor()
	
	
	self.update = function(...) self:applySkins(...) end
	
	addEventHandler("c.playerClass:skin.get", root, self.update)
	
end

