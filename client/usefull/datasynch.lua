clientData = {}
playerDatas_c = {}


clientData.events = {
	"clientData_playerData"
}

table.foreach(clientData.events, function(_, v)
	local e = addEvent(v, true)
end)

--||Player||--
function getPlayerData(player, data)
	if playerDatas_c[player] ~= nil then
		return playerDatas_c[player][data]
	else
		return false
	end
end

addEventHandler("clientData_playerData", getRootElement(), function(cData)
	playerDatas_c = cData
end)

