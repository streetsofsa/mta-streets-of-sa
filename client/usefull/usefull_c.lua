local sX, sY = guiGetScreenSize()


function dxDrawTextOutline ( text,x,y,w,h,color,size,font,alignx, aligny)
    local text = string.gsub( text, "#%x%x%x%x%x%x", "" )
	dxDrawText (text,x + 1,y,w,h,color,size,font,alignx, aligny)
	dxDrawText (text,x - 1,y,w,h,color,size,font,alignx, aligny)
	dxDrawText (text,x,y + 1,w,h,color,size,font,alignx, aligny)
	dxDrawText (text,x,y - 1,w,h,color,size,font,alignx, aligny)
end	

function getPlayerFromName(name)
    local name = name and name:gsub("#%x%x%x%x%x%x", ""):lower() or nil
    if name then
        for _, player in ipairs(getElementsByType("player")) do
            local name_ = getPlayerName(player):gsub("#%x%x%x%x%x%x", ""):lower()
            if name_:find(name, 1, true) then
                return player
            end
        end
    end
end


function isCursorOnElement(x,y,w,h)
	if isCursorShowing() then
	local mx,my = getCursorPosition()
	local fullx,fully = guiGetScreenSize()
	cursorx,cursory = mx*fullx,my*fully
		if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
			return true
		else
			return false
		end
	end
end

local fps = false
function getCurrentFPS() -- Setup the useful function
    return fps
end
 
local function updateFPS(msSinceLastFrame)
    -- FPS are the frames per second, so count the frames rendered per milisecond using frame delta time and then convert that to frames per second.
    fps = (1 / msSinceLastFrame) * 1000
end
addEventHandler("onClientPreRender", root, updateFPS)


--Math ex library

function math.odd(num)
	if(num%2 == 0) then
		return false
	end
	return true
end






























