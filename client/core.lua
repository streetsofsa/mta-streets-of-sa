core = {}

core.font = {
	dxCreateFont("resources/fonts/clock.ttf", 18) or "sans-serif",
	dxCreateFont("resources/fonts/hud.ttf", 22) or "sans-serif",
	dxCreateFont("resources/fonts/info.ttf", 32) or "sans-serif"
}


Client = {}
Client.players = {}

--Client
Client.sounds = {
	["click"] = "resources/sounds/gui/click.wav"
}


function Client.playSound(sound)
	if string.lower(Client.sounds[sound]) then
		return playSound(string.lower(Client.sounds[sound]))
	end
	return "Sound not found!"
end




function core.writePX(...)
	tbl = {...}
	local file = {}
	local txt = ""
	
	for i = 1, table.getn(tbl) do
		outputChatBox(tbl[i])
		file[i] = {}
		file[i].file = fileOpen(tbl[i])
		
		--txt = txt..dxConvertPixels(fileRead(file[i].file, fileGetSize(file[i].file)), "plain").."%msapx"
		fileClose(file[i].file)
	end
	
	local eFILE = fileCreate("efile.px")
	fileWrite(eFILE, txt)
	
	fileClose(eFILE)
end


addCommandHandler("ald", function()
	local file = fileOpen("efile.px")
	local png1 = dxConvertPixels(gettok(fileRead(file, fileGetSize(file)), 1, "%msapx"), "png")
	fileClose(file)
	
	
	--local png1 = gettok(text, 1, "%msapx")
	
	local file2 = fileCreate("png01.png")
	fileWrite(file2, png1)
	fileClose(file2)

end)



function core.log(file, text)
	local time = {}
	local rtime = getRealTime()
	local logFile
	time.day = string.format("%02d", rtime.monthday)
	time.month = string.format("%02d", tostring(rtime.month + 1))
	time.year = getRealTime().year + 1900
	time.minute = string.format("%02d", rtime.minute)
	time.hour = string.format("%02d", rtime.hour)
	time.second = string.format("%02d", rtime.second)
	
	if fileExists("logs/debug/"..time.day.."."..time.month.."."..time.year.."/"..file..".log") then
		logFile = fileOpen("logs/debug/"..time.day.."."..time.month.."."..time.year.."/"..file..".log")
	else
		logFile = fileCreate("logs/debug/"..time.day.."."..time.month.."."..time.year.."/"..file..".log")
		logFile = fileOpen("logs/debug/"..time.day.."."..time.month.."."..time.year.."/"..file..".log")
	end
	local size = fileGetSize(logFile)
	fileSetPos(logFile,size)
	fileWrite(logFile,"["..time.hour..":"..time.minute..":"..time.second.."] "..text.."\n")
	fileClose(logFile)
end




addEventHandler("onClientResourceStart", getResourceRootElement(), function()
	setBlurLevel(3)
end)


