local screen = {guiGetScreenSize()}

function isCursorOnElement(x,y,w,h)
	if isCursorShowing() then
	local mx,my = getCursorPosition()
	local fullx,fully = guiGetScreenSize()
	cursorx,cursory = mx*fullx,my*fully
		if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
			return true
		else
			return false
		end
	end
end

sosaGUI = {
	window = {},
	button = {},
	label = {},
	image = {},
	editbox = {},
	guifield = {},
	checkbox = {},
	gridlist = {},
	actionfield = {},
	scrollbar = {},
	tab = {}
}
sosaGUI.window.__index = sosaGUI.window
sosaGUI.button.__index = sosaGUI.button
sosaGUI.label.__index = sosaGUI.label
sosaGUI.image.__index = sosaGUI.image
sosaGUI.editbox.__index = sosaGUI.editbox
sosaGUI.guifield.__index = sosaGUI.guifield
sosaGUI.checkbox.__index = sosaGUI.checkbox
sosaGUI.gridlist.__index = sosaGUI.gridlist
sosaGUI.actionfield.__index = sosaGUI.actionfield
sosaGUI.scrollbar.__index = sosaGUI.scrollbar
sosaGUI.tab.__index = sosaGUI.tab
sosaGUI.__index = sosaGUI
sosaGUI.instances = {}

sosaGUI.windows = {}

function sosaGUI:new(...)
	return sosaGUI.window:new(...)
end

--||  Window  ||--
function sosaGUI.window:new(x,y,w,h,text)
	local data = {}
	data.events = {}	
	data.x = x
	data.y = y
	data.w = w
	data.h = h
	data.text = text
	data.activ = true
	data.selected = true
	data.type = "window"
	data.instance = data
	data.childs = {}
	
	data.clickfunc = function(_, state) data:onClick() end
	data.renderfunc = function() data:onRender() end
	data.cmovefunc = function() data:onMove() end
	
	data.events.click = addEventHandler("onClientClick", root, data.clickfunc)
	data.events.render = addEventHandler("onClientRender", root, data.renderfunc)
--[[	bindKey("mouse1", "down", function()
		if isCursorOnElement(data.x, data.y - 30, data.w - 30, 30) then
			data.moveEvent = addEventHandler("onClientCursorMove", root, data.cmovefunc)
			outputChatBox("ADDED")
		end	
	end)
	
	bindKey("mouse1", "up", function()
		if data.moveEvent then
			removeEventHandler("onClientCursorMove", root, data.cmovefunc)
			outputChatBox("REMOVED")
		end	
	end)]]--
	setmetatable(data, self)
	showCursor(true)
	
	table.insert(sosaGUI.windows, data)
	return data
end

function sosaGUI.window:onRender()
	if not self.activ then
		return
	end
		dxDrawImage(self.x, self.y - 30, self.w, 30, "resources/images/gui/titlebar.png")
		dxDrawText(self.text, self.x, self.y - 30, self.x + self.w, self.y, tocolor(255, 255, 255), 1, "default", "center", "center")
		dxDrawRectangle(self.x, self.y, self.w, self.h, tocolor(10, 10, 10, 200))
		dxDrawImage(self.x + self.w - 27.75, self.y - 27.5, 25, 25, "resources/images/gui/close.png")	
		
		dxDrawLine(self.x, self.y - 30, self.x + self.w, self.y - 30, tocolor(0,0,0), 1)
		dxDrawLine(self.x, self.y + self.h, self.x + self.w, self.y + self.h, tocolor(0,0,0), 1)
		dxDrawLine(self.x, self.y - 30, self.x, self.y + self.h, tocolor(0,0,0), 1)
		dxDrawLine(self.x + self.w, self.y - 30, self.x + self.w, self.y + self.h, tocolor(0,0,0), 1)

end

function sosaGUI.window:onMove()
	if not self.activ then
		return
	end
if isCursorOnElement(self.x, self.y - 30, self.w - 30, 30) then
	
	local cPos = {getCursorPosition()}
	cPos = {cPos[1] * screen[1], cPos[2] * screen[2]}
	
	local nPos = {cPos[1] - self.x, cPos[2] - (self.y - 30)}
	outputChatBox(tostring(cPos[1]).."-"..self.x)
	--cPos = {cPos[1] * screen[1], cPos[2] * screen[2]}
	--local relative = { cPos[1] - self.x, cPos[2] - (self.y - 30)}
	self.x = self.x + nPos[1]
	self.y = self.y + nPos[2]	
	
	outputChatBox("nX: "..nPos[1].."   , nY: "..nPos[2])
	outputChatBox("wX: "..self.x.."    , wY: "..self.y)
	--outputChatBox(self.x*(relative[1]/self.w))

--	self.y = self.y + (relative[2] * (cPos[2] / self.y))
end	
end

function sosaGUI.window:onClick()
	if not self.activ then
		return
	end
		if isCursorOnElement(self.x + self.w - 27.75, self.y - 27.5, 25, 25) then
			self.activ = false
			--sosaGUI.terminate(self)
			self:terminate()
		end	
end

function sosaGUI.window:terminate()
	table.foreach(self.childs, function(i, v)
		if v.type == "tabbar" then
			table.foreach(v.tabs, function(_, v)
				table.foreach(v.childs, function(_, v)
					if v.events.render then
						removeEventHandler("onClientRender", root, v.renderfunc)	
					end		
					if v.events.cmove then
						removeEventHandler("onClientCursorMove", root, v.cmovefunc)		
					end
		
					if v.events.click then
						removeEventHandler("onClientClick", root, v.clickfunc)
					end		
				end)
			end)
		end
		if v.events.render then
			removeEventHandler("onClientRender", root, v.renderfunc)	
		end		
		if v.events.cmove then
			removeEventHandler("onClientCursorMove", root, v.cmovefunc)		
		end
		
		if v.events.click then
			removeEventHandler("onClientClick", root, v.clickfunc)
		end
		v = nil
	end)
	removeEventHandler("onClientClick", root, self.clickfunc)
	removeEventHandler("onClientRender", root, self.renderfunc)
	self = nil
	showCursor(false)
	table.remove(sosaGUI.windows)
end





--||||||||||||||||||||||||||||||||||||--
--|||||||||||||||Button|||||||||||||||--
--||||||||||||||||||||||||||||||||||||--


--||  Button  ||--
function sosaGUI.button:new(instance,x,y,w,h,text,func)
	local data = {}
	data.events = {}	
	data.x = x + instance.x
	data.y = y + instance.y
	data.w = w
	data.h = h
	data.text = text
	data.func = func
	data.activ = instance.activ
	data.selected = false
	data.type = "button"
	data.instance = instance
	table.insert(data.instance.childs, data)
	
	data.clickfunc = function(_, state) data:onClick(button, state) end
	data.renderfunc = function() data:onRender() end
	data.cmovefunc = function() data:onMove() end
	
	data.events.click = addEventHandler("onClientClick", root, data.clickfunc)
	data.events.cmove = addEventHandler("onClientCursorMove", root, data.cmovefunc)
	data.events.render = addEventHandler("onClientRender", root, data.renderfunc)
	
	setmetatable(data, self)
	
	return data
end

function sosaGUI.button:onRender()
	if not self.instance.activ then
		return
	end
		dxDrawImage(self.x, self.y, 14*(self.h/74), self.h, "resources/images/gui/buttonLeft.png")
		dxDrawImage(self.x + self.w - (14*(self.h/74)), self.y, 14*(self.h/74), self.h, "resources/images/gui/buttonRight.png")
		for i = 0, (self.w + 2 - (2*(14*(self.h/74)))) do
			dxDrawImage(self.x + (14*(self.h/74)) + i - 1, self.y, 1, self.h, "resources/images/gui/buttonMiddle.png")
		end
		dxDrawText(self.text, self.x, self.y, self.x + self.w, self.y + self.h, white, 1, "default", "center", "center", false, true)

end

function sosaGUI.button:onMove()
	if not self.instance.activ then
		return
	end
	
	if isCursorOnElement(self.x, self.y, self.w, self.h) then
		self.selected = true
	else
		self.selected = false
	end
end

function sosaGUI.button:onClick(button, state)
	if not self.instance.activ or state ~= "down" then
		return
	end
	if isCursorOnElement(self.x, self.y, self.w, self.h) then
		if self.func then
		--	Client:playSound("click")
			self.func()
		end		
	end	
end

--||||||||||||||||||||||||||||||||||||--
--||||||||||||||||Label|||||||||||||||--
--||||||||||||||||||||||||||||||||||||--

--||  Label  ||--
function sosaGUI.label:new(instance,x,y,w,h,text, font, alignx, aligny)
if not alignx then alignx = "center" end
if not aligny then aligny = "center" end
if not font then font = "default" end
	local data = {}
	data.events = {}	
	data.x = x + instance.x
	data.y = y + instance.y
	data.w = w
	data.h = h
	data.text = text
	data.alignx = alignx
	data.aligny = aligny
	data.font = font
	data.activ = instance.activ
	data.type = "label"
	data.instance = instance
	table.insert(data.instance.childs, data)
	
	data.renderfunc = function() data:onRender() end
	data.events.render = addEventHandler("onClientRender", root, data.renderfunc)
	
	setmetatable(data, self)
	
	return data
end

function sosaGUI.label:onRender()
	if not self.instance.activ then
		return
	end
	dxDrawText(self.text, self.x, self.y, self.x + self.w, self.y + self.h, white, 1, self.font, self.alignx, self.aligny, false, true)
end

function sosaGUI.label:setText(text)
	self.text = text
end

function sosaGUI.label:getText()
	return self.text
end

--||  Editbox  ||--
function sosaGUI.editbox:new(instance,x,y,w,text,masked)
	if not masked then masked = false end
	local data = {}
	data.events = {}
	data.x = x + instance.x
	data.y = y + instance.y
	data.w = w
	data.h = 30 + instance.y
	data.text = text 
	data.activ = instance.activ
	data.textactiv = false
	data.type = "editbox"
	data.instance = instance
	data.masked = masked
	table.insert(data.instance.childs, data)
	
	data.clickfunc = function(_, state) data:onClick(state) end	
	data.renderfunc = function() data:onRender() end
	data.keyfunc = function(key, state) data:onKey(key, state) end
	data.events.render = addEventHandler("onClientRender", root, data.renderfunc)
	data.events.click = addEventHandler("onClientClick", root, data.clickfunc)
	data.events.key = addEventHandler("onClientKey", root, data.keyfunc)
	
	setmetatable(data, self)
	
	return data
end

enabledKeys = {
	"a",
	"b",
	"c",
	"d",
	"e",
	"f",
	"g",
	"h",
	"i",
	"j",
	"l",
	"k",
	"m",
	"n",
	"o",
	"p",
	"q",
	"r",
	"s",
	"t",
	"v",
	"w",
	"x",
	"y",
	"z",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"0",
	"backspace",
	"space",
}

function sosaGUI.editbox:onKey(key, state)
	if not self.textactiv or not self.instance.activ then
		return
	end
	if state then
		if isKeyEnabled(key) and not isChatBoxInputActive() and not isConsoleActive() and not isMainMenuActive() then
			if key ~= "backspace" then
				if dxGetTextWidth(self.text) < self.w - 6 then
				if key == "space" then
					self.text = self.text.." "
				else
					if getKeyState("lshift") then
						key = string.upper(key)
					end
					self.text = self.text..key
				end
				end
			else
				self.text = string.sub(self.text, 1, #self.text - 1)
			end	
		end
	end
end

local tick = 0
function sosaGUI.editbox:onRender()
	if not self.instance.activ then
		return
	end
		local text = self.text
		if self.masked then
			text = string.gsub(self.text,".","*")
		end
		dxDrawRectangle(self.x, self.y, self.w, 30, tocolor(255, 120, 0))
		dxDrawRectangle(self.x + 1, self.y + 1, self.w - 2, 30 - 2, tocolor(250, 250, 250, 195))
		dxDrawText(text, self.x + 3, self.y + 2, self.x + self.w - 3, self.y + 30 - 2, tocolor(15,15,15), 1, "default", "left", "center")
		if self.textactiv then
			if tick >= 60 then
				tick = 0
			end
			if tick <= 30 then
				dxDrawRectangle(dxGetTextWidth(text, 1, "default") + self.x + 4, self.y + 2, 1, 26, tocolor(5, 5, 5, 240))
			end
			tick = tick + 1
		end		
end

function sosaGUI.editbox:onClick(state)
	if not self.instance.activ then
		return
	end
		if isCursorOnElement(self.x, self.y, self.w, self.h) then	
			self.textactiv = true
			--toggleControl("chatbox", false)
		else
			self.textactiv = false
			--guiSetInputMode("allow_binds")
		end
end


--|| Image  ||--
function sosaGUI.image:new(instance,x,y,w,h,path)
	local data = {}
	data.events = {}	
	data.x = x + instance.x
	data.y = y + instance.y
	data.w = w
	data.h = h
	data.path = path
	data.activ = instance.activ
	data.type = "image"
	data.instance = instance
	table.insert(data.instance.childs, data)
	
	data.renderfunc = function() data:onRender() end
	
	data.events.render = addEventHandler("onClientRender", root, data.renderfunc)
	
	setmetatable(data, self)
	
	return data
end


function sosaGUI.image:onRender()
	if not self.instance.activ then
		return
	end
		dxDrawImage(self.x, self.y, self.w, self.h, self.path)	
end

--||  Checkbox  ||--
function sosaGUI.checkbox:new(instance,x,y,text,checked,func)
	local data = {}
	data.events = {}	
	data.x = x + instance.x
	data.y = y + instance.y
	data.w = 30
	data.h = 30	
	data.text = text
	data.checked = checked
	data.activ = instance.activ	
	data.type = "checkbox"
	data.func = func
	data.instance = instance
	table.insert(data.instance.childs, data)
	
	data.renderfunc = function() data:onRender() end
	data.clickfunc = function(_, state) data:onClick(state) end
	
	data.events.render = addEventHandler("onClientRender", root, data.renderfunc)
	data.events.click = addEventHandler("onClientClick", root, data.clickfunc)
	
	setmetatable(data, self)	
	
	return data
end

function sosaGUI.checkbox:onRender()
	if not self.instance.activ then
		return
	end
		if self.checked then
			dxDrawImage(self.x, self.y, 30, 30, "resources/images/gui/checkbox_checked.png")
		else
			dxDrawImage(self.x, self.y, 30, 30, "resources/images/gui/checkbox_unchecked.png")
		end	
		dxDrawText(self.text, self.x + 35, self.y, self.x + 180, self.y + 30, white, 1, "default-bold", "left", "center") 
end

function sosaGUI.checkbox:onClick(state)
	if not self.instance.activ or state ~= "down" then
		return
	end
	if isCursorOnElement(self.x, self.y, self.w, self.h) then
		self.checked = not self.checked
		if self.func then
			self.func()
		end
	end
end

--||  Gridlist  ||--
function sosaGUI.gridlist:new(instance,x,y,w,h,text)
	local data = {}
	data.events = {}	
	data.x = x + instance.x
	data.y = y + instance.y
	data.w = w
	data.h = h
	data.text = text
	data.rows = {}
	data.columns = {}
	data.type = "gridlist"
	data.activ = instance.activ
	data.selected = false
	data.clicked = nil
	data.activerow = 0
	data.instance = instance
	table.insert(data.instance.childs, data)
	
	data.renderfunc = function() data:onRender() end	
	data.clickfunc = function(button, state) data:onClick(button, state) end
	data.cmovefunc = function() data:onCursorMove() end
	
	data.events.click = addEventHandler("onClientClick", root, data.clickfunc)
	data.events.cmove = addEventHandler("onClientCursorMove", root, data.cmovefunc)	
	data.events.render = addEventHandler("onClientRender", root, data.renderfunc)	

	setmetatable(data, self)

	return data
end

function sosaGUI.gridlist:addRow(...)
	table.insert(self.rows, {...})
	--outputChatBox("Row added:")
	--outputChatBox("Row ID: "..table.getn(self.rows))
	--outputChatBox("Row Values: "..table.concat({...}, ", "))
end

function sosaGUI.gridlist:addColumn(string)
	table.insert(self.columns, string)
	--outputChatBox("Column added: "..string)
end

function sosaGUI.gridlist:editField(row, column, string)
	if self.rows[row][column] then
		self.rows[row][column] = string
	end
end

function sosaGUI.gridlist:getRow()
	if self.clicked then
		return self.clicked
	else
		return false
	end
end

function sosaGUI.gridlist:onRender()
	if not self.instance.activ then
		return
	end
		--Hover Grid Fenster
		if self.selected then
			dxDrawRectangle(self.x, self.y, self.w, self.h, tocolor(1, 1, 1, 180))
		else
			dxDrawRectangle(self.x, self.y, self.w, self.h, tocolor(1, 1, 1, 160))
		end	
		dxDrawRectangle(self.x, self.y, self.w, 20, tocolor(0,0,0, 190))
		dxDrawLine(self.x, self.y + 20, self.x + self.w - 2, self.y + 20, tocolor(230,230,230), 2)
		--||||--
		--Hover
		table.foreach(self.rows, function(i, v)
			if self.activerow == i then
				dxDrawRectangle(self.x, self.y + (i*22), self.w - 2, 22, tocolor(250, 115, 10, 250))
			elseif isCursorOnElement(self.x, self.y + (i*22), self.w, 22) then
				dxDrawRectangle(self.x, self.y + (i*22), self.w - 1, 22, tocolor(0, 0, 0, 180))
			end
		end)		
		
		--Beschriftung
		table.foreach(self.columns, function(i, v)
			--dxDrawRectangle(self.x + ((i-1) * (self.w / table.getn(self.columns))), self.y, self.w / table.getn(self.columns), self.h, tocolor(3, 3, 3, 205))
			dxDrawText(v, self.x + ((i - 1)*(self.w/table.getn(self.columns))),self.y, self.x + ((i - 1)*(self.w/table.getn(self.columns))) + (self.w/table.getn(self.columns)),self.y + 22, white, 1, "default-bold", "left", "center") 
			table.foreach(self.rows, function(rI, v)
					dxDrawText(v[i], self.x + ((i - 1)*(self.w/table.getn(self.columns))), self.y + (rI*22),  self.x + ((i - 1)*(self.w/table.getn(self.columns))) + (self.w/table.getn(self.columns)), (self.y + (rI*22)) + 22, white, 1, "default", "left", "center") 
			end)
		end)
end

function sosaGUI.gridlist:onClick(button, state)
	if not self.instance.activ or state ~= "down" or button ~= "left" then
		return
	end
	for i, v in ipairs(self.rows) do
		if isCursorOnElement(self.x, self.y + (i*22), self.w, 22) then	
			self.clicked = v
			self.activerow = i
			break
		elseif not isCursorOnElement(self.x, self.y + (i*22), self.w, 22) and isCursorOnElement(self.x, self.y, self.w, self.h) then
			self.clicked = nil
			self.activerow = nil
		end
	end
end

function sosaGUI.gridlist:onCursorMove()
	if not self.instance.activ then 
		return
	end
	
	if isCursorOnElement(self.x, self.y, self.w, self.h) then
		self.selected = true
	else
		self.selected = false
	end
end

function sosaGUI.gridlist:getSelectedRow()
	return self.activerow
end

--||||||||||||||
--Actionfield

function sosaGUI.actionfield:new(instance,x,y,w,h,text,color,func)
	local data = {}
	data.events = {}
	data.x = x + instance.x
	data.y = y + instance.y
	data.w = w
	data.h = h
	data.text = text
	data.activ = instance.activ
	data.selected = false
	data.color = color
	data.instance = instance	
	data.func = func
	data.clicked = false
	data.type = "afield"
	table.insert(data.instance.childs, data)
	
	data.renderfunc = function() data:onRender() end	
	data.clickfunc = function(_, state) data:onClick(state) end
	data.cmovefunc = function() data:onMove() end
	
	data.events.click = addEventHandler("onClientClick", root, data.clickfunc)
	data.events.cmove = addEventHandler("onClientCursorMove", root, data.cmovefunc)	
	data.events.render = addEventHandler("onClientRender", root, data.renderfunc)	
	
	setmetatable(data, self)
	
	return data
end

function sosaGUI.actionfield:onRender()
	if not self.instance.activ then
		return
	end
	if self.selected then
		dxDrawRectangle(self.x, self.y, self.w, self.h, tocolor(self.color[4], self.color[5], self.color[6], 200))
	else
		dxDrawRectangle(self.x, self.y, self.w, self.h, tocolor(self.color[1], self.color[2], self.color[3], 200))
	end
	dxDrawText(self.text, self.x, self.y, self.x + self.w, self.y + self.h, tocolor(35,35,35), 1, "default", "center", "center")
end

function sosaGUI.actionfield:onMove()
	if not self.instance.activ then
		return
	end
	
	if isCursorOnElement(self.x, self.y, self.w, self.h) then
		self.selected = true
	else
		self.selected = false
	end
end

function sosaGUI.actionfield:onClick(state)
	if not self.instance.activ or state ~= "down" then
		return
	end
	if isCursorOnElement(self.x, self.y, self.w, self.h) then
		if self.func then
			self.func()
		end		
		self.clicked = true
	else
		self.clicked = false
	end
end

--||Scrollbar
function sosaGUI.scrollbar:new(instance,x,y,w,h)

end

--||Tabs
function sosaGUI.tab:new(instance,x,y,w,h)
	local data = {
		tabs = {},
		events = {}
	}
	data.x = instance.x + x
	data.y = instance.y + y
	data.w = w
	data.h = h
	data.instance = instance
	data.type = "tabbar"
	table.insert(data.instance.childs, data)
	
	
	data.renderfunc = function() data:onRender() end	
	data.clickfunc = function(_, state) data:onClick(state) end
	
	data.events.click = addEventHandler("onClientClick", root, data.clickfunc)
	data.events.render = addEventHandler("onClientRender", root, data.renderfunc)	
	
	setmetatable(data, self)
	
	return data
end

function sosaGUI.tab:onRender()
	dxDrawRectangle(self.x, self.y + 20, self.w, self.h - 20, tocolor(10, 10, 10, 150))
	dxDrawLine(self.x, self.y + 20, self.x+self.w, self.y + 20, tocolor(10, 10, 10, 200), 2)
	table.foreach(self.tabs, function(i, v)
		if not v.activ then
			if isCursorOnElement(self.x-90+(90*i), self.y, 90, 20) then
				dxDrawRectangle(self.x-90+(90*i), self.y, 90, 20, tocolor(2, 2, 2, 200))
			else
				dxDrawRectangle(self.x-90+(90*i), self.y, 90, 20, tocolor(7, 7, 7, 200))
			end
			dxDrawText(v[1], self.x-90+(90*i), self.y, self.x-90+(90*i) + 90, self.y + 20, tocolor(120, 255, 170), 1, "default", "center", "center", true)
		else
			dxDrawRectangle(self.x-90+(90*i), self.y, 90, 20, tocolor(255, 130, 20, 180))
			dxDrawText(v[1], self.x-90+(90*i), self.y, self.x-90+(90*i) + 90, self.y + 20, tocolor(120, 255, 170), 1, "default", "center", "center", true)
		end	
		dxDrawLine(self.x-90+(90*i), self.y, self.x-90+(90*i), self.y + 20)
		

	end)
end


function sosaGUI.tab:onClick(state)
	if not self.instance.activ or state ~= "down" then
		return
	end
	if isCursorOnElement(self.x, self.y, (#self.tabs*90)+#self.tabs, 20) then
		
		for i, v in ipairs(self.tabs) do
			if isCursorOnElement(self.x-90+(90*i), self.y, 90, 20) then
				
				v.activ = true
			else	
				
				v.activ = false
			end
		end
	end
end

function sosaGUI.tab:addTab(string)
	local newtab = 
	{
		string,
		activ = false,
		clicked = false,
		childs = {},
		x = self.x,
		y = self.y
	} 

	table.insert(self.tabs, newtab)
	--Starttab
	self.tabs[1].activ = true
	return newtab						 
end
--|||||||||||||||||||||||||||||--
--||  Allgemeine Funktionen  ||--
--|||||||||||||||||||||||||||||--

function sosaGUI.getWindows()
	
	return sosaGUI.windows
end


function sosaGUI.terminate(instance)
	table.foreach(instance.childs, function(i, v)
		if v.type == "tabbar" then
			table.foreach(v.tabs, function(_, v)
				table.foreach(v.childs, function(_, v)
					if v.events.render then
						removeEventHandler("onClientRender", root, v.renderfunc)	
					end		
					if v.events.cmove then
						removeEventHandler("onClientCursorMove", root, v.cmovefunc)		
					end
		
					if v.events.click then
						removeEventHandler("onClientClick", root, v.clickfunc)
					end		
				end)
			end)
		end
		if v.events.render then
			removeEventHandler("onClientRender", root, v.renderfunc)	
		end		
		if v.events.cmove then
			removeEventHandler("onClientCursorMove", root, v.cmovefunc)		
		end
		
		if v.events.click then
			removeEventHandler("onClientClick", root, v.clickfunc)
		end
		v = nil
	end)
	removeEventHandler("onClientClick", root, instance.clickfunc)
	removeEventHandler("onClientRender", root, instance.renderfunc)
	instance = nil
	showCursor(false)
end

function sosaGUI.setVisible(element, state)
	element.activ = state
end

function sosaGUI.destroy(element)
	if element.events.render then
		removeEventHandler("onClientRender", root, element.renderfunc)	
	end		
	if element.events.cmove then
		removeEventHandler("onClientCursorMove", root, element.cmovefunc)		
	end
	if element.events.click then
		removeEventHandler("onClientClick", root, element.clickfunc)
	end	
	element = nil
end

--[[
function sosaGUI:onCursorMove(self, pos)
	if not self.activ then 
		return
	end
	
	if isCursorOnElement(self.x, self.y, self.w, self.h) then
		self.selected = true
	else
		self.selected = false
	end
end

function sosaGUI:terminate(instance)
	outputChatBox("JII")
	table.foreach(instance.childs, function(i, v)
		if instance.childs[i].events.render then
			removeEventHandler("onClientRender", root, instance.childs[i].renderfunc)
		end
		if instance.childs[i].events.click then
			removeEventHandler("onClientClick", root, instance.childs[i].clickfunc)
		end
		if instance.childs[i].events.cmove then
			removeEventHandler("onClientCursorMove", root, instance.childs[i].cmovefunc)
		end		
	end)
	removeEventHandler("onClientRender", root, instance.renderfunc)
	removeEventHandler("onClientClick", root, instance.clickfunc)
	showCursor(false)	
end

function sosaGUI:onClick(self, state)
	if not self.instance.activ or state ~= "down" then
		return
	end 
	--||Window||--	
	if self.type == "window" then
		if isCursorOnElement(self.x + self.w - 27.75, self.y - 27.5, 25, 25) then
			self.activ = false
			sosaGUI:terminate(self)
		end	
	--||Button||--	
	elseif self.type == "button" then
		if isCursorOnElement(self.x, self.y, self.w, self.h) then
			if self.func then
				self.func()
			end
		end	
	--||Edit Box||--
	elseif self.type == "editbox" then
		if isCursorOnElement(self.x, self.y, self.w, self.h) then	
			self.textactiv = true
			guiSetInputMode("no_binds")
		else
			self.textactiv = false
			guiSetInputMode("allow_binds")
		end
	--||Checkbox||--
	elseif self.type == "checkbox" then
		if isCursorOnElement(self.x, self.y, self.w, self.h) then
			self.checked = not self.checked
			if self.func then
				self.func()
			end
		end
	elseif self.type == "gridlist" then
		for i, v in ipairs(self.rows) do
			if isCursorOnElement(self.x, self.y + (i*22), self.w, 22) then	
				self.clicked = i
				break
			elseif not isCursorOnElement(self.x, self.y + (i*22), self.w, 22) and isCursorOnElement(self.x, self.y, self.w, self.h) then
				self.clicked = 0
			end
		end
	end
	
end


function sosaGUI:onRender(self)
	if not self.instance.activ then 
		return
	end
	--||Window||--	
	if self.type == "window" then
		dxDrawImage(self.x, self.y - 30, self.w, 30, "resources/images/gui/titlebar.png")
		dxDrawText(self.text, self.x, self.y - 30, self.x + self.w, self.y, tocolor(255, 255, 255), 1, "default", "center", "center")
		dxDrawRectangle(self.x, self.y, self.w, self.h, tocolor(10, 10, 10, 200))
		dxDrawImage(self.x + self.w - 27.75, self.y - 27.5, 25, 25, "resources/images/gui/close.png")	
		
		dxDrawLine(self.x, self.y - 30, self.x + self.w, self.y - 30, tocolor(0,0,0), 1)
		dxDrawLine(self.x, self.y + self.h, self.x + self.w, self.y + self.h, tocolor(0,0,0), 1)
		dxDrawLine(self.x, self.y - 30, self.x, self.y + self.h, tocolor(0,0,0), 1)
		dxDrawLine(self.x + self.w, self.y - 30, self.x + self.w, self.y + self.h, tocolor(0,0,0), 1)
	--||Button||--	
	elseif self.type == "button" then
		dxDrawImage(self.x, self.y, 14*(self.h/74), self.h, "resources/images/gui/buttonLeft.png")
		dxDrawImage(self.x + self.w - (14*(self.h/74)), self.y, 14*(self.h/74), self.h, "resources/images/gui/buttonRight.png")
		for i = 0, (self.w + 2 - (2*(14*(self.h/74)))) do
			dxDrawImage(self.x + (14*(self.h/74)) + i - 1, self.y, 1, self.h, "resources/images/gui/buttonMiddle.png")
		end
		dxDrawText(self.text, self.x, self.y, self.x + self.w, self.y + self.h, white, 1, "default", "center", "center", false, true)
	--||Label||--
	elseif self.type == "label" then
		dxDrawText(self.text, self.x, self.y, self.x + self.w, self.y + self.h, white, 1, "default", "center", "center", false, true)
	--||Edit Box||--
	elseif self.type == "editbox" then
		local text = self.text
		if self.masked then
			text = string.gsub(self.text,".","*")
		end
		dxDrawRectangle(self.x, self.y, self.w, 30, tocolor(255, 120, 0))
		dxDrawRectangle(self.x + 1, self.y + 1, self.w - 2, 30 - 2, tocolor(250, 250, 250, 195))
		dxDrawText(text, self.x + 3, self.y + 2, self.x + self.w - 3, self.y + 30 - 2, tocolor(15,15,15), 1, "default", "left", "center")
	--||Image||--	
	elseif self.type == "image" then
		dxDrawImage(self.x, self.y, self.w, self.h, self.path)
	--||Checkbox||--
	elseif self.type == "checkbox" then
		if self.checked then
			dxDrawImage(self.x, self.y, 30, 30, "resources/images/gui/checkbox_checked.png")
		else
			dxDrawImage(self.x, self.y, 30, 30, "resources/images/gui/checkbox_unchecked.png")
		end	
		dxDrawText(self.text, self.x + 35, self.y, self.x + 180, self.y + 30, white, 1, "default-bold", "left", "center") 
	--||Gridlist||--	
	elseif self.type == "gridlist" then
		--Hover Grid Fenster
		if self.selected then
			dxDrawRectangle(self.x, self.y, self.w, self.h, tocolor(1, 1, 1, 180))
		else
			dxDrawRectangle(self.x, self.y, self.w, self.h, tocolor(1, 1, 1, 160))
		end	
		dxDrawRectangle(self.x, self.y, self.w, 20, tocolor(0,0,0, 190))
		dxDrawLine(self.x, self.y + 22, self.x + self.w, self.y + 22, tocolor(230,230,230), 2)
		--||||--
		--Hover
		table.foreach(self.rows, function(i, v)
			if self.clicked == i then
				dxDrawRectangle(self.x, self.y + (i*22), self.w - 1, 22, tocolor(250, 115, 10, 250))
			elseif isCursorOnElement(self.x, self.y + (i*22), self.w, 22) then
				dxDrawRectangle(self.x, self.y + (i*22), self.w - 1, 22, tocolor(0, 0, 0, 180))
			end
		end)		
		
		--Beschriftung
		table.foreach(self.columns, function(i, v)
			--dxDrawRectangle(self.x + ((i-1) * (self.w / table.getn(self.columns))), self.y, self.w / table.getn(self.columns), self.h, tocolor(3, 3, 3, 205))
			dxDrawText(v, self.x + ((i - 1)*(self.w/table.getn(self.columns))),self.y, self.x + ((i - 1)*(self.w/table.getn(self.columns))) + (self.w/table.getn(self.columns)),self.y + 22, white, 1, "default-bold", "left", "center") 
			table.foreach(self.rows, function(rI, v)
					dxDrawText(v[i], self.x + ((i - 1)*(self.w/table.getn(self.columns))), self.y + (rI*22),  self.x + ((i - 1)*(self.w/table.getn(self.columns))) + (self.w/table.getn(self.columns)), (self.y + (rI*22)) + 22, white, 1, "default", "left", "center") 
			end)
		end)

	end
end]]--


function isKeyEnabled(key)
	rtn = false
	for i, v in ipairs(enabledKeys) do
		if (string.upper(v) == string.upper(key) ) then
			rtn = true
		end
	end
	return rtn
end


function guitest2()
	outputChatBox("JA")
	local jd = sosaGUI.window:new(800, 400, 500, 600, "Ich mag Zucker und Käse")
	local grid = sosaGUI.gridlist:new(jd, 20, 0, 400, 500, "Gridlist")
	grid:addColumn( "Lizenz")
	grid:addColumn("Preis")
	grid:addColumn("Column")
	grid:addRow("Sweeeger", "400.000$", "BLA5")
	grid:addRow("Zucker", "Käse", "Schoki")
	grid:addRow("Brot", "Kuchen", "KEKSE")
	grid:addRow("Brot", "Schule", "Zahn")
	grid:addRow("Haus", "Kuchen", "KEKSE")
	grid:addRow("Brot", "Koch", "Küche")
	grid:addRow("Brot", "Streets of SA", "KEKSE")
end


addCommandHandler("guitest", guitest2)


