function sosaSetVisible(element, state)
	if element and state then
		element.activ = state
	else
		return outputDebugString("[SoSA]: Error @sosaSetVisible")
	end
end

function sosaGetVisible(element)
	if element then
		return element.activ
	else
		return outputDebugString("[SoSA]: Error @sosaGetVisible")
	end
end

function sosaSetText(element, text)
	if element and text then
		element.text = text
	else
		return outputDebugString("[SoSA]: Error @sosaSetText")
	end
end

function sosaGetText(element)
	if element then
		return element.text
	else
		return outputDebugString("[SoSA]: Error @sosaGetText")
	end
end
