cDx = {}
cDx.__index = cDx
local x,y = guiGetScreenSize()

function cDx:new(title, width, height, background, fontcolor, font)
	
	--Checking the datas for the right type--
	assert((type(title) == 'string'), 'Bad Argument @cDx:new [Expected stringt at argument 1 got '..type(title)..']')
	assert((type(width) == 'number'), 'Bad Argument @cDx:new [Expected number at argument 2 got '..type(width)..']')
	assert((type(height) == 'number'), 'Bad Argument @cDx:new [Expected number at argument 3 got '..type(height)..']')
	assert((type(background) == 'function'), 'Bad Argument @cDx:new [Expected function at argument 4 got '..type(background)..']')
	assert((type(fontcolor) == 'function'), 'Bad Argument @cDx:new [Expected function at argument 5 got '..type(fontcolor)..']')
	assert((type(font) == 'string'), 'Bad Argument @cDx:new [Expected stringt at argument 6 got '..type(font)..']')
	
	--creating the instance--
	local instance = {
		title = tostring(title),
		width = tonumber(width),
		height = tonumber(height),
		background = background,
		fontcolor = fontcolor,
		font = tostring(font),
		show = false,
		list = {},
		scroll = 0,
		di = 0
	}
	
	--Setting the metatable--
	setmetatable(instance, self)
	
	--return the instance--
	return instance
end

function cDx:changeTitle(string)
	self.title = string
end

function cDx:remove(i)
	if self.list[i] then
		table.remove(self.list, i)
	end
end

function cDx:renameItem(i,string)
	for num, p in ipairs(self.list)
		if num == i then
			p = string
			return true
		end
	end
	return false
end

function cDx:add(string)
	table.insert(self.list, string)
	outputChatBox(string)
end

function cDx:scrollUp()
	if self.scroll < #self.list then
		self.scroll = self.scroll + 1
		outputChatBox(self.scroll)
	else
		self.scroll = #self.list
	end
end

function cDx:scrollDown()
	if self.scroll > 0 then
		self.scroll = self.scroll - 1
		outputChatBox(self.scroll)
	else
		self.scroll = 0
	end
end

function cDx:click(btn,state,cx,cy)
	if btn == "left" and state == "up" then
		if isCursorShowing() then
			local di = 0
			for i = 1+self.scroll, 17+self.scroll do
				if self.list[i] then
					if cx >= x/2-self.width/2+50 and cx <= x/2-self.width/2+50+412 and cy >= y/2-self.height/2+50+((di)*20) and cy <= y/2-self.height/2+50+((di)*20)+15 then
						--Was soll beim Klicken passieren!
					end
				end
				di = di + 1
			end
		end
	end
end

function cDx:enable(value)
	if self.show == value then outputDebugString("Self.show is alread the value!") return end
	if self.show == false then
		showCursor(true)
		addEventHandler("onClientRender", root, function() self:render() end)
		addEventHandler("onClientClick", root, function(btn,state,cx,cy) self:click(btn,state,cx,cy) end)
		bindKey("mouse_wheel_down","down",function() self:scrollUp() end)
		bindKey("mouse_wheel_up","down",function() self:scrollDown() end)
		self.show = value
	else
		self.show = value
		removeEventHandler("onClientRender", root, function() self:render() end)
		removeEventHandler("onClientClick", root, function() self:click() end)
		unbindKey("mouse_wheel_down","down",function() self:scrollUp() end)
		unbindKey("mouse_wheel_up","down",function() self:scrollDown() end)
	end
end

function cDx:render()
	local di = 0
	--Drawing the window--
	dxDrawRectangle(x/2-self.width/2, y/2-self.height/2, self.width, self.height, self.background, false)
	--Drawing the title
	dxDrawText(self.title, x/2-dxGetTextWidth(self.title,1.5,"default-bold")/2, y/2-self.height/2+5, self.width, self.height, tocolor(255,255,255,255), 1.5, "default-bold", "left", "top", false, false, true, true)
	--Background for the dxlist--
	dxDrawRectangle(x/2-self.width/2+45, y/2-self.height/2+47, 422, 340, tocolor(0, 0, 0, 150), false)
	--Drawing the list
	for i = 1+self.scroll, 17+self.scroll do
		if self.list[i] then
			dxDrawText(self.list[i], x/2-self.width/2+50, y/2-self.height/2+50+((di)*20), 412, 16, self.fontcolor, 1, self.font, "left", "top", false, false, true, true)
			--Adding the count--
			di = di + 1
		end
	end
	--Drawing the list beneath-- 
	dxDrawRectangle(x/2-self.width/2+45+422, y/2-self.height/2+47+((self.scroll*(#self.list/0.4)/340)), 10, 20, tocolor(155, 155, 155, 155), true)
	--Hover-Animation--
	local ki = 0
	if isCursorShowing() then
		local cx, cy = getCursorPosition()
		cx, cy = cx * x, cy * y
		for i = 1+self.scroll, 17+self.scroll do
			if self.list[i] then
				if cx >= x/2-self.width/2+50 and cx <= x/2-self.width/2+50+422 and cy >= y/2-self.height/2+50+((ki)*20) and cy <= y/2-self.height/2+50+(ki*20)+15 then
					outputChatBox("x")
					dxDrawRectangle(x/2-self.width/2+45, y/2-self.height/2+50+((ki)*20)-3, 422, 20, tocolor(255, 150, 0, 50), true)
				end
				ki = ki + 1
			end
		end
	end
end











local scoreboard = {};
local scoreboardTags = {}
local screenW, screenH = guiGetScreenSize()

scoreboard.position = 1
scoreboard.maxServer.players = 10
scoreboard.SpecialTag = true -- Das ist die obere leiste wen man auf den spieler geht mit dem Cursor. false = aus, true = an.

function scoreboard.SDown()
	if #getElementsByType("player") - scoreboard.position <= 1  then
		scoreboard.position = #getElementsByType("player") 
	else
		scoreboard.position = scoreboard.position + 1
	end
end
function scoreboard.SUp()
	if scoreboard.position <= 1 then
		scoreboard.position = 1
	else
		scoreboard.position = scoreboard.position - 1
	end
end


function scoreboard.Render()
	local x, y = guiGetScreenSize()
	local sx, sy = x/1600, y/900
	
	if x < 1600 then
		local player_screen = x * y
		local screen = 1920 * 1080 -- deine auflösung
		local screen = screen/100 --dreisatz = 1%
		local player_screen = player_screen/screen -- die Prozentzahl vom Spieler Bildschirm im Vergleich mit der größten Auflösung
		local size = 1/100*player_screen -- 1 ist die fontgröße bei einer Bildschirmauflösung von screen = 1600 * 900 
	else
		size = 1.0
	end
		
	dxDrawRectangle(579*sx, 320*sy, 442*sx, 264*sy, tocolor(0, 0, 0, 206), false)
	
	dxDrawRectangle(579*sx, 319*sy, 442*sx, 15*sy, tocolor(0, 0, 0, 153), false)
	
	dxDrawRectangle(579*sx, 291*sy, 442*sx, 25*sy, tocolor(0, 0, 0, 195), false)
	
	dxDrawText("Username", 594*sx, 320*sy, 649*sx, 333*sy, tocolor(255, 255, 255, 255), size, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Playtime", 701*sx, 320*sy, 756*sx, 333*sy, tocolor(255, 255, 255, 255), size, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Points", 795*sx, 320*sy, 850*sx, 333*sy, tocolor(255, 255, 255, 255), size, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Status", 884*sx, 320*sy, 939*sx, 333*sy, tocolor(255, 255, 255, 255), size, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Ping", 959*sx, 320*sy, 996*sx, 333*sy, tocolor(255, 255, 255, 255), size, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Arena: Lobby", 756*sx, 293*sy, 844*sx, 313*sy, tocolor(245, 103, 0, 249), size, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawRectangle(894*x/1680, 344*y/1050, 20*x/1680, 19*y/1050, tocolor(196, 88, 0, 255), true)
	dxDrawRectangle(765*x/1680, 344*y/1050, 20*x/1680, 19*y/1050, tocolor(196, 88, 0, 255), true)
	dxDrawText(">", 899*x/1680, 344*y/1050, 910*x/1680, 358*y/1050, tocolor(143, 63, 0, 254), size, "default", "left", "top", false, false, true, false, false)
	dxDrawText("<", 770*x/1680, 344*y/1050, 781*x/1680, 358*y/1050, tocolor(143, 63, 0, 254), size, "default", "left", "top", false, false, true, false, false)
	dxDrawText("Open cursor 'right click' Close again 'right click'", 723*x/1680, 683*y/1050, 979*x/1680, 697*y/1050, tocolor(255, 255, 255, 255), size, "default", "center", "bottom", false, false, true, false, false)
	
	local i = 0
	local Tab = {}
	for key, player in pairs( getElementsByType("player") ) do 
		local Name, Playtime, Status, Points, Ping, Money
		local Kills, Deaths, Adminlvl, ID, Serial, Model
		
		i = i + 1
		if getElementData( player, "loggedin" ) then
			ID = getElementData( player, "ID" )
			Name = getPlayerName( player )
			Status = getElementData( player, "Status" )
			Points = getElementData( player, "Points" )
			Kills = getElementData( player, "Kills" )
			Deaths = getElementData( player, "Deaths" )
			Adminlvl = aName( player )
			if tonumber( getElementData( player, "Adminlvl" ) ) >= 1 then
				Name = "[SA:DM]"..Name
			end
			Money = getElementData( player, "Money" )
			Serial = getServer.playerserial( player )
			Model = "Files/images/models/"..getElementModel( player )..".png"
			Playtime = math.floor ( getElementData ( player, "Playtime" ) / 60 )..":"..( getElementData ( player, "Playtime" ) - math.floor ( getElementData ( player, "Playtime" ) / 60 ) * 60 )
		else
			ID = "not loggedin"
			Name = getPlayerName( player )
			Playtime = "not loggedin"
			Status = "connecting"
			Points = "not loggedin"
			Kills = "not loggedin"
			Deaths = "not loggedin"
			Adminlvl = "not loggedin"
			Money = "not loggedin"
			Model = "Files/images/models/error.png"
		end
		Serial = getServer.playerserial( player )
		Ping = getPlayerPing( player )
		
		Tab[i] = {}
		Tab[i].Name = tostring(Name)
		Tab[i].Playtime = tostring(Playtime)
		Tab[i].Points = tostring(Points)
		Tab[i].Status = tostring(Status)
		Tab[i].Ping = Ping
		Tab[i].ID = tostring(ID)
		Tab[i].Kills = tostring(Kills)
		Tab[i].Deaths = tostring(Deaths)
		Tab[i].Adminlvl = tostring(Adminlvl)
		Tab[i].Money = tostring(Money)
		Tab[i].Serial = tostring(Serial)
		Tab[i].Model = tostring(Model)
	end
		local scale = 0	
		for i = 0 + scoreboard.position, scoreboard.maxServer.players + scoreboard.position do
			if ( Tab[i] ) then
				local R, G, B = scoreboard.findPingRGB( Tab[i].Ping ) 
				dxDrawText(Tab[i].Name, 594*sx, 340*sy+(15*scale), 649*sx, 355*sy, tocolor(255, 255, 255, 255), size, "default", "center", "center", false, false, true, true, false)
				dxDrawText(Tab[i].Playtime, 702*sx, 340*sy+(15*scale), 757*sx, 355*sy, tocolor(255, 255, 255, 255), size, "default", "center", "center", false, false, true, true, false)
				dxDrawText(Tab[i].Points, 811*sx, 339*sy+(15*scale), 832*sx, 357*sy, tocolor(255, 255, 255, 255), size, "default", "center", "center", false, false, true, true, false)
				dxDrawText(Tab[i].Status, 884*sx, 340*sy+(15*scale), 939*sx, 355*sy, tocolor(255, 255, 255, 255), size, "default", "center", "center", false, false, true, true, false)
				dxDrawText(Tab[i].Ping, 955*sx, 340*sy+(15*scale), 992*sx, 355*sy, tocolor(R, G, B, 255), size, "default", "center", "center", false, false, true, true, false)
				dxDrawRectangle(579*sx, 340*sy+(15*scale/2-2), 442*sx, 19*sy, tocolor(0, 0, 0, 192), false)
				
				if scoreboard.SpecialTag then
					if isCursorShowing() then
						if isCursorOnElement( 579*sx, 340*sy+(15*scale/2-2), 442*sx, 20*sy ) then
					
							dxDrawRectangle(579*sx, 340*sy+(15*scale/2-2), 442*sx, 19*sy, tocolor(254, 108, 0, 148), true)
							dxDrawText(Tab[i].Name, 580*sx, 176*sy, 787*sx, 218*sy, tocolor(255, 255, 255, 255), 2.00, "sans", "left", "center", false, false, true, false, false)
							dxDrawText("Admin: "..Tab[i].Adminlvl.."\nMoney:"..Tab[i].Money.."\nKills:"..Tab[i].Kills.."\nDeaths:"..Tab[i].Deaths.."\nSerial:"..Tab[i].Serial.."\n", 580*sx, 215*sy, 787*sx, 285*sy, tocolor(255, 255, 255, 255), size, "default-bold", "left", "top", false, false, true, false, false)
							local width = dxGetTextWidth( Tab[i].Name, 2, "sans" )
							dxDrawImage(692*sx+(width/2), 144*sy, 76*sx, 70*sy, Tab[i].Model, 0, 0, 0, tocolor(255, 255, 255, 255), true)
						end
					end
				end
				if isCursorShowing() then
					
					if isCursorOnElement( 1022*sx, 319*sy, 25*sx, 265*sy ) then
						dxDrawRectangle(1022*sx, 319*sy, 25*sx, 265*sy, tocolor(0, 0, 0, 207), true)
						dxDrawRectangle(1024*sx, 321*sy+scoreboard.position, 21*sx, 49*sy, tocolor(249, 134, 0, 246), true)
					else
						dxDrawRectangle(1022*sx, 319*sy, 10*sx, 265*sy, tocolor(0, 0, 0, 207), true) -- Scroll backround
						dxDrawRectangle(1024*sx, 321*sy+scoreboard.position, 6*sx, 45*sy, tocolor(249, 134, 0, 246), true) -- Scroll Item
					end
				else
					dxDrawRectangle(1022*sx, 319*sy, 10*sx, 265*sy, tocolor(0, 0, 0, 207), true) -- Scroll backround
					dxDrawRectangle(1024*sx, 321*sy+scoreboard.position, 6*sx, 45*sy, tocolor(249, 134, 0, 246), true) -- Scroll Item
				end
				scale = scale + 3
			end
		end
end
-- Info + 4
-- Long Text + 25
      

function scoreboard.findPingRGB( key )
	if ( key ) then
		if key < 60 then
			return 143, 231, 0
		elseif key > 60 and key < 120 then
			return 255, 153, 0
		elseif key > 120 then
			return 255, 0, 0
		end
	end
end

local Click = false
function scoreboard.ElementsClick()
	if isCursorShowing() then
		showCursor( false )
		Click = false
	else
		showCursor( true )
		Click = true
	end
end


function scoreboard.Tab( key, state )
	if ( state == 'down' ) then
		addEventHandler("onClientRender", getRootElement(), scoreboard.Render)
		bindKey("mouse2", "down", scoreboard.ElementsClick)
		bindKey("mouse_wheel_down","down", scoreboard.SDown)
		bindKey("mouse_wheel_up", "down", scoreboard.SUp)
		toggleControl("next_weapon", false)
		toggleControl("previous_weapon", false)
	elseif ( state == 'up' ) then
		if Click then
			showCursor( false )
		end
		removeEventHandler("onClientRender", getRootElement(), scoreboard.Render)
		unbindKey("mouse2", "down", scoreboard.ElementsClick)
		unbindKey("mouse_wheel_down","down", scoreboard.SDown)
		unbindKey("mouse_wheel_up", "down", scoreboard.SUp)
		toggleControl("next_weapon", true)
		toggleControl("previous_weapon", true)
	end
end
bindKey("tab", "up", scoreboard.Tab )
bindKey("tab", "down", scoreboard.Tab )









