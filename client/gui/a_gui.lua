
sosaGUI = {
	window = {},
	button = {},
	label = {},
	image = {},
	editbox = {},
	guifield = {},
	checkbox = {},
	gridlist = {}
}
sosaGUI.window.__index = sosaGUI.window

function sosaGUI.window:new(x,y,w,h,text)
	local data = {}	
	data.x = x
	data.y = y
	data.w = w
	data.h = h
	data.text = text
	data.activ = true
	data.selected = true
	data.type = "window"
	data.instance = data

	data.clickfunc = function(_, state) data:onClick() end
	data.renderfunc = function() data:onRender() end
	data.cmovefunc = function() data:onMove() end
	
	addEventHandler("onClientClick", root, data.clickfunc)
	addEventHandler("onClientRender", root, data.renderfunc)	
	
	setmetatable(data, self)
	
	return data
end

function sosaGUI.window:onRender()
		dxDrawImage(self.x, self.y - 30, self.w, 30, "resources/images/gui/titlebar.png")
		dxDrawText(self.text, self.x, self.y - 30, self.x + self.w, self.y, tocolor(255, 255, 255), 1, "default", "center", "center")
		dxDrawRectangle(self.x, self.y, self.w, self.h, tocolor(10, 10, 10, 200))
		dxDrawImage(self.x + self.w - 27.75, self.y - 27.5, 25, 25, "resources/images/gui/close.png")	
		
		dxDrawLine(self.x, self.y - 30, self.x + self.w, self.y - 30, tocolor(0,0,0), 1)
		dxDrawLine(self.x, self.y + self.h, self.x + self.w, self.y + self.h, tocolor(0,0,0), 1)
		dxDrawLine(self.x, self.y - 30, self.x, self.y + self.h, tocolor(0,0,0), 1)
		dxDrawLine(self.x + self.w, self.y - 30, self.x + self.w, self.y + self.h, tocolor(0,0,0), 1)

end

function sosaGUI.window:onClick()
		if isCursorOnElement(self.x + self.w - 27.75, self.y - 27.5, 25, 25) then
			self.activ = false
			--sosaGUI:terminate(self)
		end	
end