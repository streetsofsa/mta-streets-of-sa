sosaGUI = {window = {}, label = {}, button = {}}

local sosaVisible = false
local cursortime = 0

function sosaCreateWindow(x, y, width, height, title, closeEnabled)
	if sosaVisible == false then 
	if (x or y or width or height or title) == nil then return end
	if closeEnabled == nil then closeEnabled = true end		
	local element = createElement("sosaWindow")
	setElementData(element, "x", x)
	setElementData(element, "y", y)
	setElementData(element, "width", width)
	setElementData(element, "height", height)
	setElementData(element, "title", title)
	setElementData(element, "cEnabled", closeEnabled)
	sosaGUI.window.x = x 
	sosaGUI.window.y = y
	sosaVisible = true 
	addEventHandler("onClientClick", getRootElement(), function(button, state, absX, absY)
	if sosaVisible == true then
	if (absX > (x + (width - 27.75))) and (absX < (x + (width - 2.25))) and (absY > y + 2.45) and (absY < y + 27.5) then
		sosaVisible = false
		playSound("resources/sounds/gui/click.wav")	
		for id, ele in ipairs(getElementsByType("sosaWindow")) do
		triggerEvent("onSosaWindowClose", ele)
		table.foreach(getElementChildren(ele), function(i, v)
			destroyElement(v)
		end)
		destroyElement(ele)
		end
	end	
	end	
	end)
	return element
	end
end

function sosaCreateLabel(parent, x, y, width, height, text, r, g, b, font, alignX, alignY, size)
	if not isElement(parent) then return end
	if getElementType(parent) == "sosaWindow" then
	if r == nil then r = 255 end
	if g == nil then g = 255 end
	if b == nil then b = 255 end
	if font == nil then font = "default" end
	if alignX == nil then alignX = "center" end
	if alignY == nil then alignY = "center" end
	if size == nil then size = 1 end
	local element = createElement("sosaLabel")
	setElementParent(element, parent or sosaGUIRoot)
	setElementData(element, "x", x + sosaGUI.window.x)
	setElementData(element, "y", y + sosaGUI.window.y)
	setElementData(element, "width", width)
	setElementData(element, "height", height)
	setElementData(element, "text", text)
	setElementData(element, "r", r)
	setElementData(element, "g", g)
	setElementData(element, "b", b)
	setElementData(element, "font", font)
	setElementData(element, "alignX", alignX)
	setElementData(element, "alignY", alignY)
	setElementData(element, "size", size)
	return element
	end
end

function sosaCreateButton(parent, x, y, width, height, text)
	if not isElement(parent) then return end
	if getElementType(parent) == "sosaWindow" then
	local leftWidth = 14*(height/74)
	local rightWidth = 14*(height/74)
	local middleWidth = width - (leftWidth + rightWidth)
	local element = createElement("sosaButton")
	setElementParent(element, parent or sosaGUIRoot)
	setElementData(element, "x", x + sosaGUI.window.x)
	setElementData(element, "y", y + sosaGUI.window.y)
	setElementData(element, "width", width)
	setElementData(element, "height", height)
	setElementData(element, "text", text)
	setElementData(element, "r", 255)
	setElementData(element, "g", 255)
	setElementData(element, "b", 255)
	setElementData(element, "leftWidth", leftWidth)
	setElementData(element, "rightWidth", rightWidth)
	setElementData(element, "middleWidth", middleWidth)
	setElementData(element, "hovered", false)
	addEventHandler("onClientCursorMove", getRootElement(), function(cX, cY, absX, absY)
	if isElement(element) then
	if (absX > getElementData(element, "x")) and (absX < (getElementData(element, "x") + getElementData(element, "width"))) and (absY > getElementData(element, "y")) and (absY < (getElementData(element, "y") + getElementData(element, "height"))) then
		setElementData(element, "hovered", true)		
	else
		setElementData(element, "hovered", false)
	end
	end
	end)
	return element
	end
end


function sosaCreateImage(parent, x, y, width, height, image)
if not isElement(parent) then return end
if getElementType(parent) == "sosaWindow" then
	local element = createElement("sosaImage")
	setElementParent(element, parent or sosaGUIRoot)
	setElementData(element, "x", x + sosaGUI.window.x)
	setElementData(element, "y", y + sosaGUI.window.y)
	setElementData(element, "width", width)
	setElementData(element, "height", height)
	setElementData(element, "image", image)
	setElementData(element, "rotation", rotation)
end
end

function sosaCreateEditBox(parent, x, y, width, text, masked)
	if not isElement(parent) then return end
	if masked == nil then masked = false end
	if text == nil then text = "" end
	if getElementType(parent) == "sosaWindow" then
	local element = createElement("sosaEdit")
	setElementParent(element, parent)
	setElementData(element, "x", x + sosaGUI.window.x)
	setElementData(element, "y", y + sosaGUI.window.y)
	setElementData(element, "width", width)
	setElementData(element, "height", 35)
	setElementData(element, "text", text)
	setElementData(element, "masked", masked)
	addEventHandler("onSosaEditClick",element, pressEditBox)
	return element
	end
end

function sosaCreateCheckBox(parent, x, y, text, selected)
if not isElement(parent) then return end
if getElementType(parent) == "sosaWindow" then
	if selected == nil then selected = false end
	local element = createElement("sosaCheckBox")
	setElementParent(element, parent)
	setElementData(element, "x", x + sosaGUI.window.x)
	setElementData(element, "y", y + sosaGUI.window.y)
	setElementData(element, "text", text)
	setElementData(element, "selected", selected)
	addEventHandler("onSosaClick", element, function() setElementData(source, "selected", not getElementData(source, "selected")) end)
	return element
end
end

function sosaCreateGUIField(parent, x, y, width, height, text, r, g, b)
if not isElement(parent) then return end
if getElementType(parent) == "sosaWindow" then
	if text == nil then text = "" end
	if r == nil or g == nil or b == nil then r,g,b = 255, 255, 255 end
	if outlined == nil then outlined = false end
	local element = createElement("sosaGUIField")
	setElementParent(element, parent)
	setElementData(element, "x", x + sosaGUI.window.x)
	setElementData(element, "y", y + sosaGUI.window.y)
	setElementData(element, "width", width)
	setElementData(element, "height", height)	
	setElementData(element, "text", text)
	setElementData(element, "r", r)
	setElementData(element, "g", g)
	setElementData(element, "b", b)
	setElementData(element, "selected", false)
	local rH = r - (r - 5)
	local gH = g - (g - 5)
	local bH = b - (b - 5)
	addEventHandler("onClientClick", getRootElement(), function(btn, state, absX, absY)
	if isElement(element) then
	if (absX > getElementData(element, "x")) and (absX < (getElementData(element, "x") + getElementData(element, "width"))) and (absY > getElementData(element, "y")) and (absY < (getElementData(element, "y") + getElementData(element, "height"))) then
		setElementData(element, "selected", true)
	else	
		setElementData(element, "selected", false)		
	end
	end
	end)
	addEventHandler("onClientCursorMove", getRootElement(), function(cX, cY, absX, absY)
	if isElement(element) then
	if (absX > getElementData(element, "x")) and (absX < (getElementData(element, "x") + getElementData(element, "width"))) and (absY > getElementData(element, "y")) and (absY < (getElementData(element, "y") + getElementData(element, "height"))) then
		setElementData(element, "r", rH)
		setElementData(element, "g", gH)
		setElementData(element, "b", bH)
	else
		setElementData(element, "r", r)
		setElementData(element, "g", g)
		setElementData(element, "b", b)
	end
	end
	end)
	return element
end	
end

function sosaCreateOutlinedRectangle(parent, x, y, width, height, r, g, b)
if not isElement(parent) then return end
if getElementType(parent) == "sosaWindow" then
	if r == nil or g == nil or b == nil then r,g,b = 255, 255, 255 end
	local element = createElement("outlinedRectangle")
	setElementParent(element, parent)
	setElementData(element, "x", x + sosaGUI.window.x)
	setElementData(element, "y", y + sosaGUI.window.y)
	setElementData(element, "width", width)
	setElementData(element, "height", height)	
	setElementData(element, "r", r)
	setElementData(element, "g", g)
	setElementData(element, "b", b)
	return element
end
end

function setEditText ()
if editActive then
local control = controls()
for id,element in ipairs(getElementsByType("sosaEdit")) do
	if getElementData(element,"input") and not isChatBoxInputActive() and not isConsoleActive() and not isMainMenuActive() then
		if control == "backspace" then 
			setElementData(element,"text",string.sub(getElementData(element,"text"),1,string.len(getElementData(element,"text"))-1))
		else
			if (dxGetTextWidth(getElementData(element,"text"), 1, "default-bold")) < (getElementData(element,"width") - 10) then
			setElementData(element,"text",getElementData(element,"text")..control)
		end	
	end
end
end
end
end


function pressEditBox()
for id, element in ipairs (getElementsByType("sosaEdit")) do
	setElementData(element,"input",nil)
end
editActive = true
setElementData(source,"input",true)
removeEventHandler("onClientRender",getRootElement(),setEditText)
addEventHandler("onClientRender",getRootElement(),setEditText)
end
local keyStates = {}
function controls()
keyTable = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
 "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z","backspace","num_0", "num_1", "num_2", "num_3", "num_4", "num_5",
 "num_6", "num_7", "num_8", "num_9", "num_mul", "num_add", "num_sub", "num_div", "num_dec","space","[", "]", ";", ",", "-", ".", "/", "#", "\\", "="} 
for id, key in ipairs(keyTable) do
	if getKeyState(key) then
		if not KeyStates[id] then 
			KeyStates = {}
			KeyStates[id] = true
			if key == "num_0" then return "0"
			elseif key == "num_1" then return "1"
			elseif key == "num_2" then return "2"
			elseif key == "num_3" then return "3"
			elseif key == "num_4" then return "4"
			elseif key == "num_5" then return "5"
			elseif key == "num_6" then return "6"
			elseif key == "num_7" then return "7"
			elseif key == "num_8" then return "8"
			elseif key == "num_9" then return "9"
			elseif key == "num_add" then return "+"
			elseif key == "num_sub" then return "-"
			elseif key == "num_div" then return "/"
			elseif key == "num_mul" then return "*"
			elseif key == "num_dec" then return ","
			elseif key == "space" then return " "
			else
			if not (getKeyState("lshift") or getKeyState("rshift")) then
				return key
			else
				return string.upper(key)
			end
			end
		else
			return ""
		end
	end
end
KeyStates = {}
return ""
end

function sosaCreateGridList(parent, x, y, width, height, title)
	
end

function sosaGUIRender()
	--||Window||--
	for id, ele in ipairs(getElementsByType("sosaWindow")) do
		dxDrawImage(getElementData(ele,"x"), getElementData(ele,"y"), getElementData(ele,"width"), 30, "resources/images/gui/titlebar.png", 0, 0, 0, tocolor(255, 255, 255))
		dxDrawText(getElementData(ele,"title"), getElementData(ele,"x"), getElementData(ele,"y"), (getElementData(ele,"x")) + (getElementData(ele, "width")), getElementData(ele,"y") + 30, tocolor(255, 255, 255), 1, "default", "center", "center")
		dxDrawRectangle(getElementData(ele,"x"), getElementData(ele,"y") + 29, getElementData(ele,"width"), getElementData(ele,"height") - 30, tocolor(20, 20, 20, 220))
		if (getElementData(ele, "cEnabled")) == true then
			dxDrawImage(getElementData(ele,"x") + (getElementData(ele,"width") - 27.75), getElementData(ele,"y") + 2.5, 25, 25, "resources/images/gui/close.png", 0, 0, 0, tocolor(255, 255, 255, 255))
		end		
	end
	--||Label||--
	for id, ele in ipairs(getElementsByType("sosaLabel")) do
		dxDrawText(getElementData(ele, "text"), getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "x") + getElementData(ele, "width"),getElementData(ele, "y") + getElementData(ele, "height"), tocolor(getElementData(ele, "r"), getElementData(ele, "g"), getElementData(ele, "b"), 255), getElementData(ele, "size"), getElementData(ele, "font"), getElementData(ele, "alignX"), getElementData(ele, "alignY"), false, true)
	end
	--||Button||--
	for id, ele in ipairs(getElementsByType("sosaButton")) do
	if getElementData(ele, "hovered") then
		dxDrawImage(getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "leftWidth"), getElementData(ele, "height"), "resources/images/gui/buttonLeft_h.png")
		dxDrawImage(getElementData(ele, "x") + ((getElementData(ele, "width")) - (getElementData(ele, "rightWidth"))), getElementData(ele, "y"), getElementData(ele, "rightWidth"), getElementData(ele, "height"), "resources/images/gui/buttonRight_h.png")
		for i = 0, (getElementData(ele, "middleWidth") + 2) do
			dxDrawImage(getElementData(ele, "x") + (getElementData(ele, "leftWidth") - 1) + i, getElementData(ele, "y"), 1, getElementData(ele, "height"), "resources/images/gui/buttonMiddle_h.png")
		end	
		dxDrawText(getElementData(ele, "text"), getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "x") + getElementData(ele, "width"), getElementData(ele, "y") + getElementData(ele, "height"), tocolor(255, 255, 255), 1, "default", "center", "center")
	else
		dxDrawImage(getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "leftWidth"), getElementData(ele, "height"), "resources/images/gui/buttonLeft.png")
		dxDrawImage(getElementData(ele, "x") + ((getElementData(ele, "width")) - (getElementData(ele, "rightWidth"))), getElementData(ele, "y"), getElementData(ele, "rightWidth"), getElementData(ele, "height"), "resources/images/gui/buttonRight.png")
		for i = 0, (getElementData(ele, "middleWidth") + 2) do
			dxDrawImage(getElementData(ele, "x") + (getElementData(ele, "leftWidth") - 1) + i, getElementData(ele, "y"), 1, getElementData(ele, "height"), "resources/images/gui/buttonMiddle.png")
		end
		--dxDrawRectangle(getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "width"), getElementData(ele, "height"), tocolor(15, 15, 15, 190))
		dxDrawText(getElementData(ele, "text"), getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "x") + getElementData(ele, "width"), getElementData(ele, "y") + getElementData(ele, "height"), tocolor(255, 255, 255), 1, "default", "center", "center")
	end
	end
	--||EditBox||--
	for id, ele in ipairs(getElementsByType("sosaEdit")) do
		local text = getElementData(ele, "text")
		if getElementData(ele, "masked") then
			text = string.gsub(text,".","*")
		end
		--left--
		dxDrawRectangle(getElementData(ele, "x") + 1, getElementData(ele, "y") + 3, 2, 24, tocolor(235, 235, 235, 170))
		dxDrawRectangle(getElementData(ele, "x") + 3, getElementData(ele, "y") + 2, 2, 26, tocolor(235, 235, 235, 170))
		dxDrawRectangle(getElementData(ele, "x") + 5, getElementData(ele, "y") + 1, 2, 28, tocolor(235, 235, 235, 170))
		dxDrawImage(getElementData(ele,"x"), getElementData(ele, "y"), 7, 30, "resources/images/gui/edit_left.png", 0, 0, 0, tocolor(255, 255, 255, 205), true)
		--middle--
		for i = 0, (getElementData(ele, "width") - 14) do
			dxDrawRectangle(getElementData(ele, "x") + 7 + i, getElementData(ele, "y") + 1, 1, 28, tocolor(235, 235, 235, 170))
			dxDrawImage(getElementData(ele, "x") + 7 + i, getElementData(ele, "y"), 1, 30, "resources/images/gui/edit_middle.png", 0, 0, 0, tocolor(255, 255, 255, 205), true)
		end
		--right--
		dxDrawRectangle(getElementData(ele, "x") + getElementData(ele, "width") - 6, getElementData(ele, "y") + 1, 2, 28, tocolor(235, 235, 235, 170))		
		dxDrawRectangle(getElementData(ele, "x") + getElementData(ele, "width") - 4, getElementData(ele, "y") + 2, 2, 26, tocolor(235, 235, 235, 170))
		dxDrawRectangle(getElementData(ele, "x") + getElementData(ele, "width") - 2, getElementData(ele, "y") + 3, 1, 24, tocolor(235, 235, 235, 170))
		dxDrawImage((getElementData(ele, "x") + getElementData(ele, "width")) - 7, getElementData(ele, "y"), 7, 30, "resources/images/gui/edit_right.png", 0, 0, 0, tocolor(255, 255, 255, 205), true)
		
		dxDrawText(text, getElementData(ele, "x") + 5, getElementData(ele, "y") + 2, getElementData(ele, "width") - 5, getElementData(ele, "y") + 28, tocolor(30, 30, 30), 1, "default-bold", "left", "center")
		if editActive and not isChatBoxInputActive() and not isConsoleActive() and not isMainMenuActive() then
			cursortime = cursortime + 1
			if cursortime == 60 then cursortime = 0 end
			if cursortime < 30 then
				dxDrawRectangle(dxGetTextWidth(text, 1, "default-bold") + (getElementData(ele, "x") + 5), getElementData(ele, "y") + 3, 1, 24, tocolor(5, 5, 5, 240))
			end
		end
	end
	--||Image||--
	for id, ele in ipairs(getElementsByType("sosaImage")) do
		dxDrawImage(getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "width"), getElementData(ele, "height"), getElementData(ele, "image"), getElementData(ele, "rotation"))
		
	end
	--||Checkbox||--
	for id, ele in ipairs(getElementsByType("sosaCheckBox")) do
		if getElementData(ele, "selected") then
			dxDrawImage(getElementData(ele, "x"), getElementData(ele, "y"), 20, 20, "resources/images/gui/checkbox_checked.png")		
		else
			dxDrawImage(getElementData(ele, "x"), getElementData(ele, "y"), 20, 20, "resources/images/gui/checkbox_unchecked.png")
		end
		dxDrawText(getElementData(ele, "text"), getElementData(ele, "x") + 25, getElementData(ele, "y"), getElementData(ele, "x") + 25 + 40, getElementData(ele, "y") + 25, tocolor(255, 255, 255), 1.3)
	end
	--||GUI Field||--
	for id, ele in ipairs(getElementsByType("sosaGUIField")) do
		if getElementData(ele, "selected") then
			dxDrawRectangle(getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "width"), getElementData(ele, "height"), tocolor(195,105,0, 165))
		else
			dxDrawRectangle(getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "width"), getElementData(ele, "height"), tocolor(getElementData(ele, "r"), getElementData(ele, "g"), getElementData(ele, "b"), 165))
		end
		dxDrawText(getElementData(ele, "text"), getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "width"), getElementData(ele, "height"), white, 1, "default", "center", "center")
	end
	--||Outlined Rectangle||--
	for id, ele in ipairs(getElementsByType("outlinedRectangle")) do
		dxDrawRectangle(getElementData(ele, "x"), getElementData(ele, "y"), 1, getElementData(ele, "height"), tocolor(getElementData(ele, "r"), getElementData(ele, "g"), getElementData(ele, "b")))
		dxDrawRectangle(getElementData(ele, "x") + getElementData(ele, "width"), getElementData(ele, "y"), 1, getElementData(ele, "height"), tocolor(getElementData(ele, "r"), getElementData(ele, "g"), getElementData(ele, "b")))
		dxDrawRectangle(getElementData(ele, "x"), getElementData(ele, "y") + getElementData(ele, "height") - 1, getElementData(ele, "width"), 1, tocolor(getElementData(ele, "r"), getElementData(ele, "g"), getElementData(ele, "b")))
		dxDrawRectangle(getElementData(ele, "x"), getElementData(ele, "y"), getElementData(ele, "width"), 1, tocolor(getElementData(ele, "r"), getElementData(ele, "g"), getElementData(ele, "b")))
	end
end


function dxDrawAnimWindow(text,height,width,color,font,anim)
    local x,y = guiGetScreenSize()
 
    btwidth = width
    btheight = height/20
 
    local now = getTickCount()
    local elapsedTime = now - start
    local endTime = start + 1500
    local duration = endTime - start
    local progress = elapsedTime / duration
    local x1, y1, z1 = interpolateBetween ( 0, 0, 0, width, height, 255, progress, anim)
    local x2, y2, z2 = interpolateBetween ( 0, 0, 0, btwidth, btheight, btheight/11, progress, anim)
 
    posx = (x/2)-(x1/2)
    posy = (y/2)-(y1/2)
 
    dxDrawRectangle ( posx, posy-y2, x2, y2, color )
    dxDrawRectangle ( posx, posy, x1, y1, tocolor ( 0, 0, 0, 200 ) )
    dxDrawText ( text, 0, -(y1)-y2, x, y, tocolor ( 255, 255, 255, 255 ), z2,font,"center","center")   
 
 
end

function sosaDestroyElements()
	for id, ele in ipairs(getElementsByType("sosaWindow")) do
		destroyElement(ele)
		sosaVisible = false
	end
end

function sosaSetText(element, text)
	if not element then
		return false
	else
		setElementData(element, "text", text)
	end
end



function sosaSetState(element, state)
	if not element then
		return false
	else
		setElementData(element, "selected", state)
	end
end	

function sosaGetState(element)
	if not element then
		return false
	else
		return getElementData(element, "selected")
	end	
end

function sosaClickHandler(button, state, absX, absY, wX, wY, wZ)
if button == "left" and state == "down" then
	--Button--
	for id, ele in ipairs(getElementsByType("sosaButton")) do
		if (absX > getElementData(ele, "x")) and (absX < (getElementData(ele, "x") + getElementData(ele, "width"))) and (absY > getElementData(ele, "y")) and (absY < (getElementData(ele, "y") + getElementData(ele, "height"))) then
			triggerEvent("onSosaClick", ele)
			playSound("resources/sounds/gui/click.wav")
		end
	end
	
	for id, ele in ipairs(getElementsByType("sosaCheckBox")) do
		if (absX > getElementData(ele, "x")) and (absX < (getElementData(ele, "x") + 30)) and (absY > getElementData(ele, "y")) and (absY < (getElementData(ele, "y") + 30)) then
			triggerEvent("onSosaClick", ele)
			playSound("resources/sounds/gui/click.wav")			
		end
	end	
	
	for id, ele in ipairs(getElementsByType("sosaGUIField")) do
		if (absX > getElementData(ele, "x")) and (absX < (getElementData(ele, "x") + getElementData(ele, "width"))) and (absY > getElementData(ele, "y")) and (absY < (getElementData(ele, "y") + getElementData(ele, "height"))) then
			triggerEvent("onSosaClick", ele)
			playSound("resources/sounds/gui/click.wav")			
		end
	end
	--EditBox--
	for id, ele in ipairs(getElementsByType("sosaEdit")) do
		if(absX > getElementData(ele, "x")) and (absX < (getElementData(ele, "x") + getElementData(ele, "width"))) and (absY > getElementData(ele, "y")) and (absY < (getElementData(ele, "y") + 30)) then
			triggerEvent("onSosaEditClick", ele)
			playSound("resources/sounds/gui/click.wav")			
		else
			editActive = false
		end
	end
end	
end

addEvent("onSosaWindowClose", true)
addEvent("onSosaEditClick", true)
addEvent("onSosaClick", true)
addEventHandler("onClientClick", getRootElement(), sosaClickHandler)
addEventHandler("onClientRender", getRootElement(), sosaGUIRender)


