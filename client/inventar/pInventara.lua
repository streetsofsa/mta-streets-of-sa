pInventar = {datas = {}, cardatas = {}}
pInventar.settings = {
	visible = false
}

pInventar.tab = {
	[1] = true, -- Item Tab
	[2] = false -- Fahrzeug Tab
}

pInventar.datas.descriptions = {
	["nil"] = "",
	["Snacks"] = "Das kleine Schoko-Haselnuss-Wunder für Unterwegs. Immer gut für den kleinen Hunger",
	["Hamburger"] = "Mit knackigem Salat, frisch gegrilltem Fleisch und einer himmlisch leckeren Soße ist dieser Hamburger die perfekte Mahlzeit.",
	["essen_slot3"] = "",
	["Drogen"] = "Wäre es nicht schön, etwas Spaß in sein langweiliges Leben zu hauchen? Aber pass auf, sonst könntest du als Junkie im Gefängnis enden!",
	["Mats"] = "Mats benötigs du für die Herstellung von Waffen."
}

pInventar.datas.current = 0
pInventar.cardatas.current = 0

pInventar.gui = {
	window = {},
	label = {},
	field = {},
	data = {
		[1] = {},
		[2] = {},
		[3] = {},
		[4] = {},
		[5] = {},
		[6] = {},
		[7] = {},
		[8] = {}
	},
	area = {},
	button = {}
}

local sX, sY = guiGetScreenSize()

function pInventar.createGUI()
	addEventHandler("onClientClick", getRootElement(), onClientClick)
	pInventar.gui.window[1] = sosaCreateWindow((sX/2) - 250, (sY/2) - 250, 500, 515, "Inventar", true, true)
	--||Item Area||--
	pInventar.gui.area[1] = sosaCreateOutlinedRectangle(pInventar.gui.window[1], 0, 30, 273, 480, 0, 0, 0) 
	--||Slots||--	
	--1--
	pInventar.gui.field[1] = sosaCreateGUIField(pInventar.gui.window[1], 0, 30, 273, 60, "", 10, 10, 10)
	pInventar.gui.data[1][1] = sosaCreateLabel(pInventar.gui.window[1], 0, 30, 273, 60, "", 255, 255, 255, "default-bold", "left", "center", 1.2)
	pInventar.gui.data[1][2] = sosaCreateLabel(pInventar.gui.window[1], 0, 30, 273, 60, "", 255, 255, 255, "default-bold", "right", "center", 1.2)	
	--2--
	pInventar.gui.field[2] = sosaCreateGUIField(pInventar.gui.window[1], 0, 90, 273, 60, "", 25, 25, 25)
	pInventar.gui.data[2][1] =  sosaCreateLabel(pInventar.gui.window[1], 0, 90, 273, 60, "", 255, 255, 255, "default-bold", "left", "center", 1.2)
	pInventar.gui.data[2][2] = sosaCreateLabel(pInventar.gui.window[1], 0, 90, 273, 60, "", 255, 255, 255, "default-bold", "right", "center", 1.2)	
	--3--
	pInventar.gui.field[3] = sosaCreateGUIField(pInventar.gui.window[1], 0, 150, 273, 60, "", 10, 10, 10)
	pInventar.gui.data[3][1] = sosaCreateLabel(pInventar.gui.window[1], 0, 150, 273, 60, "", 255, 255, 255, "default-bold", "left", "center", 1.2)
	pInventar.gui.data[3][2] = sosaCreateLabel(pInventar.gui.window[1], 0, 150, 273, 60, "", 255, 255, 255, "default-bold", "right", "center", 1.2)	
	--4--
	pInventar.gui.field[4] = sosaCreateGUIField(pInventar.gui.window[1], 0, 210, 273, 60, "", 25, 25, 25)
	pInventar.gui.data[4][1] = sosaCreateLabel(pInventar.gui.window[1], 0, 210, 273, 60, "", 255, 255, 255, "default-bold", "left", "center", 1.2)
	pInventar.gui.data[4][2] = sosaCreateLabel(pInventar.gui.window[1], 0, 210, 273, 60, "", 255, 255, 255, "default-bold", "right", "center", 1.2)	
	--5--
	pInventar.gui.field[5] = sosaCreateGUIField(pInventar.gui.window[1], 0, 270, 273, 60, "", 10, 10, 10)
	pInventar.gui.data[5][1] = sosaCreateLabel(pInventar.gui.window[1], 0, 270, 273, 60, "", 255, 255, 255, "default-bold", "left", "center", 1.2)
	pInventar.gui.data[5][2] = sosaCreateLabel(pInventar.gui.window[1], 0, 270, 273, 60, "", 255, 255, 255, "default-bold", "right", "center", 1.2)	
	--6--
	pInventar.gui.field[6] = sosaCreateGUIField(pInventar.gui.window[1], 0, 330, 273, 60, "", 25, 25, 25)
	pInventar.gui.data[6][1] = sosaCreateLabel(pInventar.gui.window[1], 0, 330, 273, 60, "", 255, 255, 255, "default-bold", "left", "center", 1.2)
	pInventar.gui.data[6][2] = sosaCreateLabel(pInventar.gui.window[1], 0, 330, 273, 60, "", 255, 255, 255, "default-bold", "right", "center", 1.2)	
	--7--
	pInventar.gui.field[7] = sosaCreateGUIField(pInventar.gui.window[1], 0, 390, 273, 60, "", 10, 10, 10)
	pInventar.gui.data[7][1] = sosaCreateLabel(pInventar.gui.window[1], 0, 390, 273, 60, "", 255, 255, 255, "default-bold", "left", "center", 1.2)
	pInventar.gui.data[7][2] = sosaCreateLabel(pInventar.gui.window[1], 0, 390, 273, 60, "", 255, 255, 255, "default-bold", "right", "center", 1.2)	
	--8--
	pInventar.gui.field[8] = sosaCreateGUIField(pInventar.gui.window[1], 0, 450, 273, 60, "", 25, 25, 25)
	pInventar.gui.data[8][1] = sosaCreateLabel(pInventar.gui.window[1], 0, 450, 273, 60, "", 255, 255, 255, "default-bold", "left", "center", 1.2)
	pInventar.gui.data[8][2] = sosaCreateLabel(pInventar.gui.window[1], 0, 450, 273, 60, "", 255, 255, 255, "default-bold", "right", "center", 1.2)	
	--||||--
	pInventar.gui.label[2] = sosaCreateLabel(pInventar.gui.window[1], 274, 50, 225, 100, "Items", 255, 255, 255, "default-bold", "center", "top", 1.3)
	pInventar.gui.button[3] = sosaCreateButton(pInventar.gui.window[1], 280, 45, 30, 30, "<")
	pInventar.gui.button[4] = sosaCreateButton(pInventar.gui.window[1],  464, 45, 30, 30, ">")
	
	pInventar.gui.label[1] = sosaCreateLabel(pInventar.gui.window[1], 274, 240, 225, 60, "", 255, 255, 255, "default", "center", "center", 1)
	
	addEventHandler("onSosaClick", pInventar.gui.button[3], tabHandler)
	addEventHandler("onSosaClick", pInventar.gui.button[4], tabHandler)
	addEventHandler("onSosaWindowClose", pInventar.gui.window[1], function() 
		removeEventHandler("onClientClick", getRootElement(), onClientClick)
		pInventar.settings.visible = false 
	end)
	pInventar.createTab1()
end

function pInventar.createTab1()
	--||Slots||--
	--1--
	if pInventar.datas.items[1] then
	if tonumber(pInventar.datas.items[1].v) > 0 then
		sosaSetText(pInventar.gui.data[1][1], pInventar.datas.items[1].i)
		sosaSetText(pInventar.gui.data[1][2], pInventar.datas.items[1].v)
	end
	end
	--2--
	if pInventar.datas.items[2] then	
	if tonumber(pInventar.datas.items[2].v) > 0 then
		sosaSetText(pInventar.gui.data[2][1], pInventar.datas.items[2].i)
		sosaSetText(pInventar.gui.data[2][2], pInventar.datas.items[2].v)
	end
	end
	--3--
	if pInventar.datas.items[3] then
	if tonumber(pInventar.datas.items[3].v) > 0 then
		sosaSetText(pInventar.gui.data[3][1], pInventar.datas.items[3].i)
		sosaSetText(pInventar.gui.data[3][2], pInventar.datas.items[3].v)
	end
	end
	--4--
	if pInventar.datas.items[4] then
	if tonumber(pInventar.datas.items[4].v) > 0 then
		sosaSetText(pInventar.gui.data[4][1], pInventar.datas.items[4].i)
		sosaSetText(pInventar.gui.data[4][2], pInventar.datas.items[4].v)
	end
	end
	--5--
	if pInventar.datas.items[5] then
	if tonumber(pInventar.datas.items[5].v) > 0 then
		sosaSetText(pInventar.gui.data[5][1], pInventar.datas.items[5].i)
		sosaSetText(pInventar.gui.data[5][2], pInventar.datas.items[5].v)
	end
	end
	--6--
	if pInventar.datas.items[6] then
	if tonumber(pInventar.datas.items[6].v) > 0 then
		sosaSetText(pInventar.gui.data[6][1], pInventar.datas.items[6].i)
		sosaSetText(pInventar.gui.data[6][2], pInventar.datas.items[6].v)
	end
	end
	--7--
	if pInventar.datas.items[7] then
	if tonumber(pInventar.datas.items[7].v) > 0 then
		sosaSetText(pInventar.gui.data[7][1], pInventar.datas.items[7].i)
		sosaSetText(pInventar.gui.data[7][2], pInventar.datas.items[7].v)
	end
	end
	--8--
	if pInventar.datas.items[8] then
	if tonumber(pInventar.datas.items[8].v) > 0 then
		sosaSetText(pInventar.gui.data[8][1], pInventar.datas.items[8].i)
		sosaSetText(pInventar.gui.data[8][2], pInventar.datas.items[8].v)
	end
	end
	--||||--
	
	pInventar.gui.button[1] = sosaCreateButton(pInventar.gui.window[1], 299, 350, 175, 30, "Benutzen")
	pInventar.gui.button[2] = sosaCreateButton(pInventar.gui.window[1], 299, 400, 175, 30, "Wegwerfen")
	--addEventHandler("onSosaClick", pInventar.gui.button[1], tabHandler)
	--addEventHandler("onSosaClick", pInventar.gui.button[2], tabHandler)
end


function pInventar.createTab2()
	--||Slots||--
	--1--
	if pInventar.cardatas.items[1] then
		sosaSetText(pInventar.gui.data[1][1], pInventar.cardatas.items[1].i)
		sosaSetText(pInventar.gui.data[1][2], "Slot: "..pInventar.cardatas.items[1].slot)
	end
	--2--
	if pInventar.cardatas.items[2] then
		sosaSetText(pInventar.gui.data[2][1], pInventar.cardatas.items[2].i)
		sosaSetText(pInventar.gui.data[2][2], "Slot: "..pInventar.cardatas.items[2].slot)
	end	
	--3--
	if pInventar.cardatas.items[3] then	
		sosaSetText(pInventar.gui.data[3][1], pInventar.cardatas.items[3].i)
		sosaSetText(pInventar.gui.data[3][2], "Slot: "..pInventar.cardatas.items[3].slot)
	end	
	--4--
	if pInventar.cardatas.items[4] then	
		sosaSetText(pInventar.gui.data[4][1], pInventar.cardatas.items[4].i)
		sosaSetText(pInventar.gui.data[4][2], "Slot: "..pInventar.cardatas.items[4].slot)
	end	
	--5--
	if pInventar.cardatas.items[5] then	
		sosaSetText(pInventar.gui.data[5][1], pInventar.cardatas.items[5].i)
		sosaSetText(pInventar.gui.data[5][2], "Slot: "..pInventar.cardatas.items[5].slot)
	end	
	--6--
	if pInventar.cardatas.items[6] then	
		sosaSetText(pInventar.gui.data[6][1], pInventar.cardatas.items[6].i)
		sosaSetText(pInventar.gui.data[6][2], "Slot: "..pInventar.cardatas.items[6].slot)
	end	
	--7--
	if pInventar.cardatas.items[7] then	
		sosaSetText(pInventar.gui.data[7][1], pInventar.cardatas.items[7].i)
		sosaSetText(pInventar.gui.data[7][2], "Slot: "..pInventar.cardatas.items[7].slot)
	end	
	--8--
	if pInventar.cardatas.items[8] then	
		sosaSetText(pInventar.gui.data[8][1], pInventar.cardatas.items[8].i)
		sosaSetText(pInventar.gui.data[8][2], "Slot: "..pInventar.cardatas.items[8].slot)
	end
	--||||--
	

end


function tabHandler()
table.foreach(pInventar.gui.data, function(i, v)
	sosaSetText(pInventar.gui.data[i][1], "")
	sosaSetText(pInventar.gui.data[i][2], "")
end)

	if pInventar.tab[1] then
		pInventar.tab[1] = not pInventar.tab[1]
		pInventar.tab[2] = not pInventar.tab[2]
		sosaSetText(pInventar.gui.label[1], "")
		--destroyElement(pInventar.gui.button[1])
		--destroyElement(pInventar.gui.button[2])
		pInventar.createTab2()
		sosaSetText(pInventar.gui.label[2], "Fahrzeuge")	
	elseif pInventar.tab[2] then
		pInventar.tab[1] = not pInventar.tab[1]
		pInventar.tab[2] = not pInventar.tab[2]	
		sosaSetText(pInventar.gui.label[1], "")
		pInventar.createTab1()
		sosaSetText(pInventar.gui.label[2], "Items")
	end
end

function onClientClick(btn, state, absX, absY)
absX = absX
absY = absY
local wX = (sX/2) - 250
local wY = (sY/2) - 250
if pInventar.tab[1] then -- Wenn "Fahrzeuge" Tab offen
	if btn == "left" and state == "down" then
		if (absX >	0+ wX) and (absX < 0 + 273+ wX) and (absY > 30 + wY) and (absY < 30 + 60 + wY) then -- GUIField 1
			pInventar.datas.current = 1
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 90 + wY) and (absY < 90 + 60 + wY) then -- GUIField 2
			pInventar.datas.current = 2
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 150 + wY) and (absY < 150 + 60 + wY) then -- GUIField 3	
			pInventar.datas.current = 3
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 210 + wY) and (absY < 210 + 60 + wY) then -- GUIField 4	
			pInventar.datas.current = 4
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 270 + wY) and (absY < 270 + 60 + wY) then -- GUIField 5
			pInventar.datas.current = 5
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 330 + wY) and (absY < 330 + 60 + wY) then -- GUIField 6
			pInventar.datas.current = 6
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 390 + wY) and (absY < 390 + 60 + wY) then -- GUIField 7
			pInventar.datas.current = 7
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 450 + wY) and (absY < 450 + 60 + wY) then -- GUIField 8	
			pInventar.datas.current = 8
		end	
		sosaSetState(pInventar.gui.field[pInventar.datas.current], true)
		
		if pInventar.datas.current > 0 then
		if pInventar.datas.descriptions[pInventar.datas.items[pInventar.datas.current].i] then
			sosaSetText(pInventar.gui.label[1], pInventar.datas.descriptions[pInventar.datas.items[pInventar.datas.current].i])
		else	
			sosaSetText(pInventar.gui.label[1], "Keine Beschreibung für Item: "..pInventar.datas.items[pInventar.datas.current].i.."!")		
		end
		end
	end	
elseif pInventar.tab[2] then -- Wenn "Items" Tab offen
	if btn == "left" and state == "down" then
		if (absX >	0+ wX) and (absX < 0 + 273+ wX) and (absY > 30 + wY) and (absY < 30 + 60 + wY) then -- GUIField 1
			--pInventar.datas.current = 1
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 90 + wY) and (absY < 90 + 60 + wY) then -- GUIField 2
			--pInventar.datas.current = 2
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 150 + wY) and (absY < 150 + 60 + wY) then -- GUIField 3	
			--pInventar.datas.current = 3
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 210 + wY) and (absY < 210 + 60 + wY) then -- GUIField 4	
			--pInventar.datas.current = 4
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 270 + wY) and (absY < 270 + 60 + wY) then -- GUIField 5
			--pInventar.datas.current = 5
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 330 + wY) and (absY < 330 + 60 + wY) then -- GUIField 6
			--pInventar.datas.current = 6
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 390 + wY) and (absY < 390 + 60 + wY) then -- GUIField 7
			--pInventar.datas.current = 7
		elseif (absX >	0 + wX) and (absX < 0 + 273 + wX) and (absY > 450 + wY) and (absY < 450 + 60 + wY) then -- GUIField 8	
			--pInventar.datas.current = 8
		end	
		--sosaSetState(pInventar.gui.field[pInventar.datas.current], true)
	
		--if pInventar.datas.descriptions[pInventar.datas.items[pInventar.datas.current].i] then
			sosaSetText(pInventar.gui.label[1], "---")
		--else	
			sosaSetText(pInventar.gui.label[1], "")		
		--end
	end	

end
end

function pInventar.getDatas(datas, cardatas)
if not pInventar.settings.visible then
	pInventar.settings.visible = true
	local invDatas = {}
	pInventar.settings.rows = {}
	pInventar.datas.items = {}	
	pInventar.cardatas.items = {}	
	table.foreach(datas, function(i, v)
		if type(v) == "userdata" then
			v = 1
		end
		if v > 0 then
			table.insert(pInventar.datas.items, { i = i, v = v})
			table.insert(pInventar.settings.rows, {i = i, oX = 0, v = v})
		end
	end)
	table.foreach(cardatas, function(i, v)
		--outputChatBox("--I--")
		--outputChatBox(tostring(i))
		--outputChatBox("--V--")
		--outputChatBox(tostring(v))
		--|| I = 1/I[:](Bullet)
		--|| V = Userdata(Userdata)
		--|| Slot = 2/I[:](2)
		--table.insert(pInventar.cardatas.items, { i = gettok(i, 1, ":"), v = v, slot = gettok(i, 2, ":")})	
		table.insert(pInventar.cardatas.items, { i = gettok(v, 2, ":"), v = i, slot = gettok(v, 1, ":")})
	end)
	pInventar.settings.visible = true
	showCursor(true)
	pInventar.createGUI()
else
	outputChatBox("JD")
end	
end

addEvent("updateInventarData", true)
addEventHandler("updateInventarData", getLocalPlayer(), pInventar.getDatas)