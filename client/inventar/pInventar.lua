--||||||||||||||||||||||||||||||||||||
--|||Name: Player Inventar(Client) |||
--|||Project: MTA Streets of SA    |||
--|||Author: Apo                   |||
--|||Version: 1.0                  |||
--||||||||||||||||||||||||||||||||||||


local inv = {}
local screen = {guiGetScreenSize()}
local native = {screen[1]/1920, screen[2]/1080}

cInventar = {}

cInventar.descriptions = {
	["nil"] = "",
	["Snacks"] = "Das kleine Schoko-Haselnuss-Wunder für Unterwegs. Immer gut für den kleinen Hunger",
	["Hamburger"] = "Mit knackigem Salat, frisch gegrilltem Fleisch und einer himmlisch leckeren Soße ist dieser Hamburger die perfekte Mahlzeit.",
	["essen_slot3"] = "",
	["Drogen"] = "Wäre es nicht schön, etwas Spaß in sein langweiliges Leben zu hauchen? Aber pass auf, sonst könntest du als Junkie im Gefängnis enden!",
	["Mats"] = "Mats benötigs du für die Herstellung von Waffen."
}

function cInventar:create(...)
	local obj = setmetatable ({},{__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
end

function cInventar:constructor(items)
	self.vehicles = {}
	table.foreach(items, function(i, v)
		if string.find(i, "VehicleKey") then
			self.vehicles["Fahrzeugschlüssel: "..(gettok(i, 2, ":"))] = v
			items[i] = nil
		end
	end)
	
	
	self.items = items
	self.tab = 1
	self.x = (screen[1]/2) - 250
	self.y = (screen[2]/2) - 250
	self.w = 500
	self.h = 481
	self.visible = true
	self.active = {item = {}, cars = {}}
	
	
	outputChatBox("INV")
	
	self:initializeGUI()
end

function cInventar:initializeGUI()
	self.gui = {
	tab1 = {button = {}, f = {}}, 
	}
	--||GUI Elemente Allgemein
	self.gui.window = sosaGUI.window:new(self.x, self.y, self.w, self.h, "Inventar")
	self.gui.label = sosaGUI.label:new(self.gui.window, 270*native[1], (self.h/2-30)*native[2], (self.w - 275)*native[1], 100*native[2], "", "default",  "center", "top")
	
	--
	--Da der Code auch beim Wechseln des Tabs ausgeführt wird,
	--wird die Funktion aufgerufen, damit der Code nicht verdoppelt wird
	self:toggle()
	
	self.renderFunc = function() self:Render() end
	self.clickFunc = function(...) self:Click(...) end
	
	addEventHandler("onClientRender", root, self.renderFunc)
	addEventHandler("onClientClick", root, self.clickFunc)
end


function cInventar:toggle()

	
	--||GUI Dinge für Tab 1(Item Tab)
	self.gui.tab1.button[1] = sosaGUI.button:new(self.gui.window, 290*native[2], (self.h/2+70)*native[2], 185, 30, "Benutzen", function() self:action("!use", self.active.item) end)
	self.gui.tab1.button[2] = sosaGUI.button:new(self.gui.window, 290*native[2], (self.h/2+110)*native[2], 185, 30, "Wegwerfen", function() self:action("!use", self.active.item) end)
	
	for i=1, 8 do
		self.gui.tab1.f[i] = {}
				
		if (i%2 == 0) then
			self.gui.tab1.f[i][1] = sosaGUI.actionfield:new(self.gui.window, 0, i*(60*native[2])-60, 265*native[1], 60*native[2], "", {20,20,20,5,5,5}, function() if self.items[i] then self.active.item = self.items[i] else self.active.item = {} end end)
		else
			self.gui.tab1.f[i][1] = sosaGUI.actionfield:new(self.gui.window, 0, i*(60*native[2])-60, 265*native[1], 60*native[2], "", {10,10,10,5,5,5}, function() if self.items[i] then self.active.item = self.items[i] else self.active.item = {} end end)
		end
		if self.items[i] then	
			self.gui.tab1.f[i][2] = sosaGUI.label:new(self.gui.window, 1, i*(60*native[2])-60, 265*native[1], 60*native[2], tostring(self.items[i][1]) or "", "default-bold", "left")
			self.gui.tab1.f[i][3] = sosaGUI.label:new(self.gui.window, 0, i*(60*native[2])-60, 265*native[1], 60*native[2], tostring(self.items[i][2]) or "", "default-bold", "right")	
		end	
	end	
	--||||--
		
	--||||--
end


function cInventar:action(cmd, item)
	if cmd == "!use" then
		outputChatBox(tostring(item[1]).." : "..tostring(item[2]))
	
	elseif cmd == "!find" then
		local epos = {getElementPosition(item[2])}
		outputChatBox(getZoneName(epos[1], epos[2], epos[3], false))
	
	elseif cmd == "!lock" then
		
	end
end



--|| Alles u
function cInventar:Render()
	if not self.visible then
		return
	end
	dxDrawRectangle(self.x + 265, self.y+20, 1, self.h-20, tocolor(0,0,0))
end

function cInventar:Click(btn, state, absX, absY)
	if (absX > self.x + self.w - 27.75) and (absX < self.x + self.w - 2) and (absY > self.y - 27.5) and (absY < self.y - 2) then
		self:deconstructor()
	end
	
	if not(btn == "left" and state == "down") and (not isCursorOnElement(self.x, self.y, self.w, self.h)) then
	
		return
	end
	
	
	self.gui.label:setText(cInventar.descriptions[tostring(self.active.item[1])])
end

function cInventar:deconstructor()
	removeEventHandler("onClientRender", root, self.renderFunc)
	removeEventHandler("onClientClick", root, self.clickFunc)
	
	self = nil
	inv[getLocalPlayer()] = nil
end


addEvent("c.playerClass:openInventory", true)
addEventHandler("c.playerClass:openInventory", getLocalPlayer(), function(items, cars)

	if not inv[getLocalPlayer()] then
		inv[getLocalPlayer()] = true
		local t1 = {items = {}, vehs = {}}
		table.foreach(items, function(i, v)
			table.insert(t1.items, {i, v})
		end)
		table.foreach(cars, function(i, v)
			table.insert(t1.vehs, {i, v})
		end)
		cInventar:create(t1.items, t1.vehs)
	end
end)


