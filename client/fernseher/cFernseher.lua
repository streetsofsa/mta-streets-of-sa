--|||||||||||||||||||||||||||||||||||||
--||||Name: TV System              ||||
--||||Author: Apoollo, ravecow     ||||
--||||Project: Streets of SA       ||||
--||||Version: 1.0                 ||||
--|||||||||||||||||||||||||||||||||||||
local TVS = {}

Fernseher = {}
Fernseher.events = {
	"Fernseher:On",
	"Fernseher:Off",
	"Fernseher:setChannel"
}
TVChannels = {
	"SA News",
	"Vinewood TV"
}

addEvent("Fernseher:New", true)
addEventHandler("Fernseher:New", root, function(data)
	TVS[#TVS+1] = Fernseher:new(data)
end)

table.foreach(Fernseher.events, function(_, v)
	addEvent(v, true)
end)



function Fernseher:new(...)
	local obj = setmetatable({}, {__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
end


function Fernseher:turnOn()
	fadeCamera(false, 0.8)
	setTimer(function()
		hud.visible = false
		toggleAllControls(false)
		addEventHandler("onClientRender", root, self.renderFunc)
		fadeCamera(true, 0.5)
	end, 750, 1)	
end

function Fernseher:turnOff()
	fadeCamera(false, 0.8)
	setTimer(function()
		toggleAllControls(true)
		removeEventHandler("onClientRender", root, self.renderFunc)
		fadeCamera(true, 0.5)
	end, 750, 1)	
end

function Fernseher:ChangeChannel(channel)
	fadeCamera(false, 0.5)
	setTimer(function()
		self.channel = channel
		fadeCamera(true, 0.5)
	end, 500, 1)	
end

function Fernseher:constructor(data)
	self.tv = data.tv
	self.channel = data.channel
	self.events = {}
	
	self.screen = {guiGetScreenSize()}
	
	self.onFunc = function(...) self:turnOn(...) end	
	self.offFunc = function(...) self:turnOff(...) end	
	self.channelFunc = function(...) self:ChangeChannel(...) end	
	
	self.renderFunc = function(...) self:onRender(...) end
	self.tick = getTickCount()
	
	self.events.on = addEventHandler("Fernseher:On", root, self.onFunc)
	self.events.off = addEventHandler("Fernseher:Off", root, self.offFunc)
	self.events.ChangeChannel = addEventHandler("Fernseher:setChannel", root, self.channelFunc)
	
end

function Fernseher:onRender()
	dxDrawRectangle(0, 0, self.screen[1], 80*(1080/self.screen[2]), tocolor(0,0,0))
	dxDrawRectangle(0, self.screen[2] - (80*(1080/self.screen[2])), self.screen[1], 80*(1080/self.screen[2]), tocolor(0,0,0))
	dxDrawImage(self.screen[1] - (175*(1920/self.screen[1])) - 5, 5 + 80*(1080/self.screen[2]), 175*(1920/self.screen[1]), 75*(1080/self.screen[2]), "resources/images/tv/channels/"..self.channel..".png")	
	if self.channel == 1 then
		dxDrawImage(0, self.screen[2] - (80*(1080/self.screen[2])) - (104*(1080/self.screen[2])), self.screen[1], 104*(1080/self.screen[2]), "resources/images/tv/news/news.png")
		dxDrawImage(getTickCount() - self.tick, self.screen[2] - (80*(1080/self.screen[2])) - (104*(1080/self.screen[2])) - 40*(1080/self.screen[2]), 290*(1920/self.screen[1]), 40*(1080/self.screen[2]), "resources/images/tv/news/breaking.png")
	end
end



