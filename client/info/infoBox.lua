local sX, sY = guiGetScreenSize()
local width = 450 
local height = 130 
local iBoxData = {}

function infobox(text, type, time)
	if infoboxVisible then return end
	infoboxVisible = true
	if type == nil then type = "info" end
	iBoxData.x = (sX - width) / 2
	iBoxData.y = 0
	iBoxData.width = width
	iBoxData.height = height
	iBoxData.text = text
	iBoxData.type = type
	addEventHandler("onClientRender", getRootElement(), infoboxRender)
	playSound("resources/sounds/info.wav")
	setTimer(function() removeEventHandler("onClientRender", getRootElement(), infoboxRender) infoboxVisible = false end, 2000, 1)
end

function infoboxRender()
	dxDrawRectangle(iBoxData.x, iBoxData.y, iBoxData.width, iBoxData.height, tocolor(2, 2, 2, 190))
	if (iBoxData.type == "error") then
		dxDrawImage(iBoxData.x + 25, iBoxData.y + 25, 80, 80, "resources/images/info/error.png")
	elseif (iBoxData.type == "info") then
		dxDrawImage(iBoxData.x + 25, iBoxData.y + 25, 80, 80, "resources/images/info/info.png")
	elseif (iBoxData.type == "sucess") then
		dxDrawImage(iBoxData.x + 25, iBoxData.y + 25, 80, 80, "resources/images/info/sucess.png")
	end

	dxDrawText(iBoxData.text, iBoxData.x + 125, 0, iBoxData.x + iBoxData.width, iBoxData.height, tocolor(255, 255, 255), 1.2, "default-bold", "center", "center", false, true)
end


addEvent("_infobox", true)
addEventHandler("_infobox", getRootElement(), function(text, type)
	infobox(text, type)
end)