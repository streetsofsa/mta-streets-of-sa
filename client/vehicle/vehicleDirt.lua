vehicleDirt = {}

function vehicleDirt:new (...)
	local obj = setmetatable ( {}, {__index = self} )
	if obj.constructor then
		obj:constructor (...)
	end
	return obj
end

function vehicleDirt:constructor(...)
	self.dirtTable = {}
	self.dirtTable.textures = "resources/textures/vehicleDirt/"
	self.dirtTable.shader = "resources/shader/vehicleDirt/"
end