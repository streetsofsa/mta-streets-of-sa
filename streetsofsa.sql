-- phpMyAdmin SQL Dump
-- version 4.3.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Erstellungszeit: 27. Mrz 2015 um 17:37
-- Server-Version: 5.6.23
-- PHP-Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Datenbank: `streetsofsa`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `banned`
--

CREATE TABLE `banned` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Admin` text NOT NULL,
  `Reason` text NOT NULL,
  `Date` date NOT NULL,
  `IP` text NOT NULL,
  `Serial` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `factiondepot`
--

CREATE TABLE `factiondepot` (
  `fID` varchar(255) NOT NULL DEFAULT '',
  `Kasse` varchar(255) DEFAULT NULL,
  `Drogen` varchar(255) DEFAULT NULL,
  `Mats` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `factiondepot`
--

INSERT INTO `factiondepot` (`fID`, `Kasse`, `Drogen`, `Mats`) VALUES
('1', '0', '0', '0'),
('2', '0', '0', '0'),
('3', '0', '0', '0'),
('4', '0', '0', '0'),
('5', '0', '0', '0'),
('6', '0', '0', '0'),
('7', '0', '0', '0'),
('8', '0', '0', '0'),
('9', '0', '0', '0'),
('10', '0', '0', '0'),
('11', '0', '0', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `factionvehicles`
--

CREATE TABLE `factionvehicles` (
  `ID` int(11) NOT NULL,
  `Faction` int(11) NOT NULL,
  `Rank` int(11) NOT NULL DEFAULT '1',
  `VehicleID` int(11) NOT NULL,
  `PosX` text CHARACTER SET utf8 NOT NULL,
  `PosY` text CHARACTER SET utf8 NOT NULL,
  `PosZ` text CHARACTER SET utf8 NOT NULL,
  `Rotation` text CHARACTER SET utf8 NOT NULL,
  `R1` int(11) NOT NULL DEFAULT '0',
  `G1` int(11) NOT NULL DEFAULT '0',
  `B1` int(11) NOT NULL DEFAULT '0',
  `R2` int(11) NOT NULL DEFAULT '0',
  `G2` int(11) NOT NULL DEFAULT '0',
  `B2` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `factionvehicles`
--

INSERT INTO `factionvehicles` (`ID`, `Faction`, `Rank`, `VehicleID`, `PosX`, `PosY`, `PosZ`, `Rotation`, `R1`, `G1`, `B1`, `R2`, `G2`, `B2`) VALUES
(1, 1, 1, 596, '1591.5', '-1710.3000488281', '5.6999998092651', '0', 0, 0, 0, 255, 255, 255),
(2, 1, 1, 596, '1595.5', '-1710.2998046875', '5.6999998092651', '0', 0, 0, 0, 255, 255, 255),
(3, 1, 1, 596, '1587.5', '-1710.2998046875', '5.6999998092651', '0', 0, 0, 0, 255, 255, 255),
(4, 1, 1, 596, '1583.5', '-1710.2998046875', '5.6999998092651', '0', 0, 0, 0, 255, 255, 255),
(5, 1, 1, 596, '1578.5999755859', '-1710.2998046875', '5.6999998092651', '0', 0, 0, 0, 255, 255, 255),
(6, 1, 1, 596, '1574.599609375', '-1710.2998046875', '5.6999998092651', '0', 0, 0, 0, 255, 255, 255),
(7, 1, 1, 596, '1570.3000488281', '-1710.2998046875', '5.6999998092651', '0', 0, 0, 0, 255, 255, 255),
(8, 1, 1, 596, '1566.2998046875', '-1710.2998046875', '5.6999998092651', '0', 0, 0, 0, 255, 255, 255),
(9, 1, 1, 596, '1559', '-1710.2998046875', '5.6999998092651', '0', 0, 0, 0, 255, 255, 255),
(10, 1, 1, 596, '1563', '-1710.2998046875', '5.6999998092651', '0', 0, 0, 0, 255, 255, 255),
(11, 1, 1, 599, '1601.5', '-1700.0999755859', '6.0999999046326', '90', 0, 0, 0, 255, 255, 255),
(12, 1, 1, 599, '1601.5', '-1704.2998046875', '6.0999999046326', '90', 0, 0, 0, 255, 255, 255),
(13, 1, 1, 599, '1601.5', '-1691.9000244141', '6.0999999046326', '90', 0, 0, 0, 255, 255, 255),
(14, 1, 1, 599, '1601.5', '-1695.8994140625', '6.0999999046326', '90', 0, 0, 0, 255, 255, 255),
(15, 1, 1, 599, '1601.5', '-1687.9000244141', '6.0999999046326', '90', 0, 0, 0, 255, 255, 255),
(16, 1, 1, 599, '1601.5', '-1684', '6.0999999046326', '90', 0, 0, 0, 255, 255, 255),
(17, 1, 1, 427, '1538.8000488281', '-1645', '6.0999999046326', '180', 0, 0, 0, 255, 255, 255),
(18, 1, 1, 427, '1534.5999755859', '-1645', '6.0999999046326', '180', 0, 0, 0, 255, 255, 255),
(19, 1, 1, 601, '1526.5999755859', '-1645.5', '5.5999999046326', '180', 0, 0, 0, 255, 255, 255),
(20, 1, 1, 601, '1530.599609375', '-1645.5', '5.5999999046326', '180', 0, 0, 0, 255, 255, 255),
(21, 1, 1, 523, '1528.5999755859', '-1683.4000244141', '5.5999999046326', '270', 0, 0, 0, 255, 255, 255),
(22, 1, 1, 523, '1528.599609375', '-1689.3994140625', '5.5999999046326', '270', 0, 0, 0, 255, 255, 255),
(23, 1, 1, 523, '1528.599609375', '-1687.3994140625', '5.5999999046326', '270', 0, 0, 0, 255, 255, 255),
(24, 1, 1, 523, '1528.599609375', '-1685.3994140625', '5.5999999046326', '270', 0, 0, 0, 255, 255, 255),
(39, 2, 1, 470, '193.5', '1919.40002', '17.8', '180', 0, 0, 0, 0, 0, 0),
(40, 2, 1, 470, '202.5', '1919.40002', '17.8', '180', 0, 0, 0, 0, 0, 0),
(41, 2, 1, 470, '220.5', '1919.40002', '17.8', '180', 0, 0, 0, 0, 0, 0),
(42, 2, 1, 470, '211.5', '1919.40002', '17.8', '180', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `housedata`
--

CREATE TABLE `housedata` (
  `ID` int(11) NOT NULL,
  `Besitzer` text CHARACTER SET utf8 NOT NULL,
  `Preis` int(11) NOT NULL,
  `Ort` text CHARACTER SET utf8 NOT NULL,
  `Garagenplatz` varchar(255) NOT NULL,
  `Garage` int(11) NOT NULL,
  `GarageX` varchar(255) NOT NULL,
  `GarageY` varchar(255) NOT NULL,
  `GarageZ` varchar(255) NOT NULL,
  `Slots` int(11) NOT NULL,
  `hX` text CHARACTER SET utf8 NOT NULL,
  `hY` text CHARACTER SET utf8 NOT NULL,
  `hZ` text CHARACTER SET utf8 NOT NULL,
  `Interior` text NOT NULL,
  `Kasse` text CHARACTER SET utf8 NOT NULL,
  `Miete` text CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `housedata`
--

INSERT INTO `housedata` (`ID`, `Besitzer`, `Preis`, `Ort`, `Garagenplatz`, `Garage`, `GarageX`, `GarageY`, `GarageZ`, `Slots`, `hX`, `hY`, `hZ`, `Interior`, `Kasse`, `Miete`) VALUES
(1, 'Apoollo', 100000, 'Mulholland, Red County', '102|-30.3|19|28', 2, '1519.906250', '-694.433594', '94.750000', 4, '1496.977539', '-687.898438', '95.563309', '5|140.18|1366.58|1084', '0', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `knowledge`
--

CREATE TABLE `knowledge` (
  `Name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pInventar`
--

CREATE TABLE `pInventar` (
  `ID` varchar(255) NOT NULL DEFAULT '',
  `Name` varchar(255) DEFAULT NULL,
  `Drogen` varchar(255) DEFAULT NULL,
  `Mats` varchar(255) DEFAULT NULL,
  `wp_slot1` varchar(255) DEFAULT NULL,
  `wp_slot2` varchar(255) DEFAULT NULL,
  `wp_slot3` varchar(255) DEFAULT NULL,
  `wp_slot4` varchar(255) DEFAULT NULL,
  `wp_slot5` varchar(255) DEFAULT NULL,
  `essen_slot1` int(11) NOT NULL DEFAULT '0',
  `essen_slot2` int(11) NOT NULL DEFAULT '0',
  `essen_slot3` int(11) NOT NULL DEFAULT '0',
  `Kontoauszug` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `pInventar`
--

INSERT INTO `pInventar` (`ID`, `Name`, `Drogen`, `Mats`, `wp_slot1`, `wp_slot2`, `wp_slot3`, `wp_slot4`, `wp_slot5`, `essen_slot1`, `essen_slot2`, `essen_slot3`, `Kontoauszug`) VALUES
('1', 'Apoollo', '5', '100', '0;0', '0;0', '0;0', '0;0', '0;0', 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `players`
--

CREATE TABLE `players` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Passwort` text NOT NULL,
  `Handgeld` int(11) NOT NULL DEFAULT '2500',
  `Bankgeld` int(11) NOT NULL DEFAULT '500',
  `Job` int(11) NOT NULL DEFAULT '0',
  `Faction` int(11) NOT NULL DEFAULT '0',
  `Rang` int(11) NOT NULL DEFAULT '0',
  `Spielzeit` int(11) NOT NULL DEFAULT '0',
  `Social` int(11) NOT NULL DEFAULT '0',
  `spawnX` double NOT NULL DEFAULT '1686',
  `spawnY` double NOT NULL DEFAULT '-2238',
  `spawnZ` double NOT NULL DEFAULT '14',
  `spawnRot` double NOT NULL DEFAULT '180',
  `spawnInt` int(11) NOT NULL DEFAULT '0',
  `spawnDim` int(11) NOT NULL DEFAULT '0',
  `Skin` int(11) NOT NULL DEFAULT '78',
  `Regierung` int(11) NOT NULL DEFAULT '0',
  `President` int(11) NOT NULL DEFAULT '0',
  `Adminlevel` int(11) NOT NULL DEFAULT '0',
  `Warns` int(11) NOT NULL DEFAULT '0',
  `Jailtime` int(11) NOT NULL DEFAULT '0',
  `Wanteds` int(11) NOT NULL DEFAULT '0',
  `Serial` varchar(255) NOT NULL,
  `Regdate` varchar(255) NOT NULL,
  `Unternehmen` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `players`
--

INSERT INTO `players` (`ID`, `Name`, `Passwort`, `Handgeld`, `Bankgeld`, `Job`, `Faction`, `Rang`, `Spielzeit`, `Social`, `spawnX`, `spawnY`, `spawnZ`, `spawnRot`, `spawnInt`, `spawnDim`, `Skin`, `Regierung`, `President`, `Adminlevel`, `Warns`, `Jailtime`, `Wanteds`, `Serial`, `Regdate`, `Unternehmen`) VALUES
(1, 'Apoollo', '8984360418806C50C725E48F95507AC7', 2862, 1500, 0, 1, 7, 711, 5, 1581.555664, -1682.645508, 6.21875, 180, 0, 0, 288, 0, 0, 5, 0, 0, 1, '', '', '0'),
(2, 'ravecow', '488F65F02AFB2CE271FD3E37BD24C05A', 2540, 1500, 0, 1, 7, 2447, 5, 1514.287109, -697.535156, 94.632484, 180, 0, 0, 288, 0, 0, 5, 0, 0, 1, '', '', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pSkin`
--

CREATE TABLE `pSkin` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `SKIN` int(11) NOT NULL,
  `SKINCOLOR` int(11) NOT NULL,
  `SHIRT` int(11) NOT NULL DEFAULT '0',
  `HEAD` int(11) DEFAULT '0',
  `TROUSERS` int(11) NOT NULL DEFAULT '0',
  `SHOES` int(11) NOT NULL DEFAULT '0',
  `HATS` int(11) NOT NULL DEFAULT '0',
  `GLASSES` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `pSkin`
--

INSERT INTO `pSkin` (`ID`, `SKIN`, `SKINCOLOR`, `SHIRT`, `HEAD`, `TROUSERS`, `SHOES`, `HATS`, `GLASSES`) VALUES
(1, 2, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sosacars`
--

CREATE TABLE `sosacars` (
  `ID` int(11) NOT NULL,
  `VehicleID` int(11) NOT NULL,
  `PosX` text CHARACTER SET utf8 NOT NULL,
  `PosY` text CHARACTER SET utf8 NOT NULL,
  `PosZ` text CHARACTER SET utf8 NOT NULL,
  `Rotation` text CHARACTER SET utf8 NOT NULL,
  `R1` int(11) NOT NULL DEFAULT '0',
  `G1` int(11) NOT NULL DEFAULT '0',
  `B1` int(11) NOT NULL DEFAULT '0',
  `R2` int(11) NOT NULL DEFAULT '0',
  `G2` int(11) NOT NULL DEFAULT '0',
  `B2` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `sosacars`
--

INSERT INTO `sosacars` (`ID`, `VehicleID`, `PosX`, `PosY`, `PosZ`, `Rotation`, `R1`, `G1`, `B1`, `R2`, `G2`, `B2`) VALUES
(1, 462, '1678.39941', '-2292.2998', '13.2', '270', 0, 0, 0, 0, 0, 0),
(2, 462, '1678.39941', '-2280.2998', '13.2', '270', 0, 0, 0, 0, 0, 0),
(3, 462, '1678.39941', '-2282.2998', '13.2', '270', 0, 0, 0, 0, 0, 0),
(4, 462, '1678.39941', '-2284.2998', '13.2', '270', 0, 0, 0, 0, 0, 0),
(5, 462, '1678.39941', '-2286.2998', '13.2', '270', 0, 0, 0, 0, 0, 0),
(6, 462, '1678.39941', '-2288.2998', '13.2', '270', 0, 0, 0, 0, 0, 0),
(7, 462, '1678.39941', '-2290.2998', '13.2', '270', 0, 0, 0, 0, 0, 0),
(8, 462, '1687.09998', '-2282.2998', '13.2', '90', 0, 0, 0, 0, 0, 0),
(9, 462, '1687.09998', '-2280.2998', '13.2', '90', 0, 0, 0, 0, 0, 0),
(10, 462, '1687.09998', '-2284.2998', '13.2', '90', 0, 0, 0, 0, 0, 0),
(11, 462, '1687.09998', '-2286.2998', '13.2', '90', 0, 0, 0, 0, 0, 0),
(12, 462, '1687.09998', '-2292.2998', '13.2', '90', 0, 0, 0, 0, 0, 0),
(13, 462, '1687.09998', '-2288.2998', '13.2', '90', 0, 0, 0, 0, 0, 0),
(14, 462, '1687.09998', '-2290.2998', '13.2', '90', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `unternehmen`
--

CREATE TABLE `unternehmen` (
  `ID` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Kasse` varchar(255) NOT NULL,
  `Mitglieder` varchar(255) NOT NULL,
  `Leader` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vehicles`
--

CREATE TABLE `vehicles` (
  `ID` int(11) NOT NULL,
  `Owner` text CHARACTER SET utf8 NOT NULL,
  `vehicle_key_2` text NOT NULL,
  `vehicle_key_3` text NOT NULL,
  `VehicleID` int(11) NOT NULL,
  `Slot` int(11) NOT NULL,
  `Age` int(11) NOT NULL,
  `KMStand` int(11) NOT NULL,
  `Fuel` int(11) NOT NULL,
  `PosX` text CHARACTER SET utf8 NOT NULL,
  `PosY` text CHARACTER SET utf8 NOT NULL,
  `PosZ` text CHARACTER SET utf8 NOT NULL,
  `Rotation` text CHARACTER SET utf8 NOT NULL,
  `r1` int(11) NOT NULL DEFAULT '0',
  `g1` int(11) NOT NULL DEFAULT '0',
  `b1` int(11) NOT NULL DEFAULT '0',
  `r2` int(11) NOT NULL DEFAULT '0',
  `g2` int(11) NOT NULL DEFAULT '0',
  `b2` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `vehicles`
--

INSERT INTO `vehicles` (`ID`, `Owner`, `vehicle_key_2`, `vehicle_key_3`, `VehicleID`, `Slot`, `Age`, `KMStand`, `Fuel`, `PosX`, `PosY`, `PosZ`, `Rotation`, `r1`, `g1`, `b1`, `r2`, `g2`, `b2`) VALUES
(1, 'Apoollo', '', '', 541, 1, 0, 0, 50, '1532.826172', '-1737.135742', '13.085882', '93.235474', 13, 8, 0, 0, 0, 0),
(2, 'Apoollo', '', '', 545, 2, 0, 0, 50, '1673.475586', '-2258.880859', '13.257041', '268.538818', 44, 96, 0, 0, 0, 0),
(3, 'Apoollo', '', '', 421, 3, 0, 0, 50, '1502.745117', '-1740.012695', '13.429358', '60.040283', 34, 25, 24, 245, 245, 245),
(4, 'Apoollo', '0', '0', 519, 4, 0, 0, 50, '1937.621094', '-2252.239258', '14.468425', '358.209229', 245, 245, 245, 245, 245, 245);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `banned`
--
ALTER TABLE `banned`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `factiondepot`
--
ALTER TABLE `factiondepot`
  ADD PRIMARY KEY (`fID`);

--
-- Indizes für die Tabelle `factionvehicles`
--
ALTER TABLE `factionvehicles`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `housedata`
--
ALTER TABLE `housedata`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `pInventar`
--
ALTER TABLE `pInventar`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `pSkin`
--
ALTER TABLE `pSkin`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `sosacars`
--
ALTER TABLE `sosacars`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `unternehmen`
--
ALTER TABLE `unternehmen`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `factionvehicles`
--
ALTER TABLE `factionvehicles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT für Tabelle `housedata`
--
ALTER TABLE `housedata`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `players`
--
ALTER TABLE `players`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT für Tabelle `sosacars`
--
ALTER TABLE `sosacars`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT für Tabelle `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;