--[[--
	Copyright: Shyim
--]]--

local screenW, screenH = guiGetScreenSize()
local show = false
local lp = getLocalPlayer()
local streamData ={}

function renderDev()
	local info = ""
	local dxInfo = dxGetStatus()
	local networkInfo = getNetworkStats()
	if isPedInVehicle(getLocalPlayer()) then 
		local pos = {getElementPosition(getPedOccupiedVehicle(getLocalPlayer()))}
		local rot = {getElementRotation(getPedOccupiedVehicle(getLocalPlayer()))}
		local rot2 = {getElementRotation((getLocalPlayer()))}
		local model = getElementModel(getPedOccupiedVehicle(getLocalPlayer()))
		info = info.."Position (Vehicle : "..model.." ) : "..tostring(math.floor(pos[1]))..","..tostring(math.floor(pos[2]))..","..tostring(math.floor(pos[3])).." Tippe - zum Kopieren\n"
		info = info.."Rotation: (Vehicle : "..math.floor(rot[3]).." ) "..math.floor(rot2[3]).."\n"
	else
		local pos = {getElementPosition(getLocalPlayer())}
		local rot = {getElementRotation(getLocalPlayer())}
		info = info.."Position : "..tostring(math.floor(pos[1]))..","..tostring(math.floor(pos[2]))..","..tostring(math.floor(pos[3])).." Tippe - zum Kopieren\n"
		info = info.."Rotation : "..tostring(math.floor(rot[3])).."\n"
	end
	info = info.."Skin : "..getElementModel(getLocalPlayer()).."\n"
	info = info.."Freier Arbeitsspeicher fuer MTA : "..dxInfo["VideoMemoryFreeForMTA"].."MB\n"
	info = info.."Streamed In Elements\n"
	info = info.."Streamed Markers : "..tostring(streamData["marker"] or 0).."\n"
	info = info.."Streamed Server.players : "..tostring(streamData["player"] or 0).."\n"
	info = info.."Streamed Vehicle : "..tostring(streamData["vehicle"] or 0).."\n"
	info = info.."Streamed Object : "..tostring(streamData["object"] or 0).."\n"
	info = info.."Streamed Ped : "..tostring(streamData["ped"] or 0).."\n"
	info = info.."Streamed Sound : "..tostring(streamData["sound"] or 0).."\n"
	info = info.."Network Stats\n"
	info = info.."Packet Lost : "..networkInfo["packetlossTotal"].."\n"
	info = info.."Packet Lost Last Second : "..networkInfo["packetlossLastSecond"].."\n"
	dxDrawRectangle(screenW - 311 - 10, (screenH - 388) / 2, 311, 388, tocolor(0, 0, 0, 180), true)
	dxDrawText("Develop Info", screenW - 287 - 20, ((screenH - 22) / 2) - 170, (screenW - 287 - 20) + 287, ( ((screenH - 22) / 2) - 170) + 22, tocolor(255, 255, 255, 200), 1.00, "default-bold", "center", "top", false, false, true, false, false)
	dxDrawText(info, screenW - 284 - 25, ((screenH - 344) / 2) + 20, (screenW - 284 - 25) + 284, ( ((screenH - 344) / 2) + 20) + 344, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, false, true, false, false)
end

function getStreamedElementIn()
	if (streamData[getElementType(source)] ) then
		streamData[getElementType(source)] = streamData[getElementType(source)] + 1
	else
		streamData[getElementType(source)]  = 1
	end
end

function getStreamedElementOut()
	if (streamData[getElementType(source)] ) then
		if (streamData[getElementType(source)] < 0) then 
			streamData[getElementType(source)]  = 0
		else
			streamData[getElementType(source)] = streamData[getElementType(source)] - 1
		end
	else
		streamData[getElementType(source)]  = 0
	end
end

addEventHandler("onClientElementStreamIn",getRootElement(),getStreamedElementIn)
addEventHandler("onClientElementStreamOut",getRootElement(),getStreamedElementOut)

addCommandHandler("devtool",function ( )
	if show then
		removeEventHandler("onClientRender",getRootElement(),renderDev)
		show = false
	else
		addEventHandler("onClientRender",getRootElement(),renderDev)
		show = true
	end
end)


bindKey("-","down",function ()
	if show then
		local copy
		if isPedInVehicle(getLocalPlayer()) then 
			local pos = {getElementPosition(getPedOccupiedVehicle(getLocalPlayer()))}
			local rot = {getElementRotation(getPedOccupiedVehicle(getLocalPlayer()))}
			local rot2 = {getElementRotation((getLocalPlayer()))}
			local model = getElementModel(getPedOccupiedVehicle(getLocalPlayer()))
			copy = tostring(math.floor(pos[1]))..","..tostring(math.floor(pos[2]))..","..tostring(math.floor(pos[3])).."\n"
		else
			local pos = {getElementPosition(getLocalPlayer())}
			copy = tostring(math.floor(pos[1]))..","..tostring(math.floor(pos[2]))..","..tostring(math.floor(pos[3])).."\n"
		end
		setClipboard(copy)
		outputChatBox("Die Koordinaten wurden erfolgreich kopiert!",0,125,0)
	end
end)



--[[local sx = guiGetScreenSize()
local function drawFPS()
    if not getCurrentFPS() then
        return
    end
    local roundedFPS = math.floor(getCurrentFPS())
	
    dxDrawText(roundedFPS, sx-10 - dxGetTextWidth(roundedFPS, 3)-1, 0, 0, 0, tocolor(0, 0, 0), 3)
    dxDrawText(roundedFPS, sx-10 - dxGetTextWidth(roundedFPS, 3)+1, 0, 0, 0, tocolor(0, 0, 0), 3)
    dxDrawText(roundedFPS, sx-10 - dxGetTextWidth(roundedFPS, 3), 1, 0, 0, tocolor(0, 0, 0), 3)
    dxDrawText(roundedFPS, sx-10 - dxGetTextWidth(roundedFPS, 3), 3, 0, 0, tocolor(0, 0, 0), 3)

    dxDrawText(roundedFPS, sx-10 - dxGetTextWidth(roundedFPS, 3), 2, 0, 0, tocolor(255, 255, 0), 3)
end
addEventHandler("onClientHUDRender", root, drawFPS)]]--