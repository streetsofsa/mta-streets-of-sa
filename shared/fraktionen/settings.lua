﻿Fraktionen = {}

Fraktionen.settings = {}

Fraktionen.settings.name = {
	[0] = "-/-",
	[1] = "SAPD",
	[2] = "SA Army",
	[3] = "FBI",
	[4] = "SAN News",
	[5] = "Medic",
	[6] = "Grove Street",
	[7] = "Mafia",
	[8] = "SAAP",
	[9] = "Yakuza",
	[10] = "Hitman",
	[11] = "DoD"
}

Fraktionen.settings.colors = {
	[0] = {255,255,255},
	[1] = {0,155,255},
	[2] = {100,70,0},
	[3] = {0,80,100},
	[4] = {200,145,25},
	[5] = {255,0,0},
	[6] = {0,255,0},
	[7] = {20,20,20},
	[8] = {170,255,0},
	[9] = {130,130,130},
	[10] = {200,140,0},
	[11] = {10,20,10},
}

Fraktionen.settings.ranks = {
	[1] = {
		[1] = "Rekrut",
		[2] = "Officer",
		[3] = "Sergeant",
		[4] = "Captain",
		[5] = "Inspector",
		[6] = "Superintendent",
		[7] = "Chief"
	},
	[2] = {
		[1] = "Soldat",
		[2] = "Sergeant",
		[3] = "Captain",
		[4] = "Hauptmann",
		[5] = "Major",
		[6] = "Oberlieutenant",
		[7] = "General"
	},
	[3] = {
		[1] = "Agent",
		[2] = "Probationary Agent",
		[3] = "Special Agent",
		[4] = "Senior Special Agent",
		[5] = "Supervisory Agent",
		[6] = "Stv. Direktor",
		[7] = "Direktor"
	},
	[4] = {
		[1] = "Praktikant",
		[2] = "Anzeiger",
		[3] = "Reporter",
		[4] = "Chefreporter",
		[5] = "Journalist",
		[6] = "Redakteur",
		[7] = "Chefredakteur"
	},
	[5] = {
		[1] = "Sanitaetsassistent",
		[2] = "Sanitaeter",
		[3] = "Arzt",
		[4] = "Notarzt",
		[5] = "Facharzt",
		[6] = "Oberarzt",
		[7] = "Chefarzt"
	},
	[6] = {
		[1] = "Newcomer",
		[2] = "Homie",
		[3] = "Bro",
		[4] = "Gangster",
		[5] = "Legend",
		[6] = "Underboss",
		[7] = "Big Boss"
	},
	[7] = {
		[1] = "",
		[2] = "",
		[3] = "",
		[4] = "",
		[5] = "",
		[6] = "",
		[7] = ""
	},
	[8] = {
		[1] = "Praktikant",
		[2] = "Lehrling",
		[3] = "Abschlepper",
		[4] = "Abschleppmeister",
		[5] = "Abteilungsleiter",
		[6] = "Meister",
		[7] = "Chef"
	},
	[9] = {
		[1] = "Shatei",
		[2] = "Kyodai",
		[3] = "Taisho",
		[4] = "Kensai",
		[5] = "Kaikei",
		[6] = "Shugo Tenshi",
		[7] = "Oyabun"
	},
	[10] = {
		[1] = "Praktikant",
		[2] = "Schuetze",
		[3] = "Auftragskiller",
		[4] = "Assasin",
		[5] = "Abteilungsleiter",
		[6] = "Serienkiller",
		[7] = "Spezialist"
	},
	[11] = {
		[1] = "Rekrut",
		[2] = "Analyst",
		[3] = "Teamleiter",
		[4] = "Abteilungsleiter",
		[5] = "Offizier",
		[6] = "Berater",
		[7] = "Verteidigungsminister"
	}	
}

Fraktionen.settings.allowed = {
[0] = {0},
[1] = {1,2,3},
[2] = {1,2,3},
[3] = {1,2,3},
[4] = {4},
[5] = {5},
[6] = {6},
[7] = {7},
[8] = {8},
[9] = {9},
[10] = {10},
[11] = {11,1,3}
}

