
hInteriors = {
	 
	[1] = "1|223.04|1287.26|1083.2", -- Schäbige Hütte
	[2] = "5|2233.8|-1115.36|1051", -- Hotel
	[3] = "8|2365.3|-1134.92|1051", --  Normale Mittelklasse Wohnung
	[4] = "11|2282.91|-1140.29|1051", -- Hotel 2
	[5] = "6|2196.79|-1204.35|1050", -- Große Mittelklasse Wohnung
	[6] = "10|2270.39|-1210.45| 1048", -- Kleine Mittelklasse Wohnung
	[7] = "6|2308.79|-1212.88|1050", -- Kleine schäbige Hütte
	[8] = "1|2217.54|-1076.29|1051", -- Hotelzimmer für 2
	[9] = "2|2237.59|-1080.87|1050", -- Mittelklasse Wohnung, klein
	[10] = "9|2317.82|-1026.75|1051", -- normal bis große Oberklasse Wohnung, 2Stöckig
	[11] = "4|260.98|1284.55|1081", -- kleine wohnung, schmaler flur
	[12] = "5|140.18|1366.58|1084", -- Villa 
	[13] = "9|82.95|1322.44|1084", -- normal bis große Mittelklasse Wohnung, 2Stöckig
	[14] = "15|-283.55|1470.98|1085", --normale mittelklasse wohnung, 2Stöckig, hell
	[15] = "4|-260.6|1456.62|1085", -- normale mittelklasse wohnung, 2Stöckig
	[15] = "8|-42.58|1405.61|1085", -- kleine mittelklasse wohnung
	[16] = "6|-68.69|1351.97|1081", -- kleine, schäbige hütte
	[17] = "6|2333.11|-1077.1|1050", -- kleine, schäbige hütte
	[18] = "3|2496.05|-1692.73|1015", -- normal große mittelklasse wohnung, 2Stöckig, CJ
	[19] = "8|2807.62|-1174.1|1025", -- normal große mittelklasse wohnung, 2Stöckig
	[20] = "12|447.52|511.49|1002.5", -- kleine schäbige-mittelklasse wohnung
	[21] = "3|1527.38|-11.02|1003", -- sehr kleine schäbige wohnung
	[22] = "10|422.16|2536.52|10",	-- kleine, sehr schäbige wohnung
	
	
	
	
	[1000] = "INT|X|Y|Z", -- Garage 0 Slots
	[1001] = "101|-30.3|21.5|28", -- Garage 2 Slots
	[1002] = "102|-30.3|19|28", -- Garage 4 Slots
	[1003] = "INT|X|Y|Z", -- Garage 6 Slots
	[1004] = "INT|X|Y|Z", -- Garage 8 Slots
	[1005] = "INT|X|Y|Z", -- Garage 10 Slots
	[1006] = "INT|X|Y|Z", -- Garage 10 Slots
	[1007] = "INT|X|Y|Z", -- Garage 7 Slots
	[1008] = "INT|X|Y|Z" -- Garage 8 Slots
}


--||Garagen erstellen||--
hGaragen = {
------------------------------------------------------------------	
	interior = {
		--||2 Slots||--
		[1] = {
			{14783,-24.2000000,24.1200000,28.8500000,0.0000000,0.0000000,0.0000000}, --object(int3int_kbsgarage) (1)
			{14783,-24.1000000,19.0000000,28.9500000,0.0000000,0.0000000,0.0000000}, --object(int3int_kbsgarage) (2)
			{2885,-31.4000000,21.4000000,31.0000000,0.0000000,0.0000000,90.0000000}, --object(xref_garagedoor) (1)
			{9321,-17.6100000,21.4000000,31.0000000,0.0000000,281.0000000,0.0000000}, --object(garage_sfn01) (1)
			{9321,-17.5996100,21.4003900,28.4000000,0.0000000,280.9970000,0.0000000}, --object(garage_sfn01) (2)
		},
		--||4 Slots||--
		[2] = {
			{14783,-24.1000000,19.0000000,28.9500000,0.0000000,0.0000000,0.0000000}, --object(int3int_kbsgarage) (2)
			{9321,-17.5100000,21.4000000,31.0000000,0.0000000,281.0000000,0.0000000}, --object(garage_sfn01) (1)
			{9321,-17.5000000,21.4003900,28.4000000,0.0000000,280.9970000,0.0000000}, --object(garage_sfn01) (2)
			{9321,-17.5100000,16.0000000,31.0000000,0.0000000,280.9970000,0.0000000}, --object(garage_sfn01) (3)
			{9321,-17.5000000,16.0000000,28.4000000,0.0000000,280.9970000,0.0000000}, --object(garage_sfn01) (4)
		}
	},
------------------------------------------------------------------	
	points = {
		--|| Spawnpoints 2Slot-Garage||--	
		[1] = {
		
		},
		--|| Spawnpoints 4Slot-Garage||--
		[2] = {
		
		}
	}
------------------------------------------------------------------		
}
--||||--































