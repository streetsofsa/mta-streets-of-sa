Downloader = {}
Downloader.__index = Downloader

function Downloader:TransferFiles(resource)
	table.foreach(getElementsByType("player"), function(_, v)
		local data = {}
		data.tFiles = {}
		data.tSize = 0
		data.tPlayer = v
		data.tResource = resource
		data.meta = xmlLoadFile(":"..getResourceName(data.tResource).."/meta.xml")
		if data.meta then
			table.foreach(xmlNodeGetChildren(data.meta), function(i, v)
				local child = {}
				child.name = xmlNodeGetName(v)
				child.path = xmlNodeGetAttribute(v, "src")
				child.type = xmlNodeGetAttribute(v, "type")
				local file = fileOpen(":"..getResourceName(data.tResource).."/"..child.path)
				if file then
					data.tSize = data.tSize + fileGetSize(file)
				end
			end)
		end
	end)
end


addEventHandler("onResourcePreStart", root, function(...)
	Downloader:TransferFiles(...)
end)	