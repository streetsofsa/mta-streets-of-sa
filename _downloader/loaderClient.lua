-- Client
local ClientDownloadFiles = {};
local screenwidth, screenheight = guiGetScreenSize();

function tempDrawTransfers()
	if(ClientDownloadFiles.transferFileName)then
		if not(transferFile)then
			if(fileExists(':'..ClientDownloadFiles.transferFileOrder..'/'..ClientDownloadFiles.transferFileName))then
				local transferFile = fileOpen(':'..ClientDownloadFiles.transferFileOrder..'/'..ClientDownloadFiles.transferFileName);
				if(transferFile)then
					ClientDownloadFiles.transferCompletedSize = ClientDownloadFiles.transferCompletedSize + fileGetSize(transferFile);
					if(ClientDownloadFiles.transferCompletedSize >= ClientDownloadFiles.transferFileSize)then
						ClientDownloadFiles.transferCompletedSize = 0;
						fileClose(transferFile);
						transferFile = false
					end
				end
			end
		end
		dxDrawRectangle(1349*screenwidth/1680, 894*screenheight/1050, 247*screenwidth/1680, 57*screenheight/1050, tocolor(6, 6, 6, 230), false)
		dxDrawText("Addantion File Downloading ("..((ClientDownloadFiles.transferCompletedSize/ClientDownloadFiles.transferFileSize)*10).."%)\nFile: "..(ClientDownloadFiles.transferFileName or 'none').." ( "..sizeFormat(ClientDownloadFiles.transferCompletedSize).." / "..sizeFormat(ClientDownloadFiles.transferFileSize or 0)..")", 1349*screenwidth/1680, 893*screenheight/1050, 1595*screenwidth/1680, 932*screenheight/1050, tocolor(255, 255, 255, 255), 0.80, "default-bold", "center", "center", false, false, true, false, false)
		dxDrawRectangle(1383*screenwidth/1680, 932*screenheight/1050, (172-(ClientDownloadFiles.transferFileSize/ClientDownloadFiles.transferCompletedSize))*screenwidth/1680, 7*screenwidth/1050, tocolor(255, 255, 255, 255), true)
	end
end
addEventHandler('onClientPreRender', root, tempDrawTransfers)

addEvent('Client:BeginnTransfer', true)
addEventHandler('Client:BeginnTransfer', localPlayer, function(order, file, data, size)
	ClientDownloadFiles.transferCompletedSize = 0;
	ClientDownloadFiles.transferFileName 	  = data;
	ClientDownloadFiles.transferFileSize 	  = size;
	ClientDownloadFiles.transferFileOrder 	  = order;
	outputConsole("[DATA] `"..data.."("..sizeFormat(size)..")` Has been Downloaded!");
end)

-------------------------
-- By DKong (sizeForm) --
-------------------------
function sizeFormat(size)
	local size = tostring(size)
	if size:len() >= 4 then		
		if size:len() >= 7 then
			if size:len() >= 9 then
				local returning = size:sub(1, size:len()-9)
				if returning:len() <= 1 then
					returning = returning.."."..size:sub(2, size:len()-7)
				end
				return returning.." GB";
			else				
				local returning = size:sub(1, size:len()-6)
				if returning:len() <= 1 then
					returning = returning.."."..size:sub(2, size:len()-4)
				end
				return returning.." MB";
			end
		else		
			local returning = size:sub(1, size:len()-3)
			if returning:len() <= 1 then
				returning = returning.."."..size:sub(2, size:len()-1)
			end
			return returning.." KB";
		end
	else
		return size.." Bytes";
	end
end