-- Server
C_Downloader = {}
C_Downloader.__index = C_Downloader;

function C_Downloader:Transfer(resource)
	for _, player in pairs(getElementsByType("player")) do 
		local self = {}
		setmetatable(self, {__index = C_Downloader});
		self.transferFiles 		= 	{};
		self.transferFilesSize	= 	0; -- 0 Bytes
		self.transferPlayer 	= 	player;
		self.transferResource	= 	resource;
		local transferXml 		= 	xmlLoadFile(':'..getResourceName(self.transferResource)..'/meta.xml');
		if(transferXml)then
			for i, row in ipairs(xmlNodeGetChildren(transferXml)) do 
			local name	 = xmlNodeGetName(row);
			local script = xmlNodeGetAttribute(row, 'src');
			local typ 	 = xmlNodeGetAttribute(row, 'type');
				if(script)then
					file = fileOpen(':'..getResourceName(self.transferResource)..'/'..script);
					if(file)then
						self.transferFilesSize = self.transferFilesSize + fileGetSize(file);
						triggerLatentClientEvent(self.transferPlayer, 'Client:BeginnTransfer', 50000, false, self.transferPlayer, getResourceName(self.transferResource), file, script, fileGetSize(file));
						if(not(isTimer(self.transferTimer)))then
							self.transferHandler 	= 	getLatentEventHandles(self.transferPlayer)[#getLatentEventHandles(self.transferPlayer)];
							self.transferTimer 		= 	setTimer(function(...) self:UpdateTransfer(...) end, 50, 0);
						end
						fileClose(file);
					end
				end
			end
			xmlUnloadFile(transferXml);
		end
	end
end
addEventHandler('onResourcePreStart', root, function(...) C_Downloader:Transfer(...) end);


function C_Downloader:UpdateTransfer()
	self.transferHandler = getLatentEventHandles(self.transferPlayer)[#getLatentEventHandles(self.transferPlayer)];
	self.transferStatus  = getLatentEventStatus(self.transferPlayer, self.transferHandler)   -- Get latent event status
	if not(self.transferStatus)then
		killTimer(self.transferTimer);
	end
end

