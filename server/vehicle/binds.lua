vehFunctions = {}

function vehFunctions.bindKeys(player, key, state)
	if key == "x" then
		if Server.players[player] then
		if getPedOccupiedVehicle(player) and getPedOccupiedVehicleSeat(player) == 0 then
			local veh = getPedOccupiedVehicle(player)
			--
			if Server.vehicles[veh] then
			if Server.vehicles[veh]:getData("Faction") ~= false then
			--||Fraktion Autos||--
				local allowed = false
				for _, v in ipairs(Fraktionen.settings.allowed[tonumber(Server.vehicles[veh]:getData("Faction"))]) do
					if v == tonumber(Server.players[player]:getData("Faction")) then
						allowed = true
						break
					end
				end
				if Server.vehicles[veh]:getData("Faction") == 0 then
				--||Noob Autos||--
					Server.vehicles[veh]:setEngineState(not Server.vehicles[veh]:getEngineState())
				else
				--||Fraktion Autos||--
					if allowed == true then
						Server.vehicles[veh]:setEngineState(not Server.vehicles[veh]:getEngineState())
					else
						infobox(player, "Du besitzt keinen Schlüssel für dieses Fahrzeug!")
					end
				end
			else
			--||Privat Autos||--
				outputChatBox("PRIVAT")
				outputChatBox("1: "..tostring(Server.players[player].inv:getData("VehicleKey:"..getVehicleNameFromModel(Server.vehicles[veh]:getData("Model")))))
				outputChatBox("2: "..tostring(Server.vehicles[veh].c_vehicle))
				if Server.players[player].inv:getData("VehicleKey:"..getVehicleNameFromModel(Server.vehicles[veh]:getData("Model"))) == Server.vehicles[veh].c_vehicle then
					Server.vehicles[veh]:setEngineState(not Server.vehicles[veh]:getEngineState())
				else	
					infobox(player, "Du besitzt keinen Schlüssel für dieses Fahrzeug!")
				end
			end
			end
			--
		end
		end
	elseif key == "l" then
		if getPedOccupiedVehicle(player) and getPedOccupiedVehicleSeat(player) == 0 then
			local veh = getPedOccupiedVehicle(player)
			if Server.vehicles[veh] then
				
				Server.vehicles[veh]:setLightState(not Server.vehicles[veh]:getLightState())
			elseif Server.jobVehicles[veh] then
				outputChatBox(tostring(not Server.jobVehicles[veh]:getLightState()))
				Server.jobVehicles[veh]:setLightState(not Server.jobVehicles[veh]:getLightState())
			end
		end
	end
end
--[[
addEventHandler("onPlayerJoin", getRootElement(), function()
	bindKey(source, "x", "down", vehFunctions.bindKeys)
	bindKey(source, "l", "down", vehFunctions.bindKeys)
end)

table.foreach(getElementsByType("player"), function(i, v)
	bindKey(v, "x", "down", vehFunctions.bindKeys)
	bindKey(v, "l", "down", vehFunctions.bindKeys)
end)]]--