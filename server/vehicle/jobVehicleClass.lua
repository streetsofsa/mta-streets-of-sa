jobVehicles = {}

jobVehicleClass = {}

function jobVehicleClass:new(...)
	local obj = setmetatable ( {}, {__index = self} )
	if obj.constructor then
		obj:constructor (...)
	end
	return obj
end

function jobVehicleClass:setEngineState(state)
	setVehicleEngineState(self.c_vehicle, state)
	self:setData("Motor", state)
end

function jobVehicleClass:getEngineState()
	return getVehicleEngineState(self.c_vehicle)
end

function jobVehicleClass:setLightState(state)
	if state then
		setVehicleOverrideLights(self.c_vehicle, 2)
	else
		setVehicleOverrideLights(self.c_vehicle, 1)
	end
	self:setData("Light", state)
end

function jobVehicleClass:getLightState()
	return self:getData("Light")
end

function jobVehicleClass:getVehicle()
	return self.c_vehicle
end

function jobVehicleClass:setData(index, value)
	if setElementData(self.c_vehicle, index, value) then
		return true
	else
		return false
	end
end

function jobVehicleClass:getData(index)
	return getElementData(self.c_vehicle, index)
end

function jobVehicleClass:constructor(veh, job)
	self.c_vehicle = veh
	setVehicleEngineState(self.c_vehicle, false)
	
	self:setData("Job", job)
	self:setData("Light", false)
	self:setData("Motor", false)
end