﻿vehicleClass.vehCMD = {}


function vehicleClass.vehCMD.getVehicles(player)
	local pName = getPlayerName(player)
	local playerVehicles = {}
	table.foreach(Server.vehicles, function(i, v)
			if Server.vehicles[i]:getOwner() == pName then
				playerVehicles[tonumber(Server.vehicles[i]:getData("Slot"))] = Server.vehicles[i]
				--table.insert(playerVehicles, Server.vehicles[i])
			end
	end)
	return playerVehicles
end


addCommandHandler("ctest", function(source)
	table.foreach(vehicleClass.vehCMD.getVehicles(source), function(i, v)
		outputChatBox(i..":".."")
	end)
	outputChatBox("table.getn: "..table.getn(vehicleClass.vehCMD.getVehicles(source)))
end)

function vehicleClass.vehCMD.park(player)
	if getPedOccupiedVehicle(player) and getPedOccupiedVehicleSeat(player) == 0 then
		local veh = getPedOccupiedVehicle(player)
		if Server.vehicles[veh] then
			if getPlayerName(player) == Server.vehicles[veh]:getOwner() then
				local x, y, z = getElementPosition(veh)
				local rX, rY, rZ = getElementRotation(veh)
				Server.vehicles[veh]:setData("vehX", x)
				Server.vehicles[veh]:setData("vehY", y)
				Server.vehicles[veh]:setData("vehZ", z)
				Server.vehicles[veh]:setData("vehRZ", rZ)
				infobox(player, "Das Fahrzeug wurde erfolgreich geparkt")
			else
				infobox(player, "Du besitzt keinen Schluessel fuer dieses Fahrzeug")
			end
		end
	else
		infobox(player, "Du musst in einem Fahrzeug sitzen")
	end
end
addCommandHandler("park", vehicleClass.vehCMD.park)

function vehicleClass.vehCMD.respawn(player, cmd, slot)
	local vehList = vehicleClass.vehCMD.getVehicles(player)
	if slot and vehList[tonumber(slot)] then
		vehList[tonumber(slot)]:respawnTheVehicle()
		vehList[tonumber(slot)]:setEngineState(false)
	end
end
addCommandHandler("towveh", vehicleClass.vehCMD.respawn)
--and Server.players[player]:getData("loggedIn")