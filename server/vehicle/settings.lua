vehicles = {}

vehicles.settings = {}

vehicles.settings.amount = {
	--||2-Door and Compact Cars||--
	[602] = 1000, --Alpha
	[496] = 1000, -- Blista Compact
	[401] = 1, --Bravura
	[518] = 1, --Buccaneer
	[527] = 1, --Cadrona
	[589] = 1, --Club
	[419] = 1, --Esperanto
	[533] = 1, --Feltzer
	[526] = 1, --Fortune
	[474] = 1, --Hermes
	[545] = 1, --Hustler
	[517] = 1, --Majestic
	[410] = 1, --Manana
	[600] = 1, --Picador
	[436] = 1, --Previon
	[580] = 1, --Stafford
	[439] = 1, --Stallion
	[549] = 1, --Tampa
	[491] = 1, --Virgo
	--||4-Door and Luxury Cars||--
	[445] = 1, --Admiral
	[507] = 1, --Elegant
	[585] = 1, --Emperor
	[587] = 1, --Euros
	[466] = 1, --Glendale
	[492] = 1, --Greenwood
	[546] = 1, --Intruder
	[551] = 1, --Merit
	[516] = 1, --Nebula
	[467] = 1, --Oceanic
	[426] = 1, --Premier
	[547] = 1, --Primo
	[405] = 1, --Sentinel
	[409] = 1, --Strech
	[550] = 1, --Sunrise
	[566] = 1, --Tahoma
	[540] = 1, --Vincent
	[421] = 1, --Washington
	[529] = 1, --Willard
	--||Muscle Cars||--
	[402] = 1, --Buffalo
	[542] = 1, --Clover
	[603] = 1, --Phoenix
	[475] = 1, --Sabre
	--||Sport Cars||--
	[429] = 1, --Banshee
	[541] = 1, --Bullet
	[415] = 1, --Cheetah
	[480] = 1, --Comet
	[562] = 1, --Elegy
	[565] = 1, --Flash
	[434] = 1, --Hotknife
	[494] = 1, --Hotring Racer
	[502] = 1, --Hotring Racer 2
	[503] = 1, --Hotring Racer 3
	[411] = 1, --Infernus
	[559] = 1, --Jester
	[561] = 1, --Stratum
	[560] = 1, --Sultan
	[506] = 1, --Super GT
	[451] = 1, --Turismo
	[558] = 1, --Uranus
	[555] = 1, --Windsor
	[477] = 1, --ZR350
	--||Lowrider Cars||--
	[536] = 1, --Blade
	[575] = 1, --Broadway
	[534] = 1, --Remington
	[567] = 1, --Savanna
	[535] = 1, --Slamvan
	[576] = 1, --Tornado
	[412] = 1, --Voodoo
	--||SUV and Wagons||--
	[579] = 1, --Huntley
	[400] = 1, --Landstalker
	[404] = 1, --Perennial
	[489] = 1, --Rancher
}




vehicles.settings.verbrauch = {
[405]= 1.3,
[466]= 1.2,
[410]= 1.2,
[536]= 1.2,
[475]= 1.5,
[474]= 1.4,
[478]= 1.4,
[479]= 1.3,
[491]= 1.2,
[496]= 1.0,
[535]= 1.0,
[542]= 1.1,
[422]= 1.2,
[549]= 1.2,
[434]= 1.2,
[439]= 1.3,
[534]= 1.4,
[567]= 1.4,
[489]= 1.4,
[547]= 1.2,
[551]= 1.2,
[554]= 1.6,
[561]= 1.3,
[558]= 1.3,
[579]= 1.8,
[589]= 1.2,
[400]= 1.8,
[402]= 1.6,
[411]= 2.8,
[415]= 1.8,
[426]= 1.2,
[429]= 1.7,
[477]= 1.8,
[480]= 1.6,
[506]= 2.8,
[541]= 2.6,
[545]= 1.7,
[559]= 1.8,
[560]= 2.1,
[562]= 1.8,
[522]= 2.8,
[444]= 2.8,
[581]= 1.2,
[462]= 0.6,
[463]= 1.4,
[468]= 1.5,
[586]= 1.3,
[454]= 1.2,
[452]= 2.7,
[473]= 0.8,
[446]= 1.3,
[487]= 2.0,
[511]= 2.3,
[593]= 2.1,
[519]= 2.8
}