vehiclesTable = {}

function vehiclesTable.getSlots(owner)
	local slots = 0
	table.foreach(Server.vehicles, function(i, v)
		if Server.vehicles[i]:getData("Owner") == owner then
			slots = slots + 1
		end
	end)
	return slots
end

function vehiclesTable.getVehicleFromKey(key)
	table.foreach(Server.vehicles, function(i, v)
		if (v:getData("ID")) == key then
			return v.c_vehicle
		end
	end)
	return nil
end

function vehiclesTable.loadAll()
	local sql = mysql:poll("SELECT * FROM `vehicles`", -1)
	table.foreach(sql, function(i, v)
		local newveh = createVehicle(tonumber(v["VehicleID"]), tonumber(v["PosX"]), tonumber(v["PosY"]), tonumber(v["PosZ"]), 0, 0, tonumber(v["Rotation"]), v["Owner"])
		if isElement(newveh) then
		Server.vehicles[newveh] = vehicleClass:new(newveh, v["Owner"], v["Slot"])
		Server.vehicles[newveh]:setData("key:1", v["Owner"])
		Server.vehicles[newveh]:setData("key:2", v["vehicle_key_2"])
		Server.vehicles[newveh]:setData("key:3", v["vehicle_key_3"])
		Server.vehicles[newveh]:setData("ID", v["ID"])
		Server.vehicles[newveh]:setData("Fuel", tonumber(v["Fuel"]))
		Server.vehicles[newveh]:setData("Owner", v["Owner"])
		Server.vehicles[newveh]:setData("Slot", v["Slot"])
		Server.vehicles[newveh]:setData("Age", tonumber(v["Age"]))
		Server.vehicles[newveh]:setColor(tonumber(v["r1"]), tonumber(v["g1"]), tonumber(v["b1"]), tonumber(v["r2"]), tonumber(v["g2"]), tonumber(v["b2"]))
		else
			outputServerLog("Kein Element!")
		end
	end)
end
addEventHandler("onResourceStart", getResourceRootElement(), vehiclesTable.loadAll)

function vehiclesTable.createVehicle(model, x, y, z, rotz, owner, player)
	local newveh = createVehicle(model, x, y, z, 0, 0, rotz, owner)
	local id = table.getn(mysql:poll ("SELECT `ID` FROM `vehicles`", -1)) + 1
	Server.vehicles[newveh] = vehicleClass:new(newveh, owner, false)
	Server.vehicles[newveh]:setData("ID", id)
	Server.vehicles[newveh]:setData("Owner", owner)
	Server.vehicles[newveh]:setData("Fuel", 50)
	Server.vehicles[newveh]:setData("Age", 0)
	Server.vehicles[newveh]:setData("KMStand", 0)
	Server.vehicles[newveh]:setData("Slot", vehiclesTable.getSlots(Server.vehicles[newveh]:getData("Owner")))
	if player then
		local pX, pY, pZ = getElementPosition(player)
	end
	mysql:exec("INSERT INTO `vehicles` VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
	tostring(id),
	tostring(owner),
	tostring(owner),
	tostring(owner),
	model,
	Server.vehicles[newveh]:getData("Slot"),
	Server.vehicles[newveh]:getData("Age"),
	Server.vehicles[newveh]:getData("KMStand"),
	Server.vehicles[newveh]:getData("Fuel"),
	Server.vehicles[newveh]:getData("vehX"),
	Server.vehicles[newveh]:getData("vehY"),
	Server.vehicles[newveh]:getData("vehZ"),
	Server.vehicles[newveh]:getData("vehRZ"),
	Server.vehicles[newveh]:getData("Red"),
	Server.vehicles[newveh]:getData("Green"),
	Server.vehicles[newveh]:getData("Blue"),
	Server.vehicles[newveh]:getData("Red2"),
	Server.vehicles[newveh]:getData("Green2"),
	Server.vehicles[newveh]:getData("Blue2")
	)
	Server.vehicles[newveh]:addKey(owner)
	outputChatBox("Fahrzeug erstellt")
end
addCommandHandler ('newVeh', function ( tplayer,_, model )
	if not getPedOccupiedVehicle ( tplayer ) and model and tonumber(model) then
		local x,y,z = getElementPosition ( tplayer )
		local rx,ry,rz = getElementRotation ( tplayer )
		vehiclesTable.createVehicle(tonumber(model),x,y,z,rx,getPlayerName(tplayer),tplayer)
	end
end,false,false)

addEventHandler("onResourceStop", getResourceRootElement(), function()
table.foreach(Server.vehicles, function(i, v)
	if isElement(Server.vehicles[i]:getVehicle()) then
		mysql:exec("UPDATE `vehicles` SET `VehicleID` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("Model"), Server.vehicles[i]:getData("ID"))
		mysql:exec("UPDATE `vehicles` SET `Owner` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("Owner"), Server.vehicles[i]:getData("ID"))
		mysql:exec("UPDATE `vehicles` SET `vehicle_key_2` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("key:2"), Server.vehicles[i]:getData("ID"))
		mysql:exec("UPDATE `vehicles` SET `vehicle_key_3` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("key:3"), Server.vehicles[i]:getData("ID"))
		--mysql:exec("UPDATE `vehicles` SET `Age` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("Age"), Server.vehicles[i]:getData("ID"))
		mysql:exec("UPDATE `vehicles` SET `PosX` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("vehX"), Server.vehicles[i]:getData("ID"))
		mysql:exec("UPDATE `vehicles` SET `PosY` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("vehY"), Server.vehicles[i]:getData("ID"))
		mysql:exec("UPDATE `vehicles` SET `PosZ` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("vehZ"), Server.vehicles[i]:getData("ID"))
		mysql:exec("UPDATE `vehicles` SET `Rotation` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("vehRZ"), Server.vehicles[i]:getData("ID"))
		mysql:exec("UPDATE `vehicles` SET `KMStand` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("KMStand"), Server.vehicles[i]:getData("ID"))
		mysql:exec("UPDATE `vehicles` SET `Slot` = ? WHERE `ID` = ?", Server.vehicles[i]:getData("Slot"), Server.vehicles[i]:getData("ID"))
	end
end)
end)

addCommandHandler("dev:portVeh", function(player,_, slot)
	local vehs = vehicleClass.vehCMD.getVehicles(player)
	local pos = {getElementPosition(player)}
	setElementPosition(vehs[tonumber(slot)].c_vehicle, pos[1]+3, pos[2], pos[3])
end)

