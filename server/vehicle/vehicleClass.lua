
vehicleClass = {}

function vehicleClass:new (...)
	local obj = setmetatable ( {}, {__index = self} )
	if obj.constructor then
		obj:constructor (...)
	end
	return obj
end


function vehicleClass:respawnTheVehicle()
	respawnVehicle(self.c_vehicle)
	local x, y, z = self:getData("vehX"), self:getData("vehY"), self:getData("vehZ")
	local rZ = self:getData("vehRZ")
	setElementPosition(self.c_vehicle, x, y, z)
	setElementRotation(self.c_vehicle, 0, 0, rZ)
end

function vehicleClass:setData(data, value)
	setElementData(self.c_vehicle, data, value)
end

function vehicleClass:getData(data)
	if data then
		return getElementData(self.c_vehicle, data)
	else
		return false
	end
end


function vehicleClass:addKey(player)
	if Server.players[player] then
		Server.players[player].inv:setData("VehicleKey:"..getVehicleNameFromModel(self:getData("Model")), self.c_vehicle)
	end	
end

function vehicleClass:setFuel()
	
end

function vehicleClass:setEngineState(state)
	setVehicleEngineState(self.c_vehicle, state)
	self:setData("Motor", state)
end

function vehicleClass:getEngineState()
	return self:getData("Motor")
end

function vehicleClass:setLockedState(state)
	setVehicleLocked(self.c_vehicle, state)
	self:setData("Locked", state)
end

function vehicleClass:getLockedState()
	return self:getData("Locked")
end

function vehicleClass:setLightState(state)
	if state then
		setVehicleOverrideLights(self.c_vehicle, 2)
	else
		setVehicleOverrideLights(self.c_vehicle, 1)
	end
	self:setData("Light", state)
end

function vehicleClass:getLightState(state)
	return self:getData("Light")
end


function vehicleClass:setColor(r1,g1,b1,r2,g2,b2)
	if r1 and g1 and b1 and r2 and g2 and b2 then
		setVehicleColor(self.c_vehicle, r1, g1, b1, r2, g2, b2)
	end
end

function vehicleClass:getVehicle()
	return self.c_vehicle
end

function vehicleClass:getSlot()
	return self:getData("Slot")
end

function vehicleClass:getOwner()
	return self.c_owner
end

function vehicleClass:constructor(vehID, owner, slot)
	--Initialisierung
	self.fraktionIDs = {}
	self.c_vehicle = vehID
	self.c_owner = owner
	self.c_commands = {}
	
	
	--Datas setten
	local model = getElementModel(self.c_vehicle)
	local x, y, z = getElementPosition(self.c_vehicle)
	local rX, rY, rZ = getElementRotation(self.c_vehicle)
	local r, g, b, r2, g2, b2 = getVehicleColor(self.c_vehicle, true)
	
	self:setData("Model", model)
	self:setData("Owner", owner)
	self:setData("vehX", x)
	self:setData("vehY", y)
	self:setData("vehZ", z)
	self:setData("vehRZ", rZ)
	self:setData("Fuel", 100)
	self:setData("Motor", false)
	self:setData("Light", false)
	self:setData("Locked", false)
	self:setData("Red", r)
	self:setData("Green", g)
	self:setData("Blue", b)
	self:setData("Red2", r2)
	self:setData("Green2", g2)
	self:setData("Blue2", b2)
	self:setData("Slot", tonumber(slot))
	setVehicleLocked(self.c_vehicle, false)
	setVehicleEngineState(self.c_vehicle, false)
	setVehicleOverrideLights(self.c_vehicle, 1)
	--onExplode
	
	
	addEventHandler("onVehicleEnter", root, function(player, seat)
		self:setEngineState(self:getEngineState())
		if (source == self.c_vehicle) and (seat == 0) and (Server.players[player].inv:getData("VehicleKey:"..getVehicleNameFromModel(self:getData("Model"))) == self.c_vehicle) then
			bindKey(player, "x", "down", function()
				self:setEngineState(not self:getEngineState())
			end)
			bindKey(player, "l", "down", function()
				self:setLightState(not self:getLightState())
			end)
			
			local _func = function() removeEventHandler("onVehicleExit", root, unbind) end
			local unbind = function() unbindKey(player, "x") unbindKey(player, "l") end
			
			addEventHandler("onVehicleExit", root, unbind)
		end
	end)
	
	
	self.c_timer2 = setTimer ( function () self:setFuel() end, 10000, 0 )
	addEventHandler ("s.playerClass:loggedIn", root, function(source) 
		if source == ((getPlayerFromName(self:getData("key:1"))) or (getPlayerFromName(self:getData("key:2"))) or (getPlayerFromName(self:getData("key:3")))) then 
			self:addKey(source) 
		end	
	end)	
end





