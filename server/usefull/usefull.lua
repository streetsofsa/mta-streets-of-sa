function infobox(player, text, type)
	if type == nil then type = "info" end
	triggerClientEvent(player, "_infobox", player, text, type)
end

--[[function getPlayerFromName(name)
    local name = name and name:gsub("#%x%x%x%x%x%x", ""):lower() or nil
    if name then
        for _, player in ipairs(getElementsByType("player")) do
            local name_ = getPlayerName(player):gsub("#%x%x%x%x%x%x", ""):lower()
            if name_:find(name, 1, true) then
                return player
            end
        end
    end
end--]]

function table.contains(tbl, element)
	table.foreach(tbl, function(_, v)
		if v == element then
			return true
		end
	end)
	return false
end