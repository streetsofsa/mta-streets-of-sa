Factions = {}

FactionClass = {}

function FactionClass:new(...)
	local obj = setmetatable({}, {__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
	return obj
end

function FactionClass:isMember(player)
	if self.c_members[player] then
		return true
	end
	return false
end


function FactionClass:addMember(member, rang)
	
	outputChatBox(string.format("FACTION SYSTEM: Adding member %s to faction %s (%s), Rank: %s", member:getName(), self.c_faction, self.c_fid, rang), root, 0, 100, 150)
	
	self.c_members[member] = rang or 1
	Server.players[member]:setData("Faction", self.c_fid)
	Server.players[member]:setData("Rang", rang or 1)
	bindKey(member, "y", "down", "chatbox", "Fraktionschat")
end

function FactionClass:removeMember(member)
	outputChatBox(string.format("FACTION SYSTEM: Removing member %s from faction %s (%s)", member:getName(), self.c_faction, self.c_fid), root, 0, 100, 150)
	self.c_members[member] = nil
	Server.players[member]:setData("Faction", 0)
	Server.players[member]:setData("Rang", 0)
	unbindKey(member, "y", "down", "chatbox")
end

function FactionClass:setRank(member, rank)
	outputChatBox(string.format("FACTION SYSTEM: Setting rank of member %s from faction %s (%s) to %s", member:getName(), self.c_faction, self.c_fid, rank),  root, 0, 100, 150)
	if self.c_members[member] then
		self.c_members[member] = tonumber(rank)
		Server.players[member]:setData("Rang", tonumber(rank))
	end
end



function FactionClass:constructor(fname, fid)
	Server:outputInfo("[CALLING] FactionClass: Constructor")
	self.c_faction = fname
	self.c_fid = tonumber(fid)
	
	self.ranks = {} --rank, title
	self.c_members = {} --name, rank
	self.alliedFactions = {self.c_fid} --factions are able to work together, e.g. use the same vehicles
	
	self.c_commands = {}

	self.f_commands = {}
	self.c_alliedFactions = {self.c_fid}
	
	self.vehicles = self.vehicles:new(self.c_fid)
	self.depot = self.depot:new(self.c_fid)
	
	--||Allround Faction Functions||--
	self.chat = function(player, msg)
		table.foreach(self.c_members, function(i, _)
			outputChatBox(tostring(self.ranks[self.c_members[player]]).." "..player:getName()..": "..table.concat(msg, " "), i, 8, 216, 206)
			--outputChatBox("PLAYER RANK:"..tostring(self.c_members[player])..":"..Server.players[player]:getData("Rang"))
		end)
	end
	
	
	addCommandHandler("Fraktionschat", function(player, _, ...)
		if self.c_members[player] then
			self.chat(player, {...})
		end
	end)
	
	
	--||Allround Faction Commands||--
	
	self.c_commands["ADDMEMBER"] = function(source, player) 
		if self.c_members[source] and (self.c_members[source] >= 6) then
			self:addMember(player, source)
		end
	end	
	
	self.c_commands["REMOVEMEMBER"] = function(source, player) 
		if self.c_members[player] and (self.c_members[source] >= 6) then
			self:removeMember(player, source)
		end
	end	
	
	self.c_commands["SETMEMBERRANK"] = function(source, player, rank)
		if self.c_members[player] and (self.c_members[source] >= 6) then
			self:setRank(player, rank, source)
		end
	end
	
	self.c_commands["RESPAWNVEH"] = function(source, veh)
		if self.c_members[source] and (self.c_members[source] >= 5) then
			self.vehicles:respawn(veh)
		end
	end
	
	self.c_commands["SETVEHSPAWNPOS"] = function(source, veh)
		if self.c_members[source] and (self.c_members[source] >= 5) then
			self.vehicles:setSpawnPosition(veh)
		end		
	end
	
	self.c_commands["ADDVEHICLE"] = function(source, veh)
		if self.c_members[source] and (self.c_members[source] >= 5) then
			self.vehicles:addVehicle(veh)
		end		
	end

	self.c_commands["SETVEHSPAWNPOS"] = function(source, veh)
		if self.c_members[source] and (self.c_members[source] >= 5) then
			self.vehicles:setSpawnPosition(veh)
		end		
	end	
	
	table.foreach(self.c_commands, function(i, v)
		addCommandHandler(i, function(player, _, ...)
			if self.c_members[player] then
				v(player, ...)
			end
		end, false, false)
	end)
	
end

