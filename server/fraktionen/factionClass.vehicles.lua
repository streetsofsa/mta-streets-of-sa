
FactionClass.vehicles = {}

function FactionClass.vehicles:new (...)
	local obj = setmetatable ( {}, {__index = self} )
	if obj.constructor then
		obj:constructor (...)
	end
	return obj
end


function FactionClass.vehicles:addVehicle()

	
end


function FactionClass.vehicles:setSpawnPosition(veh)
	if not veh then
		return
	end
	local pos = {getElementPosition(veh), getElementRotation(veh)}
	veh:setRespawnPosition(unpack(pos))
end


function FactionClass.vehicles:respawn(veh)
	if not veh then-- alle fraki vehs respawnen
		table.foreach(self.c_vehicles, function(i, _)
			i:respawn()
			setVehicleEngineState(i, false)
		end)
	else--bestimmtes fraki veh respawnen
		table.foreach(self.c_vehicles, function(i, _)
			if v == veh then
				i:respawn()
				setVehicleEngineState(false)
			end
		end)
	end
end

function FactionClass.vehicles:constructor(fid)
	Server:outputInfo("[CALLING] FactionClass.vehicles: Constructor")	
	self.c_faction = fid
	self.c_vehicles = {}
	
	local sql = mysql:poll("SELECT * FROM `factionvehicles` WHERE `Faction` = '"..self.c_faction.."'", -1)
	table.foreach(sql, function(_, v)
		local newveh = createVehicle(tonumber(v["VehicleID"]), tonumber(v["PosX"]), tonumber(v["PosY"]), tonumber(v["PosZ"]), 0, 0, tonumber(v["Rotation"]), Fraktionen.settings.name[tonumber(v["Faction"])])
		self.c_vehicles[newveh] = tonumber(v["Rank"]) --Mindestrang
		setElementData(newveh, "Faction", self.c_faction)
		
		setVehicleOverrideLights(newveh, 1)
		setVehicleEngineState(newveh, false)
	end)
	
	
	self.onEnter = function(player, seat, _)
	local veh = source
	setVehicleEngineState(veh, false)
	
	if self.c_vehicles[veh] then
		if seat == 0 then

			setVehicleEngineState(veh, false)
			if (Server.players[player]:getData("Faction") ~= self.c_faction) or (not Server.factions[self.c_faction].alliedFactions[Server.players[player]:getData("Faction")]) then
				cancelEvent()
			else	
			if (Server.players[player]:getData("Faction") == self.c_faction) or (Server.factions[self.c_faction].c_alliedFactions[Server.players[player]:getData("Faction")]) then
				bindKey(player, "x", "down", function()
					setVehicleEngineState(veh, not getVehicleEngineState(veh))
				end)
				bindKey(player, "l", "down", function()
					if getVehicleOverrideLights(veh) == 1 then
						setVehicleOverrideLights(veh, 2)
					elseif getVehicleOverrideLights(veh) == 2 then
						setVehicleOverrideLights(veh, 1)
					end	
				end)
				local _func = function() removeEventHandler("onVehicleExit", root, unbind) end
				local unbind = function() unbindKey(player, "x") unbindKey(player, "l") end
			
				addEventHandler("onVehicleStartExit", root, unbind)
				addEventHandler("onVehicleStartExit", root, unbind)			
			else	
				--if not  then
				outputChatBox("NOPE")
					cancelEvent()
				--end
			end	
		end
	end
	end
	end
	addEventHandler("onVehicleStartEnter", root, self.onEnter)
	
	
end

--[[
fVehicleTable = {}

function fVehicleTable.loadAll()
	local sql = mysql:poll("SELECT * FROM `factionvehicles`", -1)
	table.foreach(sql, function(i, v)
	if v["Faction"] ~= nil and v["Faction"] ~= 0 then
		local newveh = createVehicle(tonumber(v["VehicleID"]), tonumber(v["PosX"]), tonumber(v["PosY"]), tonumber(v["PosZ"]), 0, 0, tonumber(v["Rotation"]), Fraktionen.settings.name[tonumber(v["Faction"])])
		if isElement(newveh) then
		Server.vehicles[newveh] = Server.factions:new(newveh, Fraktionen.settings.name[tonumber(v["Faction"])], v["ID"], true)
		Server.vehicles[newveh]:setData("ID", v["ID"])
		Server.vehicles[newveh]:addFraktion(tonumber(v["Faction"]))
		Server.vehicles[newveh]:setData("Fuel", tonumber(v["Fuel"]))
		Server.vehicles[newveh]:setData("Faction", tonumber(v["Faction"]))
		end
	end
	end)
end
addEventHandler("onResourceStart", getResourceRootElement(), fVehicleTable.loadAll)]]--