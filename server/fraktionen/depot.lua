Fraktionsdepot = {}
fDepot = {}

function fDepot:new (...)
	local obj = setmetatable ( {}, {__index = self} )
	if obj.constructor then
		obj:constructor (...)
	end
	return obj
end

function fDepot:setMoney(money)
	self.c_money = money
end

function fDepot:setDrugs(drugs)
	self.c_drugs = drugs
end

function fDepot:setMats(mats)
	self.c_mats = mats
end

function fDepot:getMoney()
	return self.c_money
end

function fDepot:getDrugs()
	return tostring(self.c_drugs)
end

function fDepot:getMats()
	return tostring(self.c_mats)
end

function fDepot:getID()
	return tostring(self.c_id)
end

function fDepot:constructor(fID)
	self.c_id = fID
	self.c_depot = {}
	self.c_money = 0
	self.c_drugs = 0
	self.c_mats = 0
	
end

addEventHandler("onResourceStart", getResourceRootElement(), function()
	local sql = mysql:poll("SELECT * FROM `factiondepot`", -1)
	table.foreach(sql, function(i, v)
		Fraktionsdepot[tonumber(v["fID"])] = fDepot:new(tonumber(v["fID"]))
		Fraktionsdepot[tonumber(v["fID"])]:setMoney(v["Kasse"])
		Fraktionsdepot[tonumber(v["fID"])]:setDrugs(v["Drogen"])
		Fraktionsdepot[tonumber(v["fID"])]:setMats(v["Mats"])
	end)
end)

addEventHandler("onResourceStop", getResourceRootElement(), function()
table.foreach(Fraktionsdepot, function(i, v)
	mysql:exec("UPDATE `factiondepot` SET `Kasse` = ? WHERE `fID` = ?", Fraktionsdepot[i]:getMoney(), Fraktionsdepot[i]:getID())
	mysql:exec("UPDATE `factiondepot` SET `Drogen` = ? WHERE `fID` = ?", Fraktionsdepot[i]:getDrugs(), Fraktionsdepot[i]:getID())
	mysql:exec("UPDATE `factiondepot` SET `Mats` = ? WHERE `fID` = ?", Fraktionsdepot[i]:getMats(), Fraktionsdepot[i]:getID())
end)
end)

addCommandHandler("cdepot", function(player, cmd, fraktion)
	Fraktionsdepot[tonumber(fraktion)] = fDepot:new(tonumber(fraktion))
	mysql:exec("INSERT INTO `factiondepot` VALUES (?,?,?,?)",
	tonumber(fraktion),
	0,
	0,
	0
	)
end)




















