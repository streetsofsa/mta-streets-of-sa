--[[
0 = Zivilist
1 = SAPD
2 = SA Army
3 = FBI
4 = SAN News
5 = Medic
6 = Grove Street
7 = Mafia
8 = SAAP
9 = Yakuza
10 = Hitman
11 = DoD
]]--

function isFraktion(player, fraktion)
if Server.players[player]:getData("Faction") == tonumber(fraktion) then
	return true
end
end

function isSAPD(player)
if Server.players[player]:getData("Faction") == 1 then
	return true
else
	return false
end
end

function isArmy(player)
if Server.players[player]:getData("Faction") == 2 then
	return true
else
	return false
end
end

function isFBI(player)
if Server.players[player]:getData("Faction") == 3 then
	return true
else
	return false
end
end

function isNews(player)
if Server.players[player]:getData("Faction") == 4 then
	return true
else
	return false
end
end

function isMedic(player)
if Server.players[player]:getData("Faction") == 5 then
	return true
else
	return false
end
end

function isMafia(player)
if Server.players[player]:getData("Faction") == 7 then
	return true
else
	return false
end
end

function isSAAP(player)
if Server.players[player]:getData("Faction") == 8 then
	return true
else
	return false
end
end

function isYakuza(player)
if Server.players[player]:getData("Faction") == 9 then
	return true
else
	return false
end
end

function isHitman(player)
if Server.players[player]:getData("Faction") == 10 then
	return true
else
	return false
end
end

function isDoD(player)
if Server.players[player]:getData("Faction") == 11 then
	return true
else
	return false
end
end

