--||||||||||||||||||||||||||||||||||||
--|||Name: Serverside Core-Class   |||
--|||Project: MTA Streets of SA    |||
--|||Author:                       |||
--|||Version:                      |||
--||||||||||||||||||||||||||||||||||||
ServerClass = {};
ServerClass.__index = ServerClass;

addEvent("onPlayerLoggedIn", true)
ServerClass.Events = {
	["player:join"] = nil,
	["player:leave"] = nil
	
}

function ServerClass:parseSettings()
	
	local file = xmlLoadFile("settings.xml")
	
	self.settings.db = {}
	local dbSettings = file:findChild("db", 0)
	table.foreach(dbSettings:getChildren(), function(_, v)
		self.settings.db[v:getName()] = v:getValue()
	end)
	
	self.DEBUGMODE = (file:findChild("DEBUGMODE", 0)):getValue()
	
end

function ServerClass:triggerToClient(...)
	triggerClientEvent(...)
end

function ServerClass:setTime()
    local time = getRealTime()
	local hour = time.hour
	local minute = time.minute
	setTime(hour, minute)
	setMinuteDuration(60000)
	setWeather(0)
end

function ServerClass:initialize()
	setOcclusionsEnabled(false)
	self:setTime()
	
	
	--self.systems.weather = weatherSystem:constructor()
	
end


function ServerClass:constructor()
	self.players = {}
	self.vehicles = {}
	self.factions = {}
	self.biz = {}
	
	self.settings = {}
	self.systems = {}
	self.c_resource = resource
	self:parseSettings()

	self:outputInfo("[CALLING] Server: Constructor")	
	
	self.db = mysql:connect(self.settings.db.host, self.settings.db.username, self.settings.db.password, self.settings.db.dbname)--Setting up database connection
	if not self.db then
		self:outputInfo("[ERROR]: Database connection failed, stopping gamemode...")
		stopResource(self.c_resource)
		return
	end
	
	self:initialize()
	
	return self
end


function ServerClass:log(file, msg)
	local time = {}
	local rtime = getRealTime()
	local logFile
	time.day = string.format("%02d", rtime.monthday)
	time.month = string.format("%02d", tostring(rtime.month + 1))
	time.year = getRealTime().year + 1900
	time.minute = string.format("%02d", rtime.minute)
	time.hour = string.format("%2d", rtime.hour)
	time.second = string.format("%02d", rtime.second)
	
	if fileExists("/logs/"..file.."/"..time.day.."."..time.month.."."..time.year.."/"..file..".log") then
		logFile = fileOpen("/logs/"..file.."/"..time.day.."."..time.month.."."..time.year.."/"..file..".log")
	else
		logFile = fileCreate("/logs/"..file.."/"..time.day.."."..time.month.."."..time.year.."/"..file..".log")
		logFile = fileOpen("/logs/"..file.."/"..time.day.."."..time.month.."."..time.year.."/"..file..".log")
	end
	local size = fileGetSize(logFile)
	fileSetPos(logFile,size)
	fileWrite(logFile,"["..time.hour..":"..time.minute..":"..time.second.."] "..msg.."\n")
	fileClose(logFile)
end

function ServerClass:outputInfo(info)
	if self.DEBUGMODE == "true" then
		outputDebugString(info, 0, 30, 30, 200)
		
		self:log("debug", info)
		return
	end	
	return "This function was disabled"
end

--||Creating global Server var
Server = ServerClass:constructor()


outputServerLog("!-------------------------------------!")
outputServerLog("!                                     !")
outputServerLog("!            Streets of SA            !")
outputServerLog("!              Reallife               !")
outputServerLog("!        By Apoollo & ravecow         !")
outputServerLog("!                                     !")
outputServerLog("!-------------------------------------!")
