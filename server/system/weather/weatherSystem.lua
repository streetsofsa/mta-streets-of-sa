

weatherSystem = {}
weatherSystem.__index = weatherSystem;


function weatherSystem:constructor()
	Server:outputInfo("[CALLING] weatherSystem: Constructor")
	
	local self = {}
	
	
	self.weatherIDs = {0, 1, 4, 8, 9}
	
	data.rainlevel = 0
	
	
	self:changeWeather()

	return self
end

function weatherSystem:changeWeather()
	local wID = math.random(#self.weatherIDs)
	setWeatherBlended(wID)
	
	if wID == 8 then --Rain
		setRainLevel(math.floor(math.random(5, 25) / math.random(5, 10)))
	else
		resetRainLevel()
	end
	
end


addCommandHandler("cw", function()
	Server.systems.weather:changeWeather()
end)