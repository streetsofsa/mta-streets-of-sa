ATM = {}

function ATM:new(...)
	local obj = setmetatable({}, {__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
end


function ATM:onClicked(btn, state, player)
	if not (btn == "left" and state == "down") then
		return
	end
	triggerClientEvent(player, "c.economy.atm:click", player, self.atm)
	
end

function ATM:constructor(x,y,z,rx,ry,rz)
	self.atm = createObject(2942, x, y, z, rx, ry, rz)
	
	self.clicked = function(...) 
	if source == self.atm then
		self:onClicked(...)
	end
	end
	
	addEventHandler("onElementClicked", root, self.clicked)
end

addCommandHandler("atm", function(player)
	local pos = {getElementPosition(player)}
	outputChatBox(tostring(pos[1]))
	ATM:new(pos[1], pos[2], pos[3], 0, 0, 0)
end)