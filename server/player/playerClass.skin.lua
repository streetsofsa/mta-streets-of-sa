
playerClass.skin = {}
playerClass.skin.events = {
	"skin.update"
}
addEvent("s.playerClass:skin.update", true)

playerClass.skin.skinTable = {
	--SHIRT[int], HEAD[int], TROUSERS[int], SHOES[int], HATS[int], GLASSES[int] = {SKIN1, SKIN2, SKIN3}
	["0,0,0,0,0,0"] = {70, 100, 130},
	["0,0,1,0,0,0"] = {71, 101, 131}

}

function playerClass.skin:new(...)
	local obj = setmetatable ({},{__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
	return obj
end

function playerClass.skin:setData(index, data)
	self.data[index] = tonumber(data)
end

function playerClass.skin:getData(index)
	
	return self.data[index]
end

addCommandHandler("csid", function(player, _, sid)
	Server.players[player].skin:setData("SKIN", tonumber(sid))
	Server.players[player].skin:updateSkin()
end)

function playerClass.skin:destructor()
	table.foreach(self.data, function(i, v)
		mysql:exec("UPDATE `pSkin` SET `"..i.."` = ? WHERE `ID` = ?", v, Server.players[self.c_player]:getData("ID"))
	end)
	self = nil
end

function playerClass.skin:updateSkin()
	
	local skin = (self.skinTable[tostring(self:getData("SHIRT")..","..self:getData("HEAD")..","..self:getData("TROUSERS")..","..self:getData("SHOES")..","..self:getData("HATS")..","..self:getData("GLASSES"))])
	outputChatBox("[SOSA SKIN]: Updating player skin for "..self.c_player:getName().." to "..skin[self:getData("SKIN")].."!")
	self.c_player:setModel(skin[self:getData("SKIN")])
end

function playerClass.skin:setSkinDatas()
	local sql = mysql:poll("SELECT * FROM `pSkin` WHERE `ID` = '"..Server.players[self.c_player]:getData("ID").."'", -1)
	if #sql > 0 then
		table.foreach(sql[1], function(i, v)
			self:setData(i, tonumber(v))
		end)
	
	else
		--ID[int], SKIN[int(1-4)], SKINCOLOR[int(0-100)], SHIRT[int], HEAD[int], TROUSERS[int], SHOES[int], HATS[int], GLASSES[int]
		local result = mysql:exec("INSERT INTO `pSkin` VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", Server.players[self.c_player]:getData("ID"), 1, 0, 0, 0, 0, 0, 0, 0)
		self:setSkinDatas()
		
	end
	
	self:updateSkin()
end

function playerClass.skin:constructor(player)
	--outputChatBox("[SOSA SKIN]: Creating new skin player class for "..player:getName().."!", root, 200) 

	self.c_player = player
	self.data = {}
	
	self:setSkinDatas()
	
	self.update = function(...) self:updateSkin(...) end
	
	addEventHandler("s.playerClass:skin.update", root, self.update)
	
end

addCommandHandler("skin", function(player, cmd, slot, id)
	Server.players[player].skin:setClothes(tonumber(slot), tonumber(id))
end)

addCommandHandler("REG", function(player)
	Server.players[player] = playerClass:new(player) 
end)