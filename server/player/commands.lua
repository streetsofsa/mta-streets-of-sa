playerCommands = {}

function playerCommands.spawnChange(player, cmd)
	local pos = {getElementPosition(player)}
    Server.players[player]:setData("spawnX", pos[1])
    Server.players[player]:setData("spawnY", pos[2])
    Server.players[player]:setData("spawnZ", pos[3])
	infobox(player, "Spawnpoint gesetzt!")
end
addCommandHandler("spawnchange", playerCommands.spawnChange)


addEventHandler ("onPlayerChangeNick", getRootElement(), function()
	cancelEvent ()
	infobox(source, "Du darfst deinen Nickname nicht aendern!", "error")
end)

addCommandHandler ('setinv', function ( player,_,target,index,value)
	if target then
		local target = getPlayerFromName ( target )
		if isElement ( target ) then
			pInv[target]:setData (index,tonumber(value))
			outputChatBox("Gesettet")
		end
	end
end)	


addCommandHandler("getpos", function(player, cmd)
	local pos = {getElementPosition(player)}
	local dim = {getElementDimension(player)}
	local int = {getElementInterior(player)}
	outputChatBox("X: "..pos[1]..", Y: "..pos[2]..", Z: "..pos[3])
	outputChatBox("Dim: "..dim[1])
	outputChatBox("Int: "..int[1])
end)