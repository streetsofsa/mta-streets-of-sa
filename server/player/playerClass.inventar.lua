

playerClass.inv = {}

function playerClass.inv:new (...)
	local obj = setmetatable ({},{__index = self} )
	if obj.constructor then
		obj:constructor(...)
	end
	return obj
end

function playerClass.inv:setData(data,value)
	self.c_items[data] = value		
end

function playerClass.inv:getData(data)
	if self.c_items[data] then
		return self.c_items[data]	
	else
		return false
	end
end


function playerClass.inv:removeData(item) 
	if self.c_items[item] then
		self.c_items[item] = nil
	end
end

function playerClass.inv:openInventory(player)
	if player == self.c_user then
		local cData = {}
		local cData2 = {}
		table.foreach(self.c_items, function(i, v)
			if type(v) == "userdata" then
				v = 1
			end
			
			if not string.find(tostring(i), "wp_") then
				
				if (type(v) == "number") and (v > 0) then
					cData[i] = v
					
				end
			end
		end)
		
		triggerClientEvent(self.c_user, "c.playerClass:openInventory", self.c_user, cData, cData2)
	end
end

function playerClass.inv:setDatas()
	local sql = mysql:poll("SELECT * FROM `pInventar` WHERE `Name` = '"..self.c_player.."'", -1)
	if table.getn(sql) > 0 then
		--[[for i = 1, 5 do
			self:setData("weapon_"..i, h["weapon_"..i])
			local wpData = self:getData("weapon_"..i)
	--		outputChatBox(wpData)
--			local weapon = tonumber(gettok(wpData, 1, ";"))
		--	local ammo = tonumber(gettok(wpData, 2, ";"))
			--if ammo > 0 then
				--giveWeapon(self.c_user, weapon, ammo)
			--end
		end]]--
		table.foreach(sql[1], function(i, v)
			--outputChatBox("[INV]SETTING DATA:: "..tostring(i).." , SETTING VALUE:: "..tostring(v))

			if tonumber(v) and (i ~= "ID") then
				self:setData(i, tonumber(v))
			else
				self:setData(i, v)
			end
		end)
	else
		local id = table.getn(mysql:poll ("SELECT `ID` FROM `pInventar`", 1000))+1 
		mysql:exec("INSERT INTO `pInventar` VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", id, self.c_player, 0, 0, "0;0", "0;0", "0;0", "0;0", "0;0", "0", "0", "0", 0)
		self:setDatas()
	end
end

function playerClass.inv:destructor()
	mysql:exec("UPDATE `pInventar` SET `Drogen` = ? WHERE `Name` = ?", self:getData("Drogen"), self.c_player)
	mysql:exec("UPDATE `pInventar` SET `Mats` = ? WHERE `Name` = ?", self:getData("Mats"), self.c_player)
	mysql:exec("UPDATE `pInventar` SET `essen_slot1` = ? WHERE `Name` = ?", self:getData("Snacks"), self.c_player)
	mysql:exec("UPDATE `pInventar` SET `essen_slot2` = ? WHERE `Name` = ?", self:getData("Hamburger"), self.c_player)
	mysql:exec("UPDATE `pInventar` SET `essen_slot3` = ? WHERE `Name` = ?", self:getData("Fertigessen"), self.c_player)
end

function playerClass.inv:constructor(player)
	
	self.cmd = {}
	self.c_items = {}
	self.cmd["open"] = function(player) self:openInventory(player) end
	self.c_player = player
	self.c_user = getPlayerFromName(player)
	self:setDatas()
	addCommandHandler("invlook", function()
		table.foreach(self.c_items, function(i, v)
			outputChatBox("ITEM |"..tostring(i).."|   VALUE |"..tostring(v).."|")
		end)
	end)
	bindKey(self.c_user, "i", "down", self.cmd["open"])
end