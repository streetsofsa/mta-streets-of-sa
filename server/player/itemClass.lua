--|||||||||||||||||||||||||||||||||||||
--||||Name: Item Drop&Pickup        ||||
--||||Author: Apoollo, ravecow      ||||
--||||Project: Streets of SA        ||||
--||||Version: 1.0                  ||||
--||||||||||||||||||||||||||||||||||||||

local pItems = {} --Zählertable

item = {}

function item:new(...)
	local obj = setmetatable ({},{__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
	return obj
end


function item:constructor(x,y,z,typ,amount)
	if type(typ) ~= "string" then
		return false
	end

	if typ == "money" then
		self.item = createObject(1212, x, y, z - 0.5)
		setElementCollisionsEnabled(self.item, false)
		table.insert(pItems, {self.item, amount})
	end
	self.id = #pItems
	self.x = x
	self.y = y
	self.z = z
	self.typ = typ
	self.amount = amount
	self.click = function(player, button, state) self:handler(button, state, player) end
	self.hit = function(player) if getElementType(player) == "player" then bindKey(player, "e", "down", self.click)  end end
	self.leave = function(player) if getElementType(player) == "player" then unbindKey(player, "e") end end
	
	self.col = createColSphere(x, y, z, 0.5)
	
	addEventHandler("onColShapeHit", self.col, self.hit)
	addEventHandler("onColShapeLeave", self.col, self.leave)

end

function item:handler(button, state, player)
	if not button == "left" and not state == "down" or not isElement(self.col) then
		return
	end
	if isElementWithinColShape(player, self.col) then
	if self.typ == "money" then
		infobox(player, "Du hast "..self.amount.."$ aufgehoben!", "info")
		Server.players[player]:setData("Handgeld", Server.players[player]:getData("Handgeld") + tonumber(self.amount))
		self:destroy()
	end
	end
end

function item:destroy()
	setElementPosition(self.col, 0,0,0)
	removeEventHandler("onColShapeHit", self.col, self.hit)
	removeEventHandler("onColShapeLeave", self.col, self.leave)

	destroyElement(self.item)
	destroyElement(self.col)
end

addCommandHandler("drop", function(player, cmd, amount)
	if not amount then amount = 1 end
	local pos = {getElementPosition(player)}
	item:new(pos[1],pos[2], pos[3], "money", tonumber(amount))

end)
