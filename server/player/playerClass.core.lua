

playerClass = {skin = {}, inv = {}}
playerClass.events = {
	"requestLogin",
	"requestRegister"
}
table.foreach(playerClass.events, function(_, v)
	addEvent("s.playerClass:"..v, true)
end)


function playerClass:new (...)
	local obj = setmetatable ({},{__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
	return obj
end

--"UPDATE `playerData` SET `"..data.."`='"..value.."' WHERE `Name` = '"..pname.."'"
function playerClass:logout()
	if self:getData("loggedIn") then
		local pos = {getElementPosition(self.c_player)}
		Server.factions[self:getData("Faction")].c_members[self.c_player] = nil
		outputServerLog("player: logout")
		mysql:exec ("UPDATE `playerData` SET `Handgeld` = ? WHERE `ID` = ?",self:getData("Handgeld"),self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Bankgeld` = ? WHERE `ID` = ?",self:getData("Bankgeld"),self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Wanteds` = ? WHERE `ID` = ?",self:getData("Wanteds"),self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Spielzeit` = ? WHERE `ID` = ?",self:getData("Spielzeit"),self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Social` = ? WHERE `ID` = ?",self:getData("Social"),self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Jailtime` = ? WHERE `ID` = ?",self:getData("Jailtime"),self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Faction` = ? WHERE `ID` = ?",self:getData("Faction"),self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Rang` = ? WHERE `ID` = ?",self:getData("Rang"),self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `spawnX` = ? WHERE `ID` = ?",pos[1],self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `spawnY` = ? WHERE `ID` = ?",pos[2],self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `spawnZ` = ? WHERE `ID` = ?",pos[3],self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Adminlevel` = ? WHERE `ID` = ?",self:getData("Adminlevel"),self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Job` = ? WHERE `ID` = ?",self:getData("Job"),self:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Unternehmen` = ? WHERE `ID` = ?",self:getData("Unternehmen"),self:getData("ID"))
		outputServerLog("Daten des Spielers "..self:getName().."(ID:"..self:getData("ID")..")".." wurden gespeichert!")	
		
	else
		outputServerLog("Fehler beim Speichern der Daten des Spielers "..getPlayerName(self.c_player).."!")
	end
	self:destructor()
end


function playerClass:isLoggedIn()
	return self:getData("loggedIn")
end

function playerClass:getName()
	return getPlayerName(self.c_player)
end

function playerClass:wasted(player, ammo, killer, killerWeapon, bpart)
	triggerClientEvent(self.c_player, "c.playerClass:wasted", player, ammo, killer, killerWeapon, bpart)
end


function playerClass:setFaction(fromPlayer, faction, rank)
	--outputDebugString("Faction,Rank: "..tostring(faction)..","..tostring(rank))
	local oldFaction, oldRank = self:getData("Faction"), self:getData("Rang")
	
	if not (tonumber(rank) >= 0 and tonumber(rank) <= 7) then
		return
	end
	
	
	if oldFaction == faction then
		if oldRank == rank then
			return
		end
	
		Server.factions[faction]:setRank(self.c_player, rank)
		return
	end	
	
	
	if (tonumber(faction) == 0) or (tonumber(rank) == 0) then
		Server.factions[oldFaction]:removeMember(self.c_player)
		outputChatBox(string.format("%s hat dich zum Zivilisten gemacht!", fromPlayer:getName()), self.c_player, 0, 100, 150)
		return 
	end
	
	if Server.factions[faction] then
		Server.factions[oldFaction]:removeMember(self.c_player)
		
		Server.factions[faction]:addMember(self.c_player, tonumber(rank))
	
		return
	else
		return outputDebugString("Error @ setFaction: Fraktion existiert nicht!")
	end
end


function playerClass:setData(data, value)
	if not data or not value then
		return "setData: An error occured"
	end
	
	self.Datas[data] = value
	if data == "Handgeld" then
		setPlayerMoney (self.c_player,self:getData("Handgeld"))
	end
	
	if data ~= "Passwort" then
		self:updatePlayerData(data, value)
	end	
end


function playerClass:getData(data)
	if self.Datas[data] then
		return self.Datas[data]	
	else
		return false
	end
end

function playerClass:spawnThePlayer()
--[[	
	local int = self:getData("spawnInt")
	spawnPlayer(self.c_player, x, y, z)
	setElementInterior(self.c_player, int)
	
	setCameraTarget(self.c_player, self.c_player)]]--
	setElementModel(self.c_player, 1)
	spawnPlayer(self.c_player, self:getData("spawnX"), self:getData("spawnY"), self:getData("spawnZ"))
	setCameraTarget(self.c_player, self.c_player)
end



function playerClass:setSpielzeit()
	self:setData ("Spielzeit",self:getData("Spielzeit") + 1)
end


function playerClass:login(pw)
	local truepw = mysql:poll("SELECT `Password` FROM `userData` WHERE `Name` = '"..self:getName().."'", -1)
	if hash("sha512", pw) == truepw[1]["Password"] then
		self:setData("pwChecked", true)
		self:setDatas()
	else
		infobox(self.c_player, "Das Passwort ist falsch!", "error")
	end
end


function playerClass:setPlayer()
	local playerDataTable = mysql:poll("SELECT * FROM `playerData` 
	--||Daten laden||--
	table.foreach(data[1], function(i, v)
		--outputChatBox("SETTING DATA:: "..tostring(i).." , SETTING VALUE:: "..tostring(v))
		if tonumber(v) then
			self:setData(i, tonumber(v))
		else
			self:setData(i, v)
		end
	end)
	--Set faction @ login
	if (self:getData("Faction") > 0) and (Server.factions[self:getData("Faction")]) then
		Server.factions[self:getData("Faction")]:addMember(self.c_player, tonumber(self:getData("Rang")))
	end		
	
	if (self:getData("Adminlevel") > 0) then
		adminsIngame[self.c_player] = true
	end	
	
	self:setData("currentHouse", false)	
	
	self:spawnThePlayer()
	--||Creating player classes
	self.inv = self.inv:new(getPlayerName(self.c_player)) 
	self.skin = self.skin:new(self.c_player)	
	--||
	triggerEvent("s.playerClass:loggedIn", root, self.c_player)

	
	self:trigger("acceptLogin")
	
	--self.c_timer1 = setTimer(function() self:setSpielzeit() end, 1000*60, 0)
	
	addEventHandler("onPlayerQuit", getRootElement(), self.c_logout)
	addEventHandler("onPlayerWasted", getRootElement(), self.c_wasted)

	
	
	self:setData("loggedIn", true)
end


function playerClass:setDatas()
	local userDataResult = mysql:poll("SELECT `ID` FROM `userData` WHERE `Name` = '"..getPlayerName(self.c_player).."'", -1)
	
	if #userDataResult > 0 then --Player already registered
		if self:getData("pwChecked") then
			self:setPlayer()
		else
			self:trigger("requestLogin")
		end
	
	elseif	#userDataResult == 0 then
		self:trigger("c.playerClass:requestRegister")
		self:setData("Spielzeit", 0)
	end
end


function playerClass:destructor()
	if self:getData("loggedIn") then
		self.inv:destructor()
		self.skin:destructor()
		
		--killTimer(self.c_timer1)
		--killTimer(self.c_timer2)
	end
	self = nil
end

function playerClass:trigger(cmd, ...)
	triggerClientEvent(self.c_player, "c.playerClass:"..cmd, self.c_player, ...)
end

function playerClass:setSpielzeit()
if self:getData("loggedIn") then
	self:setData ("Spielzeit",self:getData("Spielzeit") + 1)
	local h = math.floor(self:getData("Spielzeit")/60)
	local m = math.floor(self:getData("Spielzeit")-(h*60))
	local Spielzeit = h..":"..m
	
	if self:getData("Spielzeit")/60 == math.floor(self:getData("Spielzeit")/60) then
		--playerClass:payday()
	end
end	
end

function playerClass:updatePlayerData(data, value)
--	if isElement(self.c_player) then
--	--	self:trigger("updateData", self.Datas)
	--	
	--end
if isElement(self.c_player) then
	--local cData = {}
	table.foreach(Server.players, function(i, v)
		--outputChatBox(string.format("[SERVER]triggerFor: %s, element: %s, index: %s, value: %s", tostring(v.c_player), tostring(self.c_player), tostring(data), tostring(value)), self.c_player, c[1], c[2], c[3])
		Server:triggerToClient(v.c_player, "client:receiveData", self.c_player, data, value)
	end)
	--triggerClientEvent(self.c_player, "clientData_playerData", self.c_player, cData)
end	
end

function playerClass:constructor(player)
	Server:outputInfo("[CALLING] playerClass["..player:getName().."]: Constructor")
	
	self.Datas = {}
	self.c_logout = function() 
		if source == self.c_player then 
			self:logout() 
		end
	end	
	self.c_wasted = function(...) self:wasted(source, ...) end
	self.c_player = player
	
	
	self:setData("loggedIn", false)
	self:trigger("new")
	self:setDatas ()
	
	
	
	function self.pdatafunc(player, _, data)
		outputChatBox(self:getData(data))
	end
	
	function self.cfacfunc(_, _, f, r)
		self:setFaction(self.c_player, tonumber(f), tonumber(r))
	end
	addCommandHandler("sosa:pdata", self.pdatafunc)
	addCommandHandler("cfac", self.cfacfunc)
	
end

--||EVENTS UND SO
addEvent("Server:playerJoin", true)
addEventHandler("Server:playerJoin", getRootElement(), function() 
	Server.players[source] = playerClass:new(source)
	
end)

addEventHandler("s.playerClass:requestLogin", getRootElement(), function(pw)
	Server.players[source]:login(pw)
end)


function saveDataOnStop()
	outputServerLog("Resource wird gestoppt, Daten werden gespeichert...")
	for i, v in ipairs(getElementsByType("player")) do
		local pos = {getElementPosition(v)}
		if Server.players[v] then
		if Server.players[v]:getData("loggedIn") then
		mysql:exec ("UPDATE `playerData` SET `Handgeld` = ? WHERE `ID` = ?",Server.players[v]:getData("Handgeld"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Bankgeld` = ? WHERE `ID` = ?",Server.players[v]:getData("Bankgeld"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Wanteds` = ? WHERE `ID` = ?",Server.players[v]:getData("Wanteds"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Spielzeit` = ? WHERE `ID` = ?",Server.players[v]:getData("Spielzeit"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Social` = ? WHERE `ID` = ?",Server.players[v]:getData("Social"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Jailtime` = ? WHERE `ID` = ?",Server.players[v]:getData("Jailtime"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Faction` = ? WHERE `ID` = ?",Server.players[v]:getData("Faction"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Rang` = ? WHERE `ID` = ?",Server.players[v]:getData("Rang"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `spawnX` = ? WHERE `ID` = ?",pos[1],Server.players[v]:getData("ID"))
		--mysql:exec ("UPDATE `playerData` SET `spawnX` = ? WHERE `ID` = ?", Server.players[v]:getData("spawnX"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `spawnY` = ? WHERE `ID` = ?",pos[2],Server.players[v]:getData("ID"))
		--mysql:exec ("UPDATE `playerData` SET `spawnY` = ? WHERE `ID` = ?", Server.players[v]:getData("spawnY"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `spawnZ` = ? WHERE `ID` = ?",pos[3],Server.players[v]:getData("ID"))
		--mysql:exec ("UPDATE `playerData` SET `spawnZ` = ? WHERE `ID` = ?", Server.players[v]:getData("spawnZ"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Adminlevel` = ? WHERE `ID` = ?",Server.players[v]:getData("Adminlevel"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Job` = ? WHERE `ID` = ?",Server.players[v]:getData("Job"),Server.players[v]:getData("ID"))
		mysql:exec ("UPDATE `playerData` SET `Unternehmen` = ? WHERE `ID` = ?",Server.players[v]:getData("Unternehmen"),Server.players[v]:getData("ID"))
		Server.players[v]:destructor()
		outputServerLog("Daten des Spielers "..Server.players[v]:getName().."(ID:"..Server.players[v]:getData("ID")..")".." wurden gespeichert!")	
		end
		end
	end
end
addEventHandler("onResourceStop", getResourceRootElement(), saveDataOnStop)
addEvent("s.playerClass:loggedIn", true)


