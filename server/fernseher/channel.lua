--|||||||||||||||||||||||||||||||||||||
--||||Name: Channels for TV        ||||
--||||Author: Apoollo & ravecow    ||||
--||||Project: Streets of SA       ||||
--||||Version: 1.0                 ||||
--|||||||||||||||||||||||||||||||||||||

Channel = {}

TVChannels = {}

function Channel:new(...)
	local obj = setmetatable({}, {__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
end


function Channel:setState(bool)
	self.state = bool
end

function Channel:Stream(cam)
	table.foreach(self.spectator, function(i, v)
		--matrix
	end)
end

function Channel:constructor(id, name)
	if TVChannels[id] then
		return "Channel with ID "..id.." already exists!"
	end
	TVChannels[id] = name
	self.id = id
	self.name = name
	self.state = true
	self.spectator = {}

	
end

function Channel:destroy()
	TVChannels[self.id] = nil
	self = nil
end