--|||||||||||||||||||||||||||||||||||||
--||||Name: TV System              ||||
--||||Author: Apoollo & ravecow    ||||
--||||Project: Streets of SA       ||||
--||||Version: 1.0                 ||||
--|||||||||||||||||||||||||||||||||||||

TVs = {} -- Zähler

Fernseher = {}

local tvModels = { --x,y,z
	[1429] = {},
	[1518] = {},
	[1750] = {},	
	[1752] = {},
	[1786] = {},
	[1792] = {}
}

function Fernseher:new(...)
	local obj = setmetatable({},{__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
end

function Fernseher:constructor(model, pos, rZ) --|| Model, {x, y, z, int, dim}, rZ
	--if #pos < 4 or not tvModels[model] then
	--	return false
	--end
	--self = {}	
	self.tv = createObject(model, pos[1], pos[2], pos[3], 0, 0, rZ)
	setElementInterior(self.tv, pos[4])
	setElementDimension(self.tv, pos[5])	
	table.insert(TVs, self.tv)
	self.id = #TVs
	self.x = pos[1]
	self.y = pos[2]
	self.z = pos[3]
	self.int = pos[4]
	self.dim = pos[5]
	self.channel = 1
	self.spectator = {}
	self.turn = function(player) 
		if not self.spectator[player] then
			self:on(player) 
			self.spectator[player] = true
		else
			self:off(player)
			self.spectator[player] = nil
		end
	end
	self.col = createColSphere(self.x, self.y, self.z, 1.5)
	outputChatBox(tostring(self.tv))
	addEventHandler("onColShapeHit", self.col, function(player)
		toggleControl(player, "enter_exit", false)
		bindKey(player, "enter", "down", self.turn)
	end)
	addEventHandler("onColShapeLeave", self.col, function(player)
		toggleControl(player, "enter_exit", true)
		unbindKey(player, "enter")
	end)
	triggerClientEvent("Fernseher:New", self.tv,  self)
end


function Fernseher:on(player)
	triggerClientEvent(player, "Fernseher:On", self.tv, self)
	bindKey(player, "arrow_l", "down", function(player) self:changeChannel(player, "previous") end)
	bindKey(player, "arrow_r", "down", function(player) self:changeChannel(player, "next") end)		
end

function Fernseher:off(player)
	triggerClientEvent(player, "Fernseher:Off", self.tv, self)
	unbindKey(player, "arrow_l", "down")
	unbindKey(player, "arrow_r", "down")
	setCameraTarget(player, player)

end


function Fernseher:changeChannel(player, typ)
	if typ == "previous" then
		outputChatBox("PREVIOUS")
		if TVChannels[self.channel - 1] then
			self:setChannel(player, self.channel - 1)
		end
	elseif typ == "next" then
		outputChatBox("NEXT")
		if TVChannels[self.channel + 1] then
			self:setChannel(player, self.channel + 1)
		end
	end
end


function Fernseher:setChannel(player, channel)
	--||removing player from spectator-list||--
	table.foreach(self.spectator, function(i, v)
		TVChannels[self.channel].spectator[i] = nil
	end)
	--||||--
	
	table.foreach(self.spectator, function(i, v)
		triggerClientEvent(i, "Fernseher:setChannel", self.tv, channel)	
		TVChannels[channel].spectator[i] = true
	end)
	self.channel = channel	
end


addCommandHandler("tv", function(player, cmd, model)
	outputChatBox("TV")
	local pos = {getElementPosition(player)}
	table.insert(pos, getElementInterior(player))
	table.insert(pos, getElementDimension(player))
	local _,_,rZ = getElementRotation(player)
	Fernseher:new(tonumber(model), pos, rZ)
end)







