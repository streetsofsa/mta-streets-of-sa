--Definitionen--
function isSupporter(admin)
	local AdminLVL = tonumber(Server.players[admin]:getData("Adminlevel"))
	if AdminLVL >= 1 then
		return true
	else
		return false
	end
end

function isModerator(admin)
	local AdminLVL = tonumber(Server.players[admin]:getData("Adminlevel"))
	if AdminLVL >= 2 then
		return true
	else
		return false
	end
end


function isAdmin(admin)
	local AdminLVL = tonumber(Server.players[admin]:getData("Adminlevel"))
	if AdminLVL >= 3 then
		return true
	else
		return false
	end
end

function isHAdmin(admin)
	local AdminLVL = tonumber(Server.players[admin]:getData("Adminlevel"))
	if AdminLVL >= 4 then
		return true
	else
		return false
	end
end

function isLeiter(admin)
	local AdminLVL = tonumber(Server.players[admin]:getData("Adminlevel"))
	if AdminLVL >= 5 then
		return true
	else
		return false
	end
end

Admin = {}
Admin.functions = {}
Admin.settings = {}
Admin.settings.name = {
	[0] = "User",
	[1] = "Supporter",
	[2] = "Moderator",
	[3] = "Admin",
	[4] = "Head Admin",
	[5] = "Projektleiter"
}

Admin.settings.color = {
	[1] = 55,
	[2] = 120,
	[3] = 250
}
adminsIngame = {}

local aR = Admin.settings.color[1]
local aG = Admin.settings.color[2]
local aB = Admin.settings.color[3]

function Admin.output(string, level)
	if not level then level = 1 end
	for i, v in pairs(adminsIngame) do
	if tonumber(Server.players[i]:getData("Adminlevel")) > tonumber(level) then
		outputChatBox("["..string.."]", i, aR, aG, aB)
	end	
	end	
end
----------------------------------------------------------------------

function Admin.Chatbox(player, cmd, ...)
	if not isSupporter(player) then return end
	local chatTable = {...}
	local text = table.concat(chatTable, " ")
	outputChatBox("[["..Admin.settings.name[Server.players[player]:getData("Adminlevel")].." "..getPlayerName(player)..": "..text.."]]", getRootElement(), 200, 145, 0)
end
addCommandHandler("o", Admin.Chatbox)

function Admin.aChat(player, cmd, ...)
	if not isSupporter(player) then return end
	local chatTable = {...}
	local text = table.concat(chatTable, " ")
	for i, v in pairs(adminsIngame) do
		outputChatBox("["..Admin.settings.name[Server.players[player]:getData("Adminlevel")].." "..getPlayerName(player)..": "..text.."]", i, 100, 25, 150)
	end	
end
addCommandHandler("a", Admin.aChat)

--|| Admin level ||--
function Admin.functions.setAdminLevel(admin, cmd, player, lvl)
	if isLeiter(admin) then
		if lvl == nil then lvl = 0 end
		if tonumber(lvl) > 5 or player == nil then
			outputChatBox("/setlevel [PLAYER] [LEVEL(1-5)]", admin, 200, 0, 0)
		else
		Server.players[getPlayerFromName(player)]:setData("Adminlevel", tonumber(lvl))
		outputChatBox("Du wurdest von "..getPlayerName(admin).." zum "..Admin.settings.name[tonumber(lvl)].." gemacht!", getPlayerFromName(player), 160, 155, 130)
		if tonumber(lvl) > 0 then 
			adminsIngame[getPlayerFromName(player)] = true
		else
			adminsIngame[getPlayerFromName(player)] = false
		end	
		Admin.output(player.." wurde von "..getPlayerName(admin).." zum Leader der Fraktion "..Admin.settings.name[tonumber(lvl)].." gemacht!", 2)
		Server:log("admin", "ADMIN: "..getPlayerName(admin).." hat den Adminlevel von "..player.." auf "..lvl.." gesetzt")
		end
	end
end
addCommandHandler("setlevel", Admin.functions.setAdminLevel)

function Admin.functions.getAdminLevel(admin, cmd)
	outputChatBox(tostring(Server.players[admin]:getData("Adminlevel")), admin)
end
addCommandHandler("alvl", Admin.functions.getAdminLevel)
------------------------------------

--|| Fraktion ||--
function Admin.functions.makeleader(admin, cmd, player, fraktion)
	if fraktion == "sapd" then fraktion = 1 end
	if fraktion == "army" then fraktion = 2 end
	if fraktion == "fbi" then fraktion = 3 end
	if fraktion == "news" then fraktion = 4 end
	if fraktion == "medic" then fraktion = 5 end
	if fraktion == "grove" then fraktion = 6 end
	if fraktion == "mafia" then fraktion = 7 end
	if fraktion == "saap" then fraktion = 8 end
	if fraktion == "yakuza" then fraktion = 9 end
	if fraktion == "hitman" then fraktion = 10 end
	if fraktion == "dod" then fraktion = 11 end
	if isHAdmin then
		if fraktion == nil or player == nil then
			outputChatBox("/makeleader [SPIELER] [FRAKTION]", admin, 200, 0, 0)
		else
			Server.players[getPlayerFromName(player)]:setFaction(admin, tonumber(fraktion), 7)
			
			outputChatBox("Du wurdest von "..getPlayerName(admin).." zum Leader der Fraktion "..Fraktionen.settings.name[tonumber(fraktion)].." gemacht!", getPlayerFromName(player), 160, 155, 130)
			Admin.output(player.." wurde von "..getPlayerName(admin).." zum "..Fraktionen.settings.name[tonumber(fraktion)].." gemacht!", 1)
			Server:log("admin", "LEADER: "..player.." wurde von "..getPlayerName(admin).." zum Leader der Fraktion "..Fraktionen.settings.name[tonumber(fraktion)].."("..fraktion..")".." ernannt")
		end
	end
end
addCommandHandler("makeleader", Admin.functions.makeleader)
------------------------------------------------------------

function Admin.functions. setWantedLevel(admin, cmd, player, wanteds)
	if admin and player then
		setPlayerWantedLevel(getPlayerFromName(player), tonumber(wanteds))
		outputChatBox("Deine Wanteds wurden von "..getPlayerName(admin).." auf "..wanteds.." Sterne gesetzt", getPlayerFromName(player), 0, 0, 240)
		for i, v in ipairs(adminsIngame) do
			Admin.output(getPlayerName(admin).." hat die Wanteds von "..player.." auf "..wanteds.." Sterne gesetzt", 2)
		end
		Server.players[getPlayerFromName(player)]:setData("Wanteds", tonumber(wanteds))
	end
end
addCommandHandler("setwanteds", Admin.functions.setWantedLevel)
------------------------------------------------------------

function Admin.functions.playerKick(admin, cmd, player, reason)
	if not player or not reason then return end
	if isSupporter(admin) then
		Server:log("admin", "KICK: "..player.." wurde von "..getPlayerName(admin).." gekickt; Grund: "..reason)		
		kickPlayer(getPlayerFromName(player), admin, reason)

	end
end
addCommandHandler("kick", Admin.functions.playerKick)
------------------------------------------------------------

function Admin.functions.giveWeapon(admin, cmd, player, weapon, ammo)
	if isAdmin(admin) then
		if getWeaponIDFromName(weapon) and getPlayerFromName(player) then
			if not ammo then ammo = 9999 end
			giveWeapon(getPlayerFromName(player), getWeaponIDFromName(weapon), tonumber(ammo), true)
			Server:log("admin", "GIVE: "..player.." hat von "..getPlayerName(admin).." eine "..weapon.." bekommen")
			Admin.output(player.." hat von "..getPlayerName(admin).." eine "..weapon.." bekommen!", 2)	
			outputChatBox("Du hast von "..getPlayerName(admin).." eine "..string.upper(weapon).." bekommen!", getPlayerFromName(player), 40, 240, 40)
		end
	end
end
addCommandHandler("gweap", Admin.functions.giveWeapon)




function Admin.functions.openPanel(player)
	--if tonumber(Server.players[player]:getData("Adminlevel")) >= 1 then
		triggerClientEvent(player, "admin:panel", player, tonumber(Server.players[player]:getData("Adminlevel")))
	--end
end
addCommandHandler("admin", Admin.functions.openPanel)





