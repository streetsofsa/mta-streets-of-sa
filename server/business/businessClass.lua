businessSystem = {}

businessClass = {}

function businessClass:new(...)
	local obj = setmetatable({}, {__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
	return obj
end

function businessClass:setData(i, v)
	setElementData(self.c_bizelement, i, v)
end

function businessClass:getData(i)
	return getElementData(self.c_bizelement, i)
end


function businessClass:enterInterior(player, marker)
		if marker == self.entryMarker then
			Server.players[player]:setData("currentBiz", self.c_bizelement)
			setElementInterior(player, self.intPos[1], self.intPos[2], self.intPos[3], self.intPos[4])
		
		elseif marker == self.exitMarker then
			setElementInterior(player, 0, self.entrypos[1], self.entrypos[2], self.entrypos[3])
		end
end




function businessClass:createInterior(px,py,pz, int)
	if not businessSystem.types[self.c_typ] then
		return
	end

	self.entrypos = {px,py,pz}
	
	if (self.c_typ == "SUPERMARKET") or (self.c_typ == "AMMUNATION") then
		self.intPos = {businessSystem.types[self.c_typ][int][1], businessSystem.types[self.c_typ][int][2], businessSystem.types[self.c_typ][int][3], businessSystem.types[self.c_typ][int][4]}
	else
		self.intPos = {businessSystem.types[self.c_typ][1], businessSystem.types[self.c_typ][2], businessSystem.types[self.c_typ][3], businessSystem.types[self.c_typ][4]}
	end
	
	self.entryMarker = createMarker(self.entrypos[1], self.entrypos[2], self.entrypos[3]+0.2, "arrow", 1, 255, 255, 255, 0)
	self.exitMarker = createMarker(self.intPos[2], self.intPos[3], self.intPos[4]+0.2, "arrow", 1, 255, 255, 255, 0)
	setElementInterior(self.exitMarker, self.intPos[1])
	
	
	self.hit = function(ele)
		local marker = source
		if not getElementType(ele) == "player" then
			return
		end
		toggleControl(ele, "enter_exit", false)
		bindKey(ele, "enter", "down", function(player) 
			self:enterInterior(player, marker)
		end) 
	end
	
	self.leave = function(ele)
		toggleControl(ele, "enter_exit", true)
		unbindKey(ele, "enter")
	end
	
	
	addEventHandler("onMarkerHit", self.entryMarker, self.hit)
	addEventHandler("onMarkerHit", self.exitMarker, self.hit)
	addEventHandler("onMarkerLeave", self.entryMarker, self.leave)
	addEventHandler("onMarkerLeave", self.exitMarker, self.leave)
end


function businessClass:constructor(id, name, typ, locX, locY, locZ)
	self.c_bizelement = createElement("Business", "bizid:"..id)
	self:setData("ID", id)
	self:setData("Name", name)
	self:setData("Typ", typ)
	self.c_id = id
	self.c_name = name
	self.c_typ = typ
	self.pos = {locX, locY, locZ}
	
	
	self.bizpickup = createPickup(self.pos[1], self.pos[2], self.pos[3], 3, 1274, 0)
	addEventHandler("onPickupHit", self.bizpickup, function()
		outputChatBox(string.format("ID: %s, Name: %s, Typ: %s", tostring(self.c_id), self.c_name, self.c_typ))
	end)
end

addCommandHandler("cbiz", function(player)
	local pos = {getElementPosition(player)}
	local id = #Server.biz+1
	Server.biz[id] = businessClass:new(id, "Burger Shot", "BURGERSHOT", pos[1], pos[2], pos[3])
	Server.players[player]:setData("admin:lastBIZ", id)
end)

addCommandHandler("bizint", function(player)
	local pos = {getElementPosition(player)}
	if Server.players[player]:getData("admin:lastBIZ") then
		local id = tonumber(Server.players[player]:getData("admin:lastBIZ"))
		Server.biz[id]:createInterior(pos[1], pos[2], pos[3])
	end	
end)