Bus = {}
local bCounter = {}
Busse = {}


function Bus:new(...)
	local obj = setmetatable ( {}, {__index = self} )
	if obj.constructor then
		obj:constructor (...)
	end
	return obj
end

function Bus:changeRoute(linie, richtung)
	Server.jobVehicles[self.newbus]:setData("Route", {linie, richtung})
	outputChatBox(tostring(Buslinien[tonumber(linie)][1][tonumber(richtung)]))
	triggerClientEvent(root, "vehicles.bus:changeRoute", self.newbus)
end

function Bus:constructor(x,y,z)
	self.newbus = createVehicle(431, x, y, z, 0, 0, 0, "SAVV")
	
	self.id = #bCounter+1
	
	Server.jobVehicles[self.newbus] = Server.jobVehiclesClass:new(self.newbus, "SAVV")
	Server.jobVehicles[self.newbus]:setData("Route", {0, 0})	
	
	
	self.update = function() triggerClientEvent(source, "vehicles.bus:new", self.newbus) end
	
	addEventHandler("onPlayerJoin", root, self.update)
	triggerClientEvent(root, "vehicles.bus:new", self.newbus, self.route)
	
	self.decon = function() self:deconstructor() end
	
	addEventHandler("onVehicleExplode", self.newbus, self.decon)
end

function Bus:deconstructor()
	
	
end



addCommandHandler("createbus", function(player, cmd)
	outputChatBox("Bus kreiert")
	local pos = {getElementPosition(player)}
	Busse[#Busse+1] = {}
	Busse[#Busse] = Bus:new(pos[1] + 1, pos[2] + 2, pos[3])
	
end)

addCommandHandler("liku", function(player, cmd, bus, linie)
	Busse[tonumber(bus)]:changeRoute(tonumber(linie), 1)
	
end)
addCommandHandler("co", function(player, cmd, r1, g1, b1, r2, g2, b2)
	
	local veh = getPedOccupiedVehicle(player)
	setVehicleColor(veh, tonumber(r1), tonumber(g1), tonumber(b1), tonumber(r2), tonumber(g2), tonumber(b2))
end)



