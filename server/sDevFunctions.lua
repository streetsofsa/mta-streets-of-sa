
local sDevFunctions = {
	
	veh = {}
}

function sDevFunctions.veh.port(slot)
	outputChatBox("hey")
end



--||||||||||||||||||||||||||||||||||||||||||--
function sDevFunctions.cmd(player, _, func, ...)
	if not Server.settings.debugmode then
		return
	end
	assert(func, "/execute: func [args]")
	local funcArgs = {...}
	
	if sDevFunctions[string.lower(gettok(func, 1, "."))] then
		if sDevFunctions[string.lower(gettok(func, 1, "."))][string.lower(gettok(func, 2, "."))] then
			sDevFunctions[string.lower(gettok(func, 1, "."))][string.lower(gettok(func, 2, "."))](funcArgs)
		end	
	else
		outputChatBox(getPlayerName(player))
		triggerClientEvent(player, "dev:executeCommand", root, func, funcArgs)
		
	end
end

addCommandHandler("execute", sDevFunctions.cmd)
