﻿--|| MAP ||--
removeWorldModel(3980, 77.638008, 1481.1875, -1785.0703, 22.38281)
removeWorldModel(4044, 77.638008, 1481.1875, -1785.0703, 22.38281)
removeWorldModel(4002, 96.110512, 1479.8672, -1790.3984, 56.02344)
removeWorldModel(4024, 96.110512, 1479.8672, -1790.3984, 56.02344)
--||Veg||--
removeWorldModel(620, 22.657042, 1447.9063, -1748.2266, 12.90625)
removeWorldModel(700, 11.084518, 1438.0313, -1747.9375, 13.44531) 
removeWorldModel(673, 8.8485546, 1415.3125, -1748.5625, 12.39844)
removeWorldModel(620, 22.657042, 1533.2656, -1749.0234, 12.80469)
removeWorldModel(673, 8.8485546, 1522.1641, -1748.5625, 13.02344)
removeWorldModel(700, 11.084518, 1553.7031, -1747.9375, 13.40625)
removeWorldModel(673, 8.8485546, 1553.2578, -1764.8125, 12.79688)
removeWorldModel(713, 27.49649, 1407.1953, -1749.3125, 13.09375)
removeWorldModel(713, 27.49649, 1405.2344, -1821.1172, 13.10156)
removeWorldModel(620, 22.657042, 1417.9766, -1832.5312, 11.98438)
removeWorldModel(673, 8.8485546, 1405.5781, -1831.6953, 12.39844)
removeWorldModel(700, 11.084518, 1447.1016, -1832.5, 12.91406)
removeWorldModel(620, 22.657042, 1456.3984, -1832.5312, 11.98438)
removeWorldModel(673, 8.8485546, 1464.0938, -1831.8828, 12.39844)
removeWorldModel(700, 11.084518, 1404.9141, -1765.2656, 12.91406)
removeWorldModel(620, 22.657042, 1504.8438, -1832.5312, 11.98438)
removeWorldModel(700, 11.084518, 1554.8203, -1816.1562, 13.47656)
removeWorldModel(700, 11.084518, 1512.9453, -1832.3516, 13.46875)
removeWorldModel(673, 8.8485546, 1499.0469, -1832.2734, 12.88281)
--||||--
removeWorldModel(3997, 102.22401, 1479.3359, -1802.2891, 12.54688)
removeWorldModel(4045, 102.22401, 1479.3359, -1802.2891, 12.54688)
removeWorldModel(4003, 500, 1447.9063, -1748.2266, 12.90625)
--|| END MAP ||--


places["StadthalleLS"] = locations:new(1479.376,-1792.660,15.556, 389.742, 173.854, 1008.382, 3, "Stadthalle Los Santos")




--[[StadtHalleLS = {marker = {}}
StadtHalleLS.marker.eingang = createMarker(1481.0999755859, -1772.3000488281, 17.700000762939, "arrow", 1, 0, 0, 0, 0)
StadtHalleLS.marker.ausgang = createMarker(387.705, 174.3994, 1008.3828, "arrow", 1, 0, 0, 0, 0)
setElementInterior(StadtHalleLS.marker.ausgang, 3)

table.foreach(StadtHalleLS.marker, function(_, v)
	addEventHandler("onMarkerHit", v, function(hitele) StadtHalleLS.hitMarker(source, hitele) end)
	addEventHandler("onMarkerLeave", v, function(hitele) unbindKey(hitele, "enter", "down", StadtHalleLS.hitMarker) end)
end)

function StadtHalleLS.hitMarker(marker, hitele)
	if marker == StadtHalleLS.marker.eingang then
	
	elseif marker == StadtHalleLS.marker.ausgang then
		
	end
end

function pickedUpRathaus (source)

	if getPedOccupiedVehicle(source) == false then
		setElementPosition (source, 384.808624,173.804992,1008.382812)
		setElementInterior (source, 3)
		toggleControl ( source, "fire", false )
		toggleControl ( source, "enter_exit", false )
		setElementData(source,"nodmzone", 1)
	end
end
addEventHandler ( "onPickupHit", rathauspickup_1, pickedUpRathaus )

rathauspickup_2 = createPickup ( 387.705, 174.3994, 1008.3828, 3, 1318, 0)
setElementInterior (rathauspickup_2, 3)

function pickedUpRathaus2 (source)
   setElementPosition (source, 1481.0999755859, -1772.3000488281, 17.700000762939)
   setElementInterior (source, 0)
   toggleControl ( source, "fire", true )
   toggleControl ( source, "enter_exit", true )
   setElementData(source,"nodmzone", 0)
end
addEventHandler ( "onPickupHit", rathauspickup_2, pickedUpRathaus2 )

rathausmarker = createMarker ( 362.45562744141, 173.60470581055, 1007.5, "corona", 3, 125, 0, 0, 0 )
setElementInterior (rathausmarker, 3)
rathausmarker2 = createMarker ( 362.45562744141, 173.60470581055, 1007, "cylinder", 1, 125, 0, 0 )
setElementInterior (rathausmarker2, 3)

function rathausmarker_func (player)
   
    setElementFrozen ( player, true )
    setTimer ( setElementFrozen, 100, 1, player, false )
	triggerClientEvent ( player, "ShowRathausMenue", getRootElement() )
	showCursor ( player, true )
	setElementData ( player, "ElementClicked", true )
end
addEventHandler ( "onMarkerHit", rathausmarker, rathausmarker_func )

rathausped = createPed(141, 359.7138671875, 173.625765625, 1008.38934)
setElementInterior (rathausped, 3)
setPedRotation(rathausped, 280)]]--