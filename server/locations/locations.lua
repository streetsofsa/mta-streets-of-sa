places = {} --Variablentabelle

locations = {}
orte = {} --Zählertabelle

function locations:new(...)
	local obj = setmetatable ({},{__index = self})
	if obj.constructor then
		obj:constructor(...)
	end
	return obj
end

function locations:enter(player, marker)
	if marker == self.markers.eingang then
		setElementDimension(player, 0)
		setElementInterior(player, self.datas.int2, self.datas.x2, self.datas.y2, self.datas.z2)
	elseif marker == self.markers.ausgang then
		setElementDimension(player, 0)
		setElementInterior(player, 0, self.datas.x1, self.datas.y1, self.datas.z1)		
	end
end


function locations:constructor(x1, y1, z1, x2, y2, z2, int2, name)
	if not name then name = "" end
	self.element = createElement("location", "LOC "..tostring(#orte + 1)..": "..name)
	self.datas = {}
	self.id = #orte + 1
	self.datas.x1 = x1
	self.datas.y1 = y1
	self.datas.z1 = z1
	self.datas.x2 = x2
	self.datas.y2 = y2
	self.datas.z2 = z2
	self.datas.int2 = int2
	self.datas.name = name
	table.insert(orte, {self.id, self.datas})
	
	self.hit = function(hitele) 
		local marker = source
		toggleControl(hitele, "enter_exit", false)
		if not getPedOccupiedVehicle(hitele) then
			bindKey(hitele, "enter", "down", function() self:enter(hitele, marker) end)
		end
	end
	
	self.leave = function(hitele) 
		unbindKey(hitele, "enter")
		toggleControl(hitele, "enter_exit", true)
	end
	
	self.markers = {}
	self.markers.eingang = createMarker(x1, y1, z1, "arrow", 1.3, 0, 0, 0, 5)
	self.markers.ausgang = createMarker(x2, y2, z2, "arrow", 1.3, 0, 0, 0, 5)
	setElementInterior(self.markers.ausgang, int2)
	
	table.foreach(self.markers, function(_, v)
		addEventHandler("onMarkerHit", v, self.hit)
		addEventHandler("onMarkerLeave", v, self.leave)
	end)
	
	local col1 = createColSphere(x1, y1, z1, 18)
	local col2 = createColSphere(x2, y2, z2, 18)
	addEventHandler("onColShapeHit", col1, function(hitele)
		setTimer(function()
			triggerClientEvent(hitele, "locations:startRender", self.element, self.datas, 1)
		end, 100, 1)
	end)
	addEventHandler("onColShapeLeave", col1, function(hitele) triggerClientEvent(hitele, "locations:stopRender", self.element) end)
	
	addEventHandler("onColShapeHit", col2, function(hitele) 
		setTimer(function()
		triggerClientEvent(hitele, "locations:startRender", self.element, self.datas, 2)
		end, 100, 1)
	end)
	addEventHandler("onColShapeLeave", col2, function(hitele) triggerClientEvent(hitele, "locations:stopRender", self.element) end)	
end



















