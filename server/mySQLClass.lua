mysql = {}
function mysql:connect(host, username, password, name)
	local obj = setmetatable({}, {__index = self})
	
	self.connection = dbConnect("mysql", "dbname="..name..";host="..host.."", username, password)
	if not self.connection then 
		return false 
	end
	return obj
end

function mysql:exec(...)
	if self.connection then
		return dbExec(self.connection, ...)
	end	
end

function mysql:query(...)
	if self.connection then
		return dbQuery(self.connection, ...)
	end
end

function mysql:poll(sql, time)
	if self.connection then
		local mysqlquery = dbQuery(self.connection, sql)
		return dbPoll(mysqlquery, time)
	end	
end



--Connect with database

--mysql:connect("db4free.net", "apreallife", "apreallife", "apreallife")
