house = {}

houseClass = {}

function getPlayerOccupiedHouse(player)
	if player then
		return Server.players[player]:getData("currentHouse")
	end
end

function houseClass:new (...)
	local obj = setmetatable ( {}, {__index = self} )
	if obj.constructor then
		obj:constructor (...)
	end
	return obj
end

function houseClass:setData(data, value)
	setElementData(self.c_house, data, value)
end

function houseClass:getData(data)
	if data then
		return getElementData(self.c_house, data)
	else
		return false
	end
end

function houseClass:hEnter(player, marker)
	unbindKey(player, "enter")
    --|| Enter ||--
	if marker == self.markers.c_eingang then 
		triggerClientEvent(player, "cHouse:call", self.c_house, player)
		--setElementInterior(player, self:getData("Interior"), self:getData("IntX"), self:getData("IntY"), self:getData("IntZ"))
		--setElementDimension(player, self:getData("Dim"))
	--|| Exit ||--
	elseif marker == self.markers.c_ausgang then 
		setElementInterior(player, 0, self:getData("houseX"), self:getData("houseY"), self:getData("houseZ"))
		setElementDimension(player, 0)
		Server.players[player]:setData("currentHouse", false)
	--|| Garage Enter ||--	
	elseif marker == self.markers.c_garage then 
		if self:getData("Owner") == getPlayerName(player) then
			if getPedOccupiedVehicle(player) then
				setElementPosition(getPedOccupiedVehicle(player), tonumber(gettok(self:getData("gPosition"), 2, "|")), tonumber(gettok(self:getData("gPosition"), 3, "|")), tonumber(gettok(self:getData("gPosition"), 4, "|")))
				setElementInterior(getPedOccupiedVehicle(player), tonumber(gettok(self:getData("gPosition"), 1, "|")))			
			end
		end	
		setElementInterior(player, tonumber(gettok(self:getData("gPosition"), 1, "|")), tonumber(gettok(self:getData("gPosition"), 2, "|")), tonumber(gettok(self:getData("gPosition"), 3, "|")), tonumber(gettok(self:getData("gPosition"), 4, "|")))
		setElementDimension(player, self:getData("Dim"))
	--|| Garage Exit ||--
	elseif marker == self.markers.c_garage_a then 
		setElementInterior(player, 0, self:getData("gX"), self:getData("gY"), self:getData("gZ") + 0.1)
		setElementDimension(player, 0)
	end
end

function houseClass:createGarage(pos, gID)
	local garage = hInteriors[tonumber("100"..tostring(gID))]
	self:setData("gX", pos[1])
	self:setData("gY", pos[2])
	self:setData("gZ", pos[3])
	
	table.foreach(hGaragen.interior[gID], function(i, v)
		local object = createObject(v[1], v[2], v[3], v[4], v[5], v[6], v[7])
		setElementInterior(object, gettok(garage, 1, "|"))
		setElementDimension(object, self:getData("Dim"))
	end)	
	
	self:setData("gPosition", garage)
	self.markers.c_garage = createMarker(tonumber(pos[1]), tonumber(pos[2]), tonumber(pos[3]) + 0.2, "arrow", 1.1, 8, 8, 8, 0)
	self.markers.c_garage_a = createMarker(tonumber(gettok(garage, 2, "|")), tonumber(gettok(garage, 3, "|")), tonumber(gettok(garage, 4, "|")), "arrow", 1.1, 8, 8, 8, 0)
	setElementInterior(self.markers.c_garage_a, tonumber(gettok(garage, 1, "|")))
	setElementDimension(self.markers.c_garage, self:getData("Dim"))
	
	addEventHandler("onMarkerHit", self.markers.c_garage, function(hitele) self.hit(source, hitele) end)
	addEventHandler("onMarkerLeave", self.markers.c_garage, function(leftele) self.leave(source, leftele) end)
	addEventHandler("onMarkerHit", self.markers.c_garage_a, function(hitele) self.hit(source, hitele) end)
	addEventHandler("onMarkerLeave", self.markers.c_garage_a, function(leftele) self.leave(source, leftele) end)
	
end

function houseClass:constructor(id, owner, houseX, houseY, houseZ)
	--||Datas setzen||--
	self.c_house = createElement("house", "h:"..id)
--	local sql = mysql:poll("SELECT * FROM `housedata` WHERE `ID` = '"..self.c_id.."'", -1)	
	self.c_id = id
	self.c_owner = owner
	self:setData("ID", id)
	self:setData("houseX", tonumber(houseX))
	self:setData("houseY", tonumber(houseY))
	self:setData("houseZ", tonumber(houseZ))
	self:setData("Dim", id + 10000) -- Haus Dims ab 10000
	self:setData("Interior", 0)
	self:setData("IntX", 0)
	self:setData("IntY", 0)
	self:setData("IntZ", 0)
	self:setData("gX", 0)
	self:setData("gY", 0)
	self:setData("gZ", 0)
	self:setData("Owner", owner)
	self:setData("Preis", 0)
	self:setData("Ort", "")
	self:setData("Carslots", 0)
	self:setData("Garage", 0)
	self:setData("gPosition", "100|0|0|0")
	self:setData("Kasse", 0)
	self:setData("Miete", 0)	
	
	--self.enter = function(player) removeEventHandler("onMarkerHit", self.c_ausgang, self.exit) self:houseEnter(player) end
	self.hit = function(marker, player) 
		toggleControl(player, "enter_exit", false)
		bindKey(player, "enter", "down", function(player) 
			self:hEnter(player, marker)
		end) 
	end
	self.leave = function(marker, player) 
		unbindKey(player, "enter")
		toggleControl(player, "enter_exit", true)
	end
	self.markers = {}
	--||Eingang||--
	--Weil nicht so schnell
	setTimer(function()
	self.markers.c_eingang = createMarker(self:getData("houseX"), self:getData("houseY"), self:getData("houseZ") + 0.2, "arrow", 1.1, 255, 255, 255, 0)
	self.pickup = createPickup(self:getData("houseX"), self:getData("houseY"), self:getData("houseZ"), 3, 1273, 0)
	self.markers.c_ausgang = createMarker(self:getData("IntX"), self:getData("IntY"), self:getData("IntZ") - 0.3, "arrow", 1.1, 255, 255, 255, 0)
	setElementInterior(self.markers.c_ausgang, self:getData("Interior"))
	
	
	
	table.foreach(self.markers, function(_, v)
		addEventHandler("onMarkerHit", v, function(hitele) self.hit(source, hitele) end)
		addEventHandler("onMarkerLeave", v, function(leftele) self.leave(source, leftele) end)
	end)	
	end, 1000, 1)
	
end


