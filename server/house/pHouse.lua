pHouse = {commands = {}}


function pHouse.loadAll()
	local sql = mysql:poll("SELECT * FROM `housedata`", -1)
	table.foreach(sql, function(i, v)
		house[tonumber(v["ID"])] = houseClass:new(tonumber(v["ID"]), v["Besitzer"], v["hX"], v["hY"], v["hZ"])
		house[tonumber(v["ID"])]:setData("ID", v["ID"])
		house[tonumber(v["ID"])]:setData("houseX",  tonumber(v["hX"]))
		house[tonumber(v["ID"])]:setData("houseY",  tonumber(v["hY"]))
		house[tonumber(v["ID"])]:setData("houseZ",  tonumber(v["hZ"]))
		house[tonumber(v["ID"])]:setData("Interior",  tonumber(gettok(v["Interior"], 1, "|")))
		house[tonumber(v["ID"])]:setData("IntX",  tonumber(gettok(v["Interior"], 2, "|")))
		house[tonumber(v["ID"])]:setData("IntY",  tonumber(gettok(v["Interior"], 3, "|")))
		house[tonumber(v["ID"])]:setData("IntZ",  tonumber(gettok(v["Interior"], 4, "|")))
		house[tonumber(v["ID"])]:setData("Owner", v["Besitzer"])
		house[tonumber(v["ID"])]:setData("Preis",  tonumber(v["Preis"]))
		house[tonumber(v["ID"])]:setData("Ort", v["Ort"])
		house[tonumber(v["ID"])]:setData("Garage",  tonumber(v["Garage"]))
		house[tonumber(v["ID"])]:setData("gPosition",  tonumber(v["Garagenplatz"]))
		house[tonumber(v["ID"])]:setData("gX",  tonumber(v["GarageX"]))
		house[tonumber(v["ID"])]:setData("gY",  tonumber(v["GarageY"]))
		house[tonumber(v["ID"])]:setData("gZ",  tonumber(v["GarageZ"]))
		house[tonumber(v["ID"])]:setData("Carslots",  tonumber(v["Slots"]))
		house[tonumber(v["ID"])]:setData("Kasse",  tonumber(v["Kasse"]))
		house[tonumber(v["ID"])]:setData("Miete",  tonumber(v["Miete"]))	
		if house[tonumber(v["ID"])]:getData("Garage") > 0 then
			house[tonumber(v["ID"])]:createGarage({house[tonumber(v["ID"])]:getData("gX"), house[tonumber(v["ID"])]:getData("gY"), house[tonumber(v["ID"])]:getData("gZ")}, house[tonumber(v["ID"])]:getData("Garage"))
		
		end
	end)
end
addEventHandler("onResourceStart", getResourceRootElement(), pHouse.loadAll)


addCommandHandler("hc", function(player, cmd, int, preis)
	local x,y,z = getElementPosition(player)
	local id = table.getn(house) + 1	
	if slots == nil then slots = 0 end
	outputChatBox("Haus-ID: "..tostring(id))
	house[id] = houseClass:new(id, getPlayerName(player), x, y, z)
	house[id]:setData("Preis", tonumber(preis))
	house[id]:setData("Carslots", 0)
	house[id]:setData("Garage", 0)
	house[id]:setData("hX", tonumber(x))
	house[id]:setData("hY", tonumber(y))
	house[id]:setData("hZ", tonumber(z))
	house[id]:setData("Interior",  tonumber(gettok(hInteriors[tonumber(int)], 1, "|")))
	house[id]:setData("IntX",  tonumber(gettok(hInteriors[tonumber(int)], 2, "|")))
	house[id]:setData("IntY",  tonumber(gettok(hInteriors[tonumber(int)], 3, "|")))
	house[id]:setData("IntZ",  tonumber(gettok(hInteriors[tonumber(int)], 4, "|")))
	house[id]:setData("Ort", getElementZoneName(player)..", "..getElementZoneName(player, true))
	mysql:exec("INSERT INTO `housedata` VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
	house[id]:getData("ID"),
	house[id]:getData("Owner"),
	house[id]:getData("Preis"),
	house[id]:getData("Ort"),
	house[id]:getData("gPosition"),	
	house[id]:getData("Garage"),
	house[id]:getData("gX"),
	house[id]:getData("gY"),
	house[id]:getData("gZ"),
	house[id]:getData("Carslots"),
	house[id]:getData("hX"),
	house[id]:getData("hY"),
	house[id]:getData("hZ"),
	house[id]:getData("Interior").."|"..house[id]:getData("IntX").."|"..house[id]:getData("IntY").."|"..house[id]:getData("IntZ"),
	house[id]:getData("Kasse"),
	house[id]:getData("Miete"))
end)

addCommandHandler("grg", function(player, cmd, id, garage) -- 1002 = 2
	if id and garage then
	house[tonumber(id)]:createGarage({getElementPosition(player)}, tonumber(garage))
	house[tonumber(id)]:setData("Garage", tonumber(garage))
	house[tonumber(id)]:setData("Carslots", tonumber(garage) * 2)
	outputChatBox("Garage für Haus "..id.." in "..house[tonumber(id)]:getData("Ort").." erstellt")
	else
		outputChatBox("1001: 2 Slots")
		outputChatBox("1002: 4 Slots")
		outputChatBox("1003: 6 Slots")
		outputChatBox("1004: 8 Slots")
		outputChatBox("1005: 10 Slots")
	end
end)

addEventHandler("onResourceStop", getResourceRootElement(), function()
table.foreach(house, function(i, v)
		mysql:exec("UPDATE `housedata` SET `Besitzer` = ? WHERE `ID` = ?", house[i]:getData("Owner"), house[i]:getData("ID"))
		--||Create||--
		mysql:exec("UPDATE `housedata` SET `Preis` = ? WHERE `ID` = ?", house[i]:getData("Preis"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `Ort` = ? WHERE `ID` = ?", house[i]:getData("Ort"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `Garagenplatz` = ? WHERE `ID` = ?", house[i]:getData("gPosition"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `Garage` = ? WHERE `ID` = ?", house[i]:getData("Garage"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `GarageX` = ? WHERE `ID` = ?", house[i]:getData("gX"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `GarageY` = ? WHERE `ID` = ?", house[i]:getData("gY"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `GarageZ` = ? WHERE `ID` = ?", house[i]:getData("gZ"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `Slots` = ? WHERE `ID` = ?", house[i]:getData("Carslots"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `hX` = ? WHERE `ID` = ?", house[i]:getData("houseX"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `hY` = ? WHERE `ID` = ?", house[i]:getData("houseY"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `hZ` = ? WHERE `ID` = ?", house[i]:getData("houseZ"), house[i]:getData("ID"))		
		mysql:exec("UPDATE `housedata` SET `Interior` = ? WHERE `ID` = ?", house[i]:getData("Interior").."|"..house[i]:getData("IntX").."|"..house[i]:getData("IntY").."|"..house[i]:getData("IntZ"), house[i]:getData("ID"))		
		--||||--
		mysql:exec("UPDATE `housedata` SET `Kasse` = ? WHERE `ID` = ?", house[i]:getData("Kasse"), house[i]:getData("ID"))
		mysql:exec("UPDATE `housedata` SET `Miete` = ? WHERE `ID` = ?", house[i]:getData("Miete"), house[i]:getData("ID"))
end)
end)
