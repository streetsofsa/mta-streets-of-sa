//HUD MASK
//File: hud.fx

//Includes
#include "mta-helper.fx"

//Settings
texture FX_TXD  : SFX_TXD;
texture FX_MASK : SFX_MASK;

float2 FX_PREPOS = float2(0, 0);
float2 FX_POS = float2(0, 0);
float2 FX_SCALE = float(1);
float2 FX_SCLCENTER = float2(0.5, 0.5);
float FX_ROTATION = float(0);
float2 FX_ROTCENTER = float2(0.5, 0.5);

//Functions
float3x3 getTransformation(){
	return MTACalcTransformMatrix(FX_PREPOS, FX_POS, FX_SCALE, FX_SCLCENTER, FX_ROTATION, FX_ROTCENTER);
}


//Techniques
technique hudmask{
	pass P0{
		
		Texture[0] = FX_TXD;
		TextureTransform[0] = getTransformation();
		TextureTransformFlags[0] = Count2;
		AddressU[0] = Clamp;
		AddressV[0] = Clamp;
		
		ColorOp[0] = Modulate;
		ColorArg1[0] = Texture;
		ColorArg2[0] = Diffuse;
		
		AlphaOp[0] = Modulate;
		AlphaArg1[0] = Texture;
		AlphaArg2[0] = Diffuse;


		
		Texture[1] = FX_MASK;
		TexCoordIndex[1] = 0;
		AddressU[1] = Clamp;
		AddressV[1] = Clamp;
		
		ColorOp[1] = SelectArg1;
		ColorArg1[1] = Current;
		
		AlphaOp[1] = Modulate;
		AlphaArg1[1] = Current;
		AlphaArg2[1] = Texture;


		ColorOp[2] = Disable;
		AlphaOp[2] = Disable;
	}
};

technique fallback{
	pass P0{}
};