//File: blur.fx

//Includes
#include "mta-helper.fx"

//Settings
texture FX_TEXTURE : SFX_TEXTURE;
float2  FX_TXDSIZE : SFX_TXDSIZE;
float   FX_AMOUNT  : SFX_AMOUNT = 0.5;


static const float Kernel[13] = { -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6 };
static const float Weights[13] = { 0.002216, 0.008764, 0.026995, 0.064759, 0.120985, 0.176033, 0.199471, 0.176033, 0.120985, 0.064759, 0.026995, 0.008764, 0.002216 };

//Sampler
sampler SAMPLER0 = sampler_state{
	Texture = FX_TEXTURE;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};


//Structures for Vertex&Pixel Shader
struct VSInput{
	float3 Position : POSITION;
	float4 Diffuse  : COLOR0;
	float2 TexCoord : TEXCOORD0;
};


struct PSInput{
	float4 Position : POSITION0;
	float4 Diffuse  : COLOR0;
	float2 TexCoord : TEXCOORD0;
};


//Functions
PSInput VSFunction(VSInput VShader){

	PSInput PShader;

	PShader.Position = MTACalcScreenPosition(VShader.Position);
	PShader.Diffuse = VShader.Diffuse;
	PShader.TexCoord = VShader.TexCoord;

	return PShader;
}

float4 PSFunction(PSInput PShader) : COLOR0{

	float4 color = 0;
	float2 coord;
    coord.y = PShader.TexCoord.y;
	
	for (int i = 0; i < 13; ++i){
	    coord.x = PShader.TexCoord.x + (Kernel[i]/FX_TXDSIZE.x) * (FX_AMOUNT * 8);
	    coord.x = PShader.TexCoord.x - (Kernel[i]/FX_TXDSIZE.x) * (FX_AMOUNT * 8);
        coord.y = PShader.TexCoord.y + (Kernel[i]/FX_TXDSIZE.y) * (FX_AMOUNT * 8);
        coord.y = PShader.TexCoord.y - (Kernel[i]/FX_TXDSIZE.y) * (FX_AMOUNT * 8);
        color += tex2D(SAMPLER0, coord.xy) * Weights[i] * 1;
	
	}

	color = color * PShader.Diffuse;
	color.a = 1;
	return color;
}


//Techniques
technique gblur{
	pass P0{
		VertexShader = compile vs_2_0 VSFunction();
		PixelShader  = compile ps_2_0 PSFunction();
	}
};

technique fallback{
	pass P0{}
};